---
title: "README"
tags: ""
---
# Usage

Extractor usage:
`./run.sh <IRI> <recursion_depth> <timeout> <output_file>`

Arguments:    

-   `<IRI>` is the IRI of the entity whose relevant knowledge fragment we want to extract
-   `<recursion_depth>` recursion depth of the relevant knowledge fragment (must be an integer)
-   `<timeout>` timeout in seconds of the extraction system (must be an integer, 0: infinite)
-   `<output_file>` output file where the extracted triples will be stored in Turtle format

## Example

`./run.sh "http://dbpedia.org/resource/Raismes" 1 120 Raismes.ttl`

# Caching

Delete the folder `.cache` if you don't want to reuse the previously cached triples.

# Testing 

For testing the whole application use the method it.unife.ml.kraider.main.MainTest#testMain()

# Change the SPARQL endpoint list

Modify the file `src/main/resources/sparql_endpoints.json` before the compilation.

(During testing change `test/main/resources/sparql_endpoints.json`)

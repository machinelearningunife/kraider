/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.main;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class MainTest {

    public MainTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of main method, of class Main.
     */
    @Test
    public void testMain() throws Exception {
        Integer timeoutInt = 0;
        String timeout = "" + timeoutInt;
        String entity = "http://dbpedia.org/resource/Raismes";
        String recursionDepth = "1";
        String output_file = entity.substring(entity.lastIndexOf("/") + 1) + "_" + recursionDepth;
        Main.main(new String[]{entity, recursionDepth, timeout, output_file});
    }

}

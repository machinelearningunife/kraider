/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.caching;

import it.unife.ml.kraider.data.JobStatus;
import it.unife.ml.kraider.data.NamedResourceJob;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.LinkedHashSet;
import java.util.Set;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import org.apache.jena.graph.Triple;
import org.ehcache.CacheManager;
import org.ehcache.config.Configuration;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.EntryUnit;
import org.ehcache.config.units.MemoryUnit;
import org.ehcache.xml.XmlConfiguration;
import org.junit.jupiter.api.Disabled;
import org.semanticweb.owlapi.model.IRI;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class KRaiderCacheTest {

    public KRaiderCacheTest() {
    }

    /**
     * Test of put method, of class KRaiderCache.
     *
     * @throws java.net.MalformedURLException
     * @throws java.lang.InterruptedException
     */
    @Test
    public void testPutGet() throws MalformedURLException, InterruptedException {
        System.out.println("put and get");
        String sparqlQuery = "SELECT";

        Set<Triple> triples = new LinkedHashSet<>();
        Model model = ModelFactory.createDefaultModel();

        Triple t1 = new Triple(
                model.createResource("angela_merkel").asNode(),
                model.createResource("p").asNode(),
                model.createResource("o").asNode());
        URL domain = new URL("http://dbpedia.org");
        triples.add(t1);
        System.out.println(t1);

        KRaiderCache instance = KRaiderCache.getInstance();
        IRI subjectIRI = IRI.create(t1.getSubject().getURI());
        JobStatus status = new JobStatus();
        NamedResourceJob job = new NamedResourceJob(domain, subjectIRI, 0, status);
        instance.put(job, triples);
        Set<Triple> cachedTriples = instance.get(job);
        System.out.println(cachedTriples.iterator().next());
        instance.close();
        assertTrue(triples.size() == cachedTriples.size());
    }

    @Disabled
    @Test
    public void tempTest() {
        CacheManager cacheManager = CacheManagerBuilder.newCacheManagerBuilder()
                .with(CacheManagerBuilder.persistence("myData"))
                .withCache("threeTieredCache",
                        CacheConfigurationBuilder.newCacheConfigurationBuilder(Long.class, String.class,
                                ResourcePoolsBuilder.newResourcePoolsBuilder()
                                        .heap(10, EntryUnit.ENTRIES)
                                        .offheap(1, MemoryUnit.MB)
                                        .disk(20, MemoryUnit.MB, true))
                                .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofSeconds(20)))
                ).build(false);

        Configuration configuration = cacheManager.getRuntimeConfiguration();
        XmlConfiguration xmlConfiguration = new XmlConfiguration(configuration);
        String xml = xmlConfiguration.toString();
        System.out.println(xml);
    }

}

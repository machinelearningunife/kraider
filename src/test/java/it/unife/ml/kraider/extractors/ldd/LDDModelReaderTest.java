/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.extractors.ldd;

import java.io.File;
import org.apache.jena.rdf.model.Model;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class LDDModelReaderTest {

    public LDDModelReaderTest() {
    }

    /**
     * Test of retrieveLinkedDataDocument method, of class LDDModelReader.
     */
    @Test
    public void testRetrieveRemoteLinkedDataDocument() throws Exception {
        System.out.println("retrieveRemoteLinkedDataDocument");
        String iri = "http://dbpedia.org/ontology/Band";
        LDDModelReader modelReader = new LDDModelReader();
        Model result = modelReader.retrieveModel(iri);
        result.write(System.out);
        assertNotNull(result);

    }

    /**
     * Test of retrieveLinkedDataDocument method, of class LDDModelReader.
     */
    @Test
    public void testRetrieveLocalLinkedDataDocument() throws Exception {
        System.out.println("retrieveLocalLinkedDataDocument");
        String filepath = System.getProperty("user.dir") 
                + File.separator + "examples" 
                + File.separator + "Angela_Merkel.ttl";
        String iri = "file://" + filepath;
        LDDModelReader modelReader = new LDDModelReader();
        Model result = modelReader.retrieveModel(iri);
        result.write(System.out);
        assertNotNull(result);


    }

}

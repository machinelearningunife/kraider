/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.extractors.sparql;

import it.unife.ml.kraider.data.Filters;
import it.unife.ml.kraider.exception.InvalidRecursionDepthException;
import it.unife.ml.kraider.extractors.sparql.SPARQLQueryMaker;
import java.net.MalformedURLException;
import org.apache.jena.query.ParameterizedSparqlString;
import org.apache.jena.shared.PrefixMapping;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.text.IsEqualIgnoringCase.equalToIgnoringCase;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.vocab.OWLRDFVocabulary;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class SPARQLQueryMakerTest {

    public SPARQLQueryMakerTest() {
    }

//    /**
//     * Test of makeQueryString method, of class SPARQLQueryMaker.
//     */
//    @Test
//    public void testMakeQueryString_URL_int() throws MalformedURLException, InvalidRecursionDepthException {
//        System.out.println("makeInstanceFragmentQueryString");
//        IRI subject = IRI.create("http://dbpedia.org/resource/Angela_Merkel");
//        int recursionDepth = 3;
//        SPARQLQueryMaker instance = new SPARQLQueryMaker();
//
//        ParameterizedSparqlString pss = new ParameterizedSparqlString();
//        pss.setNsPrefixes(PrefixMapping.Standard);
//
//        String expResult = pss.toString()
//                + "SELECT DISTINCT * WHERE { \n"
//                + "	<http://dbpedia.org/resource/Angela_Merkel> (owl:sameAs|^owl:sameAs)* ?subject_same . \n"
//                + "	?subject_same ?predicate0 ?object0 . \n"
//                + "	FILTER (?predicate0 != owl:sameAs && ?object0 != owl:Class && ?object0 != owl:Thing) . \n"
//                + "	OPTIONAL { \n"
//                + "		?object0 (owl:sameAs|^owl:sameAs)* ?object_same0 . \n"
//                + "		?object_same0 ?predicate1 ?object1 . \n"
//                + "		FILTER (?predicate1 != owl:sameAs && ?object1 != owl:Thing) . \n"
//                + "		OPTIONAL { \n"
//                + "			?object1 (owl:sameAs|^owl:sameAs)* ?object_same1 . \n"
//                + "			?object_same1 ?predicate2 ?object2 . \n"
//                + "			FILTER (?predicate2 != owl:sameAs && ?object2 != owl:Thing) . \n"
//                + "		}\n"
//                + "	} \n"
//                + "}";
//        expResult = expResult.replaceAll("\\s+", " ").trim();
//
//        String result = instance.makeInstanceFragmentQueryString(subject, recursionDepth);
//
//        result = result.replaceAll("\\s+", " ").trim();
//
//        assertThat(result, is(equalToIgnoringCase(expResult)));
//    }
    @Test
    public void testMakeSameAsQueryString() throws MalformedURLException {
        System.out.println("makeSameAsQueryString");
        IRI resource = IRI.create("http://dbpedia.org/resource/Angela_Merkel");

        SPARQLQueryMaker instance = new SPARQLQueryMaker();

        ParameterizedSparqlString pss = new ParameterizedSparqlString();
        pss.setNsPrefixes(PrefixMapping.Standard);

        String expResult = pss.toString()
                + "SELECT * WHERE { \n"
                + "<http://dbpedia.org/resource/Angela_Merkel> (owl:sameAs|^owl:sameAs)+ ?subject_same .\n"
                + "FILTER ( !REGEX( STR(?subject_same), '" + OWLRDFVocabulary.OWL_THING.getIRI().toString() + "') ) ."
                + " }";
        expResult = expResult.replaceAll("\\s+", " ").trim();

        String result = instance.makeSameAsQueryString(resource, Filters.getDefaultFilters());

        result = result.replaceAll("\\s+", " ").trim();

        assertThat(result, is(equalToIgnoringCase(expResult)));
    }

    /**
     * Test of makeEquivalentClassesQueryString method, of class
     * SPARQLQueryMaker.
     */
    @Test
    public void testMakeEquivalentClassesQueryString() {
        System.out.println("makeEquivalentClassesQueryString");
        IRI resource = IRI.create("http://dbpedia.org/resource/Angela_Merkel");

        SPARQLQueryMaker instance = new SPARQLQueryMaker();

        ParameterizedSparqlString pss = new ParameterizedSparqlString();
        pss.setNsPrefixes(PrefixMapping.Standard);

        String expResult = pss.toString()
                + "SELECT * WHERE { \n"
                + "<http://dbpedia.org/resource/Angela_Merkel> (owl:equivalentClass|^owl:equivalentClass)+ ?subject_same .\n"
                + "}";
        expResult = expResult.replaceAll("\\s+", " ").trim();

        String result = instance.makeEquivalentClassesQueryString(resource, new Filters());

        result = result.replaceAll("\\s+", " ").trim();

        assertThat(result, is(equalToIgnoringCase(expResult)));
    }

    /**
     * Test of makeEquivalentPropertyQueryString method, of class
     * SPARQLQueryMaker.
     */
    @Test
    public void testMakeEquivalentPropertyQueryString() {
        System.out.println("makeEquivalentPropertyQueryString");
        IRI resource = IRI.create("http://dbpedia.org/resource/Angela_Merkel");

        SPARQLQueryMaker instance = new SPARQLQueryMaker();

        ParameterizedSparqlString pss = new ParameterizedSparqlString();
        pss.setNsPrefixes(PrefixMapping.Standard);

        String expResult = pss.toString()
                + "SELECT * WHERE { \n"
                + "<http://dbpedia.org/resource/Angela_Merkel> (owl:equivalentProperty|^owl:equivalentProperty)+ ?subject_same .\n"
                + "}";
        expResult = expResult.replaceAll("\\s+", " ").trim();

        String result = instance.makeEquivalentPropertyQueryString(resource, new Filters());

        result = result.replaceAll("\\s+", " ").trim();

        assertThat(result, is(equalToIgnoringCase(expResult)));
    }

    /**
     * Test of makeEntityFragmentQueryString method, of class SPARQLQueryMaker.
     */
    @Test
    public void testMakeEntityFragmentQueryString_3args() throws Exception {
        System.out.println("makeEntityFragmentQueryString");
        IRI subject = IRI.create("http://dbpedia.org/resource/Angela_Merkel");
        IRI predicate = IRI.create("http://dbpedia.org/ontology/almaMater");
        int recursionDepth = 3;
        SPARQLQueryMaker instance = new SPARQLQueryMaker();

        ParameterizedSparqlString pss = new ParameterizedSparqlString();
        pss.setNsPrefixes(PrefixMapping.Standard);

        String expResult = pss.toString()
                + "SELECT DISTINCT * WHERE { \n"
                //                + "	<http://dbpedia.org/resource/Angela_Merkel> (owl:sameAs|^owl:sameAs)* ?subject_same . \n"
                + "	<http://dbpedia.org/resource/Angela_Merkel> <http://dbpedia.org/ontology/almaMater> ?object0 . \n"
                + "     FILTER ( !REGEX( STR(?object0), '" + OWLRDFVocabulary.OWL_THING.getIRI().toString() + "') ) . \n"
                + "	OPTIONAL { \n"
//                + "		?object0 (owl:sameAs|^owl:sameAs)* ?object_same0 . \n"
                + "		?object0 ?predicate1 ?object1 . \n"
                + "             FILTER ( (!REGEX( STR(?object1), '" + OWLRDFVocabulary.OWL_THING.getIRI().toString() + "')  \n"
                + "              && (!REGEX( STR(?predicate1), '" + OWLRDFVocabulary.OWL_EQUIVALENT_CLASS.getIRI().toString() + "') \n"
                + "              && !REGEX( STR(?predicate1), '" + OWLRDFVocabulary.OWL_EQUIVALENT_PROPERTY.getIRI().toString() + "') \n"
                + "              && !REGEX( STR(?predicate1), '" + OWLRDFVocabulary.OWL_SAME_AS.getIRI().toString() + "') ) ) ) . \n"
                + "		OPTIONAL { \n"
//                + "			?object1 (owl:sameAs|^owl:sameAs)* ?object_same1 . \n"
                + "			?object1 ?predicate2 ?object2 . \n"
                + "                     FILTER ( (!REGEX( STR(?object2), '" + OWLRDFVocabulary.OWL_THING.getIRI().toString() + "')  \n"
                + "                      && (!REGEX( STR(?predicate2), '" + OWLRDFVocabulary.OWL_EQUIVALENT_CLASS.getIRI().toString() + "') \n"
                + "                      && !REGEX( STR(?predicate2), '" + OWLRDFVocabulary.OWL_EQUIVALENT_PROPERTY.getIRI().toString() + "') \n"
                + "                      && !REGEX( STR(?predicate2), '" + OWLRDFVocabulary.OWL_SAME_AS.getIRI().toString() + "') ) ) ) . \n"
                + "		}\n"
                + "	} \n"
                + "}";
        expResult = expResult.replaceAll("\\s+", " ").trim();

        String result = instance.makeEntityFragmentQueryString(subject, predicate, Filters.getDefaultFilters(), recursionDepth);

        result = result.replaceAll("\\s+", " ").trim();

        assertThat(result, is(equalToIgnoringCase(expResult)));
    }

    /**
     * Test of makeEntityFragmentQueryString method, of class SPARQLQueryMaker.
     */
    @Test
    public void testMakeEntityFragmentQueryString_IRI_int() throws Exception {
        System.out.println("makeEntityFragmentQueryString");
        IRI subject = IRI.create("http://dbpedia.org/resource/Angela_Merkel");
        int recursionDepth = 3;
        SPARQLQueryMaker instance = new SPARQLQueryMaker();

        ParameterizedSparqlString pss = new ParameterizedSparqlString();
        pss.setNsPrefixes(PrefixMapping.Standard);

        String expResult = pss.toString()
                + "SELECT DISTINCT * WHERE { \n"
//                + "	<http://dbpedia.org/resource/Angela_Merkel> (owl:sameAs|^owl:sameAs)* ?subject_same . \n"
                + "	<http://dbpedia.org/resource/Angela_Merkel> ?predicate0 ?object0 . \n"
//                + "	?subject_same ?predicate0 ?object0 . \n"
                + "     FILTER ( (!REGEX( STR(?object0), '" + OWLRDFVocabulary.OWL_THING.getIRI().toString() + "')  \n"
                + "      && (!REGEX( STR(?predicate0), '" + OWLRDFVocabulary.OWL_EQUIVALENT_CLASS.getIRI().toString() + "') \n"
                + "      && !REGEX( STR(?predicate0), '" + OWLRDFVocabulary.OWL_EQUIVALENT_PROPERTY.getIRI().toString() + "') \n"
                + "      && !REGEX( STR(?predicate0), '" + OWLRDFVocabulary.OWL_SAME_AS.getIRI().toString() + "') ) ) ) . \n"
                + "	OPTIONAL { \n"
//                + "		?object0 (owl:sameAs|^owl:sameAs)* ?object_same0 . \n"
                + "		?object0 ?predicate1 ?object1 . \n"
                + "             FILTER ( (!REGEX( STR(?object1), '" + OWLRDFVocabulary.OWL_THING.getIRI().toString() + "')  \n"
                + "              && (!REGEX( STR(?predicate1), '" + OWLRDFVocabulary.OWL_EQUIVALENT_CLASS.getIRI().toString() + "') \n"
                + "              && !REGEX( STR(?predicate1), '" + OWLRDFVocabulary.OWL_EQUIVALENT_PROPERTY.getIRI().toString() + "') \n"
                + "              && !REGEX( STR(?predicate1), '" + OWLRDFVocabulary.OWL_SAME_AS.getIRI().toString() + "') ) ) ) . \n"
                + "		OPTIONAL { \n"
//                + "			?object1 (owl:sameAs|^owl:sameAs)* ?object_same1 . \n"
                + "			?object1 ?predicate2 ?object2 . \n"
                + "                     FILTER ( (!REGEX( STR(?object2), '" + OWLRDFVocabulary.OWL_THING.getIRI().toString() + "')  \n"
                + "                      && (!REGEX( STR(?predicate2), '" + OWLRDFVocabulary.OWL_EQUIVALENT_CLASS.getIRI().toString() + "') \n"
                + "                      && !REGEX( STR(?predicate2), '" + OWLRDFVocabulary.OWL_EQUIVALENT_PROPERTY.getIRI().toString() + "') \n"
                + "                      && !REGEX( STR(?predicate2), '" + OWLRDFVocabulary.OWL_SAME_AS.getIRI().toString() + "') ) ) ) . \n"
                + "		}\n"
                + "	} \n"
                + "}";
        expResult = expResult.replaceAll("\\s+", " ").trim();

        String result = instance.makeEntityFragmentQueryString(subject, Filters.getDefaultFilters(), recursionDepth);

        result = result.replaceAll("\\s+", " ").trim();

        assertThat(result, is(equalToIgnoringCase(expResult)));
    }

}

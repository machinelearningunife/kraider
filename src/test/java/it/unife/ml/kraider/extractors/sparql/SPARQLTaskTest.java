/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.extractors.sparql;

import it.unife.ml.kraider.data.Filters;
import it.unife.ml.kraider.exception.InvalidRecursionDepthException;
import it.unife.ml.kraider.exception.UnsupportedRDFSyntaxException;
import it.unife.ml.kraider.extractors.SPARQLQueryExecutor;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Stream;
import javax.xml.ws.http.HTTPException;
import org.apache.jena.query.ParameterizedSparqlString;
import org.apache.jena.query.ResultSetRewindable;
import org.apache.jena.shared.PrefixMapping;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import static org.mockito.Mockito.*;
import org.semanticweb.owlapi.model.IRI;
import it.unife.ml.kraider.api.KTriple;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class SPARQLTaskTest {

    ParameterizedSparqlString pss = new ParameterizedSparqlString();

    public SPARQLTaskTest() {
        pss.setNsPrefixes(PrefixMapping.Standard);
    }

    @BeforeAll
    public static void init() {

    }

//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//    
//    @Before
//    public void setUp() {
//    }
//    
//    @After
//    public void tearDown() {
//    }
    /**
     * Test of raidInstanceFragment method, of class SPARQLTask.
     */
    @ParameterizedTest
    @MethodSource
    public void testRaidEntityFragment(IRI subject, URL domain, URL sparqlEndpointURL) throws MalformedURLException, InvalidRecursionDepthException, IOException, UnsupportedRDFSyntaxException {
        System.out.println("raidEntityFragment");
//        IRI subject = IRI.create(subjectString);
        int recursionDepth = 1;

//        String queryString = String.format(pss.toString()
//                + "SELECT * WHERE { \n"
//                + "<%s> (owl:sameAs|^owl:sameAs)* ?subject_same .\n"
//                + " ?subject_same ?predicate0  ?object0 .\n"
//                + " FILTER (?predicate0 != owl:sameAs) .\n"
//                + "OPTIONAL { \n"
//                + " ?object0 (owl:sameAs|^owl:sameAs)* ?object_same0 .\n"
//                + " ?object_same0 ?predicate1 ?object1 .\n"
//                + " FILTER (?predicate1 != owl:sameAs && ?object1 != owl:Class && ?object1 != owl:Thing) .\n"
//                + "OPTIONAL { \n"
//                + " ?object1 (owl:sameAs|^owl:sameAs)* ?object_same1 .\n"
//                + " ?object_same1 ?predicate2 ?object2 . \n"
//                + " FILTER (?predicate2 != owl:sameAs && ?object2 != owl:Class && ?object2 != owl:Thing) .\n"
//                + " }\n"
//                + " }\n"
//                + "}", subject.toString());
//        String queryEntityString = String.format(pss.toString()
//                + "SELECT * WHERE { \n"
//                + "<%s> ?predicate0  ?object0 .\n"
//                + " FILTER (?predicate0 != owl:sameAs) .\n"
//                + "}", subject.toString());
//        
//        String queryString = String.format(pss.toString()
//                + "SELECT * WHERE { \n"
//                + "<%s> (owl:equivalentClass|^owl:equivalentClass)+ ?subject_same .\n"
//                + "}", category.toString());
        SPARQLQueryMaker queryMaker = new SPARQLQueryMaker();
//        when(queryMaker.makeEntityFragmentQueryString(subject, recursionDepth)).thenReturn(queryEntityString);

        SPARQLEndpoint endpoint = mock(SPARQLEndpoint.class);
//        when(endpoint.getDefaultGraphURIs()).thenReturn(Collections.singletonList("http://dbpedia.org"));
        when(endpoint.getDefaultGraphIRIs()).thenReturn(Collections.emptyList());
        when(endpoint.getNamedGraphIRIs()).thenReturn(Collections.emptyList());
        when(endpoint.getURL()).thenReturn(sparqlEndpointURL);
        when(endpoint.getDomain()).thenReturn(domain);

        SPARQLTask instance = new SPARQLTask(SPARQLQueryExecutor.sparqlEndpointQueryExecutor(),
                queryMaker, new Filters(), endpoint, SPARQLEndpointTripleExtractor.class);

        Set<KTriple> result = null;
        try {
            result = instance.raidEntityFragment(subject, recursionDepth);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace(System.out);
            throw e;
        }
        assertTrue(result != null && result.size() > 0);

    }

    private static Stream<Arguments> testRaidEntityFragment() throws MalformedURLException {
        return Stream.of(
                Arguments.of("http://dbpedia.org/resource/Angela_Merkel", new URL("http://dbpedia.org"), new URL("http://dbpedia.org/sparql")),
                Arguments.of("http://data.europa.eu/euodp/jrc-names/Angela_Merkel", new URL("http://data.europa.eu"), new URL("https://data.europa.eu/euodp/sparqlep"))
        );
    }

    /**
     * Test of raidSameAs method, of class SPARQLTask.
     *
     * @param entity
     * @param domain
     * @param sparqlEndpointURL
     */
    @ParameterizedTest
    @MethodSource
    public void testRaidSameAs(IRI entity, URL domain, URL sparqlEndpointURL) throws IOException, HTTPException, UnsupportedRDFSyntaxException {
        System.out.println("raidSameAs");
        String queryString = String.format(pss.toString()
                + "SELECT * WHERE { \n"
                + "<%s> (owl:sameAs|^owl:sameAs)+ ?subject_same .\n"
                + "}", entity.toString());

        SPARQLQueryMaker queryMaker = mock(SPARQLQueryMaker.class);
        when(queryMaker.makeSameAsQueryString(eq(entity), any(Filters.class))).thenReturn(queryString);

        SPARQLEndpoint endpoint = mock(SPARQLEndpoint.class);
        when(endpoint.getDefaultGraphIRIs()).thenReturn(Collections.emptyList());
        when(endpoint.getNamedGraphIRIs()).thenReturn(Collections.emptyList());
        when(endpoint.getURL()).thenReturn(sparqlEndpointURL);
        when(endpoint.getDomain()).thenReturn(domain);

        SPARQLTask instance = new SPARQLTask(SPARQLQueryExecutor.sparqlEndpointQueryExecutor(),
                queryMaker, new Filters(), endpoint, SPARQLEndpointTripleExtractor.class);
        Set<KTriple> result = instance.raidSameAs(entity);
        assertTrue(result.size() > 0);
    }

    private static Stream<Arguments> testRaidSameAs() throws MalformedURLException {
        return Stream.of(
                Arguments.of("http://dbpedia.org/resource/Angela_Merkel", new URL("http://dbpedia.org"), new URL("http://dbpedia.org/sparql"))
        );
    }

    /**
     * Test of raidEquivalentClasses method, of class SPARQLTask.
     *
     * @param category
     * @param domain
     * @param sparqlEndpointURL
     */
    @ParameterizedTest
    @MethodSource
    public void testRaidEquivalentClasses(IRI category, URL domain, URL sparqlEndpointURL) throws IOException, HTTPException, UnsupportedRDFSyntaxException {
        System.out.println("raidEquivalentClasses");

        String queryString = String.format(pss.toString()
                + "SELECT * WHERE { \n"
                + "<%s> (owl:equivalentClass|^owl:equivalentClass)+ ?subject_same .\n"
                + "}", category.toString());

        SPARQLQueryMaker queryMaker = mock(SPARQLQueryMaker.class);
        when(queryMaker.makeEquivalentClassesQueryString(eq(category), any(Filters.class))).thenReturn(queryString);

        SPARQLEndpoint endpoint = mock(SPARQLEndpoint.class);
        when(endpoint.getDefaultGraphIRIs()).thenReturn(Collections.emptyList());
        when(endpoint.getNamedGraphIRIs()).thenReturn(Collections.emptyList());
        when(endpoint.getURL()).thenReturn(sparqlEndpointURL);
        when(endpoint.getDomain()).thenReturn(domain);

        SPARQLTask instance = new SPARQLTask(
                SPARQLQueryExecutor.sparqlEndpointQueryExecutor(),
                queryMaker, new Filters(), endpoint, SPARQLEndpointTripleExtractor.class);
        Set<KTriple> result = instance.raidEquivalentClasses(category);
        assertTrue(result.size() > 0);
    }

    private static Stream<Arguments> testRaidEquivalentClasses() throws MalformedURLException {
        return Stream.of(
                Arguments.of("http://dbpedia.org/ontology/Person", new URL("http://dbpedia.org"), new URL("http://dbpedia.org/sparql"))
        );
    }

    /**
     * Test of raidEquivalentProperty method, of class SPARQLTask.
     *
     * @param property
     * @param domain
     * @param sparqlEndpointURL
     */
    @ParameterizedTest
    @MethodSource
    public void testRaidEquivalentProperty(IRI property, URL domain, URL sparqlEndpointURL) throws HTTPException, UnsupportedRDFSyntaxException, IOException {
        System.out.println("raidEquivalentProperty");
        String queryString = String.format(pss.toString()
                + "SELECT * WHERE { \n"
                + "<%s> (owl:equivalentProperty|^owl:equivalentProperty)+ ?subject_same .\n"
                + "}", property.toString());

        SPARQLQueryMaker queryMaker = mock(SPARQLQueryMaker.class);
        when(queryMaker.makeEquivalentPropertyQueryString(eq(property), any(Filters.class))).thenReturn(queryString);

        SPARQLEndpoint endpoint = mock(SPARQLEndpoint.class);
        when(endpoint.getDefaultGraphIRIs()).thenReturn(Collections.emptyList());
        when(endpoint.getNamedGraphIRIs()).thenReturn(Collections.emptyList());
        when(endpoint.getURL()).thenReturn(sparqlEndpointURL);
        when(endpoint.getDomain()).thenReturn(domain);

        SPARQLTask instance = new SPARQLTask(
                SPARQLQueryExecutor.sparqlEndpointQueryExecutor(),
                queryMaker, new Filters(), endpoint, SPARQLEndpointTripleExtractor.class);
        Set<KTriple> result = instance.raidEquivalentProperty(property);
        assertTrue(result.size() > 0);
    }

    private static Stream<Arguments> testRaidEquivalentProperty() throws MalformedURLException {
        return Stream.of(
                Arguments.of("http://dbpedia.org/ontology/almaMater", new URL("http://dbpedia.org"), new URL("http://dbpedia.org/sparql"))
        );
    }

    /**
     * Test of queryAsResultSet method, of class SPARQLTask.
     */
    @Disabled
    @Test
    public void testQueryAsResultSet() throws IOException, UnsupportedRDFSyntaxException {
        System.out.println("queryAsResultSet");
        String sparqlQueryString = "";
        SPARQLTask instance = null;
        ResultSetRewindable expResult = null;
        ResultSetRewindable result = instance.queryAsResultSet(sparqlQueryString);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of resolveBlankNode method, of class SPARQLTask.
     */
    @Disabled
    @Test
    public void testResolveBlankNode() {
        System.out.println("resolveBlankNode");
        SPARQLTask instance = null;
        Set<KTriple> expResult = null;
        Set<KTriple> result = instance.resolveBlankNode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}

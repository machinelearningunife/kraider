/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.extractors;

import it.unife.ml.kraider.exception.UnsupportedRDFSyntaxException;
import it.unife.ml.kraider.extractors.embedded.EmbeddedHTMLEndpoint;
import it.unife.ml.kraider.extractors.embedded.EmbeddedModelReader;
import it.unife.ml.kraider.extractors.ldd.LDDEndpoint;
import it.unife.ml.kraider.extractors.ldd.LDDModelReader;
import it.unife.ml.kraider.extractors.sparql.SPARQLEndpoint;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import javax.xml.ws.http.HTTPException;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSetRewindable;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class SPARQLQueryExecutorTest {

    private String sparqlQueryString = "PREFIX owl: <http://www.w3.org/2002/07/owl#>"
            + " "
            + "SELECT * WHERE { \n"
            + "?s ?p ?o ."
            + "} LIMIT 10";

    public SPARQLQueryExecutorTest() {

    }

    /**
     * Test of sparqlEndpointQueryExecutor method, of class SPARQLQueryExecutor.
     */
    @Test
    public void testSparqlEndpointQueryExecutor() throws MalformedURLException, UnsupportedRDFSyntaxException, IOException {
        System.out.println("sparqlEndpointQueryExecutor");
        SPARQLEndpoint endpoint = mock(SPARQLEndpoint.class);
        when(endpoint.getDefaultGraphIRIs()).thenReturn(Collections.emptyList());
        when(endpoint.getNamedGraphIRIs()).thenReturn(Collections.emptyList());
        when(endpoint.getURL()).thenReturn(new URL("http://query.wikidata.org/sparql"));
        when(endpoint.getDomain()).thenReturn(new URL("http://www.wikidata.org"));

        SPARQLQueryExecutor instance = SPARQLQueryExecutor.sparqlEndpointQueryExecutor();
        ResultSetRewindable result = instance.executeQuery(sparqlQueryString, endpoint);

        while (result.hasNext()) {
            QuerySolution sol = result.next();
            System.out.println(sol.toString());
        }
        result.reset();
        assertTrue(result.hasNext());
    }

    /**
     * Test of sparqlLDDQueryExecutor method, of class SPARQLQueryExecutor.
     */
    @Test
    public void testSparqlLDDQueryExecutor() throws MalformedURLException, IOException, HTTPException, UnsupportedRDFSyntaxException {
        System.out.println("sparqlLDDQueryExecutor");
        LDDEndpoint endpoint = mock(LDDEndpoint.class);
        when(endpoint.getURL()).thenReturn(new URL("http://dbpedia.org/resource/Angela_Merkel"));
        when(endpoint.getDomain()).thenReturn(new URL("http://dbpedia.org"));

        SPARQLQueryExecutor instance = SPARQLQueryExecutor.sparqlLDDQueryExecutor(new LDDModelReader());
        ResultSetRewindable result = instance.executeQuery(this.sparqlQueryString, endpoint);

        while (result.hasNext()) {
            QuerySolution sol = result.next();
            System.out.println(sol.toString());
        }
        result.reset();
        assertTrue(result.hasNext());
    }
    
    /**
     * Test of sparqlLDDQueryExecutor method, of class SPARQLQueryExecutor.
     */
    @Test
    public void testSparqlEmbeddedQueryExecutor() throws MalformedURLException, IOException, HTTPException, UnsupportedRDFSyntaxException {
        System.out.println("sparqlEmbeddedQueryExecutor");
        Endpoint endpoint = mock(EmbeddedHTMLEndpoint.class);
        when(endpoint.getURL()).thenReturn(new URL("http://dbpedia.org/sparql?default-graph-uri=http%3A%2F%2Fdbpedia.org&query=DESCRIBE%20%3Chttp%3A%2F%2Fdbpedia.org%2Fresource%2FFerrara%3E&format=text%2Fx-html-script-turtle"));
        when(endpoint.getDomain()).thenReturn(new URL("http://dbpedia.org"));

        SPARQLQueryExecutor instance = SPARQLQueryExecutor.sparqlLDDQueryExecutor(new EmbeddedModelReader());
        ResultSetRewindable result = instance.executeQuery(this.sparqlQueryString, endpoint);

        while (result.hasNext()) {
            QuerySolution sol = result.next();
            System.out.println(sol.toString());
        }
        result.reset();
        assertTrue(result.hasNext());
    }

}

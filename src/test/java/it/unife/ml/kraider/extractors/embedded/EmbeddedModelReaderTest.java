/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.extractors.embedded;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class EmbeddedModelReaderTest {

    public EmbeddedModelReaderTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of retrieveEmbeddedModel method, of class EmbeddedModelReader.
     */
    @Test
    public void testRetrieveEmbeddedModelJSONLD() throws Exception {
        System.out.println("retrieveEmbeddedModel with JSON-LD Embedded Data");
        String iri = "http://dbpedia.org/sparql?default-graph-uri=http%3A%2F%2Fdbpedia.org&query=DESCRIBE%20%3Chttp%3A%2F%2Fdbpedia.org%2Fresource%2FFerrara%3E&format=text%2Fx-html-script-ld%2Bjson";
        EmbeddedModelReader modelReader = new EmbeddedModelReader();
        Model result = modelReader.retrieveModel(iri);
        assertTrue(result.size() > 0);
    }

    @Test
    public void testRetrieveEmbeddedModelMicrodata() throws Exception {
        System.out.println("retrieveEmbeddedModel with Microdata Embedded Data");
        String iri = "http://dbpedia.org/sparql?default-graph-uri=http%3A%2F%2Fdbpedia.org&query=DESCRIBE%20%3Chttp%3A%2F%2Fdbpedia.org%2Fresource%2FFerrara%3E&format=text%2Fhtml";
        EmbeddedModelReader modelReader = new EmbeddedModelReader();
        Model result = modelReader.retrieveModel(iri);
        assertTrue(result.size() > 0);
    }

    @Test
    public void testRetrieveEmbeddedModelRdfa() throws Exception {
        System.out.println("retrieveEmbeddedModel with RDFa Embedded Data");
        String iri = "https://www.curlingcalendar.com/tournaments/289";
        EmbeddedModelReader modelReader = new EmbeddedModelReader();
        Model result = modelReader.retrieveModel(iri);
        assertTrue(result.size() > 0);
    }

    @Test
    public void testRetrieveEmbeddedModelTurtle() throws Exception {
        System.out.println("retrieveEmbeddedModel with TURTLE Embedded Data");
        String iri = "http://dbpedia.org/sparql?default-graph-uri=http%3A%2F%2Fdbpedia.org&query=DESCRIBE%20%3Chttp%3A%2F%2Fdbpedia.org%2Fresource%2FFerrara%3E&format=text%2Fx-html-script-turtle";
        EmbeddedModelReader modelReader = new EmbeddedModelReader();
        Model result = modelReader.retrieveModel(iri);
        assertTrue(result.size() > 0);
    }

}

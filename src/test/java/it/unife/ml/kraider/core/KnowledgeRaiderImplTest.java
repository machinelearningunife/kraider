/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.core;

import it.unife.ml.kraider.api.KRaiderConfiguration;
import it.unife.ml.kraider.api.KRaiderConfigurationBuilder;
import it.unife.ml.kraider.api.TripleExtractorFactory;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import it.unife.ml.kraider.api.KTriple;
import java.io.FileWriter;
import java.util.HashSet;
import java.util.Set;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Statement;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class KnowledgeRaiderImplTest {

    private final Logger logger = LoggerFactory.getLogger(KnowledgeRaiderImplTest.class);

    public KnowledgeRaiderImplTest() {
    }

    /**
     * Test of raidKnowledge method, of class KnowledgeRaiderImpl.
     */
    @Disabled
    @Test
    public void testRaidKnowledge_3args() {
        System.out.println("raidKnowledge 3 args");
        String subject = "";
        String predicate = "";
        String object = "";
        KnowledgeRaiderImpl instance = null;
        List<BlockingQueue<KTriple>> expResult = null;
        List<BlockingQueue<KTriple>> result = instance.raidKnowledge(subject, predicate, object);
        assertEquals(expResult, result);
    }

    /**
     * Test of raidKnowledge method, of class KnowledgeRaiderImpl.
     */
//    @Disabled
    @Test
    public void testRaidKnowledge_2args() throws Exception {
        logger.info("raidKnowledge 2 args");
//        String resource = "http://dbpedia.org/resource/Angela_Merkel";
        String resource = "http://dbpedia.org/resource/Raismes";
        CopyOnWriteArrayList<TripleExtractorFactory> factoryList = new CopyOnWriteArrayList<>();
        KnowledgeRaiderImpl instance = new KnowledgeRaiderImpl(factoryList);
        KRaiderConfigurationBuilder confBuilder = new KRaiderConfigurationBuilder(instance.getDefaultConfiguration(resource));
        KRaiderConfiguration conf = confBuilder
                .recursionDepth(1)
                .nMaxExtractors(128)
//                .cache(false)
                //                .nMaxExtractors(1)
                //                .sameAsLastObject(false)
                .buildConfiguration();

        BlockingQueue<KTriple> result = instance.raidKnowledge(resource, conf, true, true);
        logger.debug("waiting...");
//        synchronized (this) {
//            wait();
//
//        }
        assertTrue(!result.isEmpty());

        instance.extractedTriplesStatistics.printStatistics();

        KTriple triple;
        Set<Triple> triples = new HashSet<>();
        while ((triple = result.poll(5, TimeUnit.SECONDS)) != null) {
//            Resource sub = ResourceFactory.createResource(triple.getSubject().toString());
//            RDFNode obj = ResourceFactory.createResource(triple.getObject().toString());
//            RDFNode prop = ResourceFactory.createProperty(triple.getPredicate().toString());
            
            triples.add(new Triple(triple.getSubject().asNode(), triple.getPredicate().asNode(), triple.getObject().asNode()));
        }
        
        logger.info("Unique triples: " + triples.size());
        
        Model model = ModelFactory.createDefaultModel();
        for (Triple t : triples) {
            RDFNode object;
            if (t.getObject().isURI()) {
                object = ResourceFactory.createResource(t.getObject().getURI());
            } else {
                object = ResourceFactory.createPlainLiteral(t.getObject().toString());
            }
            model.add(ResourceFactory.createResource(t.getSubject().getURI()), 
                    ResourceFactory.createProperty(t.getPredicate().getURI()),
                    object);
        }
        model.write(new FileWriter("Modello_triple"), "TURTLE");
    }

    @Disabled
    @Test
    public void testRaidKnowledgeStop() throws Exception {
        logger.info("testRaidKnowledgeStop");
        String resource = "http://dbpedia.org/resource/Angela_Merkel";
        CopyOnWriteArrayList<TripleExtractorFactory> factoryList = new CopyOnWriteArrayList<>();
        KnowledgeRaiderImpl instance = new KnowledgeRaiderImpl(factoryList);
        KRaiderConfigurationBuilder confBuilder = new KRaiderConfigurationBuilder(instance.getDefaultConfiguration(resource));
        KRaiderConfiguration conf = confBuilder
                .recursionDepth(2)
                //                .sameAsLastObject(false)
                .buildConfiguration();

        BlockingQueue<KTriple> result = instance.raidKnowledge(resource, conf, false, true);
        TimeUnit.SECONDS.sleep(40);
        instance.stop();
        logger.debug("waiting...");
//        synchronized (this) {
//            wait();
//
//        }
        assertTrue(!result.isEmpty());

        instance.extractedTriplesStatistics.printStatistics();
    }

}

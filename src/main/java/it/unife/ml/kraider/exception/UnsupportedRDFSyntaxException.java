/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.exception;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class UnsupportedRDFSyntaxException extends Exception {
    
    public UnsupportedRDFSyntaxException(String msg) {
        super(msg);
    }
    
}

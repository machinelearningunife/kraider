package it.unife.ml.kraider.main;

import it.unife.ml.kraider.api.*;
import it.unife.ml.kraider.core.KnowledgeRaiderImpl;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Collections;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResourceFactory;
import org.semanticweb.owlapi.model.IRI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KRaider {

    private final static Logger logger = LoggerFactory.getLogger(KRaider.class);

    private String IRI, recursionDepth, timeout, outputFile;

    public KRaider(String IRI, String recursionDepth, String timeout, String outputFile) {
        this.IRI = IRI;
        this.recursionDepth = recursionDepth;
        this.timeout = timeout;
        this.outputFile = outputFile;
    }

    public Map<Origin, Integer> run() throws IOException, InterruptedException {
        try {

            long startTime = System.currentTimeMillis();
            IRI entity = org.semanticweb.owlapi.model.IRI.create(IRI);
            Integer recursionDepth = Integer.parseInt(this.recursionDepth);
            Integer timeout = Integer.parseInt(this.timeout);
            FileWriter output = new FileWriter(outputFile);

            String resource = entity.toString();
            CopyOnWriteArrayList<TripleExtractorFactory> factoryList = new CopyOnWriteArrayList<>();
            KnowledgeRaiderImpl instance = new KnowledgeRaiderImpl(factoryList);
            KRaiderConfigurationBuilder confBuilder = new KRaiderConfigurationBuilder(
                    instance.getDefaultConfiguration(resource));
            KRaiderConfiguration conf = confBuilder
                    .recursionDepth(recursionDepth)
                    .nMaxExtractors(128)
                    // .cache(false)
                    // .nMaxExtractors(1)
                    // .sameAsLastObject(false)
                    .buildConfiguration();

            Thread timeoutThread = null;
            if (timeout > 0) {
                timeoutThread = new Thread(() -> {
                    try {
                        TimeUnit.SECONDS.sleep(timeout);
                        instance.stop();
                    } catch (InterruptedException ex) {
                        // do nothing
                        // logger.error(ex.getMessage());
                    }
                });
                timeoutThread.start();
            }

            BlockingQueue<KTriple> result = instance.raidKnowledge(resource, conf, true, true);

            if (timeoutThread != null) {
                timeoutThread.interrupt();
            }
            logger.debug("waiting...");

            instance.getExtractedTriplesStatistics().printStatistics();

            KTriple triple;
            Set<Triple> triples = new HashSet<>();
            while ((triple = result.poll(5, TimeUnit.SECONDS)) != null) {
                // Resource sub =
                // ResourceFactory.createResource(triple.getSubject().toString());
                // RDFNode obj = ResourceFactory.createResource(triple.getObject().toString());
                // RDFNode prop =
                // ResourceFactory.createProperty(triple.getPredicate().toString());

                triples.add(new Triple(triple.getSubject().asNode(), triple.getPredicate().asNode(),
                        triple.getObject().asNode()));
            }

            // logger.info("Unique triples: " + triples.size());

            Model model = ModelFactory.createDefaultModel();
            for (Triple t : triples) {
                RDFNode object;
                if (t.getObject().isURI()) {
                    object = ResourceFactory.createResource(t.getObject().getURI());
                } else {
                    object = ResourceFactory.createPlainLiteral(t.getObject().toString());
                }
                model.add(ResourceFactory.createResource(t.getSubject().getURI()),
                        ResourceFactory.createProperty(t.getPredicate().getURI()),
                        object);
            }
            model.write(output, "TURTLE");
            long endTimeNoCache = System.currentTimeMillis();
            logger.info("Entity {} recursion depth {} time: {}", entity, recursionDepth,
                    (endTimeNoCache - startTime) / 1000.0);
            // System.exit(0);

            return instance.getExtractedTriplesStatistics().getOriginCount();

        } catch (Exception ex) {
            System.err.println(ex.getMessage());

            return Collections.emptyMap();
        }

    }

    /*
    public void run() throws IOException, InterruptedException {
        try {

            long startTime = System.currentTimeMillis();
            IRI entity = org.semanticweb.owlapi.model.IRI.create(IRI);
            Integer recursionDepth = Integer.parseInt(this.recursionDepth);
            Integer timeout = Integer.parseInt(this.timeout);
            FileWriter output = new FileWriter(outputFile);

            String resource = entity.toString();
            CopyOnWriteArrayList<TripleExtractorFactory> factoryList = new CopyOnWriteArrayList<>();
            KnowledgeRaiderImpl instance = new KnowledgeRaiderImpl(factoryList);
            KRaiderConfigurationBuilder confBuilder = new KRaiderConfigurationBuilder(
                    instance.getDefaultConfiguration(resource));
            KRaiderConfiguration conf = confBuilder
                    .recursionDepth(recursionDepth)
                    .nMaxExtractors(128)
                    // .cache(false)
                    // .nMaxExtractors(1)
                    // .sameAsLastObject(false)
                    .buildConfiguration();

            Thread timeoutThread = null;
            if (timeout > 0) {
                timeoutThread = new Thread(() -> {
                    try {
                        TimeUnit.SECONDS.sleep(timeout);
                        instance.stop();
                    } catch (InterruptedException ex) {
                        // do nothing
                        // logger.error(ex.getMessage());
                    }
                });
                timeoutThread.start();
            }

            BlockingQueue<KTriple> result = instance.raidKnowledge(resource, conf, true, true);

            if (timeoutThread != null) {
                timeoutThread.interrupt();
            }
            logger.debug("waiting...");

            instance.getExtractedTriplesStatistics().printStatistics();

            KTriple triple;
            Set<Triple> triples = new HashSet<>();
            while ((triple = result.poll(5, TimeUnit.SECONDS)) != null) {
                // Resource sub =
                // ResourceFactory.createResource(triple.getSubject().toString());
                // RDFNode obj = ResourceFactory.createResource(triple.getObject().toString());
                // RDFNode prop =
                // ResourceFactory.createProperty(triple.getPredicate().toString());

                triples.add(new Triple(triple.getSubject().asNode(), triple.getPredicate().asNode(),
                        triple.getObject().asNode()));
            }

            // logger.info("Unique triples: " + triples.size());

            Model model = ModelFactory.createDefaultModel();
            for (Triple t : triples) {
                RDFNode object;
                if (t.getObject().isURI()) {
                    object = ResourceFactory.createResource(t.getObject().getURI());
                } else {
                    object = ResourceFactory.createPlainLiteral(t.getObject().toString());
                }
                model.add(ResourceFactory.createResource(t.getSubject().getURI()),
                        ResourceFactory.createProperty(t.getPredicate().getURI()),
                        object);
            }
            model.write(output, "TURTLE");
            long endTimeNoCache = System.currentTimeMillis();
            logger.info("Entity {} recursion depth {} time: {}", entity, recursionDepth,
                    (endTimeNoCache - startTime) / 1000.0);
            // System.exit(0);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }
    */
}

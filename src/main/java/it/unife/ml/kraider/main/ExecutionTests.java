/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class ExecutionTests {

    private final static Logger logger = LoggerFactory.getLogger(ExecutionTests.class);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        // TODO code application logic here
        Integer timeoutInt = 24 * 60 * 60;
        String timeout = "" + timeoutInt;

        String[] entities = new String[]{
            // famous cases
            //            "http://dbpedia.org/resource/Angela_Merkel",
            //            "http://dbpedia.org/resource/Leonardo_da_Vinci",
            //            "http://dbpedia.org/resource/Nikola_Tesla",
            // only 10
            "http://dbpedia.org/resource/Michael_Makowski",
            "http://dbpedia.org/resource/Raismes",
            "http://dbpedia.org/resource/Andy_Hicks",
            "http://dbpedia.org/resource/Liftaya",
            "http://dbpedia.org/resource/Gambia_national_basketball_team",
            "http://dbpedia.org/resource/George_Holcombe",
            "http://dbpedia.org/resource/Týniště_nad_Orlicí",
            "http://dbpedia.org/resource/Abandoned_village",
            "http://dbpedia.org/resource/Rodric_Braithwaite",
            "http://dbpedia.org/resource/Navojoa"
// 100
//"http://dbpedia.org/resource/Michael_Makowski",
//"http://dbpedia.org/resource/Raismes",
//"http://dbpedia.org/resource/Jack_Stephens_(footballer)__10",
//"http://dbpedia.org/resource/Majid_Kavian",
//"http://dbpedia.org/resource/Frida_Östberg__7",
//"http://dbpedia.org/resource/Hifuyo_Uchida__1",
//"http://dbpedia.org/resource/Great_Eagle_(car_company)__1913_Model_C__1",
//"http://dbpedia.org/resource/Paul_Robinson_(footballer,_born_1979)__8",
//"http://dbpedia.org/resource/Rungroj_Sawangsri__3",
//"http://dbpedia.org/resource/Manuel_Pérez,_Jr.",
//"http://dbpedia.org/resource/F.C._Vizela__Felipe_Martins__1",
//"http://dbpedia.org/resource/Paulo_Diogo__3",
//"http://dbpedia.org/resource/2009–10_FC_Nistru_season__Andrei_Praporscic__1",
//"http://dbpedia.org/resource/Ragada__Ragada__1",
//"http://dbpedia.org/resource/Abandoned_village",
//"http://dbpedia.org/resource/Baptist_Hospital_of_Miami",
//"http://dbpedia.org/resource/George_Holcombe",
//"http://dbpedia.org/resource/Týniště_nad_Orlicí",
//"http://dbpedia.org/resource/Gambia_national_basketball_team",
//"http://dbpedia.org/resource/Julian_Estrada__1",
//"http://dbpedia.org/resource/Rodric_Braithwaite",
//"http://dbpedia.org/resource/Alfredo_Véa_Jr.__1",
//"http://dbpedia.org/resource/Corethrovalva_goniosema",
//"http://dbpedia.org/resource/Andy_Hicks",
//"http://dbpedia.org/resource/Liftaya",
//
//"http://dbpedia.org/resource/Strenuella",
//"http://dbpedia.org/resource/Stigmella_ilsea",
//"http://dbpedia.org/resource/Gherry_Setya__3",
//"http://dbpedia.org/resource/Sand_River_(Limpopo)",
//"http://dbpedia.org/resource/G_Hannelius__1",
//"http://dbpedia.org/resource/Jesse_Crichton",
//"http://dbpedia.org/resource/Gun_Club_Hill_Barracks",
//"http://dbpedia.org/resource/Mark_Leech__7",
//"http://dbpedia.org/resource/Brendan_James_(album)",
//"http://dbpedia.org/resource/Tendla",
//"http://dbpedia.org/resource/Joseph_Miitamariki__3",
//"http://dbpedia.org/resource/Trade_Centre_Wales",
//"http://dbpedia.org/resource/Michael_Townsend__1",
//"http://dbpedia.org/resource/Greenville,_Georgia",
//"http://dbpedia.org/resource/Sangeeta_N._Bhatia",
//"http://dbpedia.org/resource/Anthony_O'Connor_(footballer)__4",
//"http://dbpedia.org/resource/Donny_White",
//"http://dbpedia.org/resource/Eden_Sharav",
//"http://dbpedia.org/resource/Contumazá_District",
//"http://dbpedia.org/resource/Kapong_District",
//"http://dbpedia.org/resource/Teresa_M._Chafin",
//"http://dbpedia.org/resource/Chris_Moore_(footballer,_born_1984)__8",
//"http://dbpedia.org/resource/Pan_Sutong",
//"http://dbpedia.org/resource/SPB_TV",
//"http://dbpedia.org/resource/Jesús_Graña",
//"http://dbpedia.org/resource/Sarv,_Mahabad",
//"http://dbpedia.org/resource/National_Conference_of_Tripura",
//"http://dbpedia.org/resource/Wiegand_Island",
//"http://dbpedia.org/resource/Montvicq",
//"http://dbpedia.org/resource/Steel_Canyon_Resort,_California",
//"http://dbpedia.org/resource/Rathfern_Rangers_F.C.__Craig_McMurdie__1",
//"http://dbpedia.org/resource/Pingtan_County",
//"http://dbpedia.org/resource/Baseball_at_the_2004_Summer_Olympics",
//"http://dbpedia.org/resource/Barzio",
//"http://dbpedia.org/resource/Steve_Coppell__6",
//"http://dbpedia.org/resource/Quaëdypre",
//"http://dbpedia.org/resource/Rodrigo_Jara__3",
//"http://dbpedia.org/resource/Absdale",
//"http://dbpedia.org/resource/Ballabhpur_Wildlife_Sanctuary",
//"http://dbpedia.org/resource/Matteo_Berretti__3",
//"http://dbpedia.org/resource/Amadeu_Vives_i_Roig",
//"http://dbpedia.org/resource/Graham_Drinkwater",
//"http://dbpedia.org/resource/School_of_Technology,_Hemmatabad",
//"http://dbpedia.org/resource/2013–14_Valencia_CF_season__Salva_Ruiz__1",
//"http://dbpedia.org/resource/National_Association_of_Chicana_and_Chicano_Studies",
//"http://dbpedia.org/resource/Hristu_Chiacu__8",
//"http://dbpedia.org/resource/Danielle_Ryan",
//"http://dbpedia.org/resource/Larry_Estridge",
//"http://dbpedia.org/resource/Navojoa",
//"http://dbpedia.org/resource/Valle_del_Rosario",
//"http://dbpedia.org/resource/Island_of_the_Dead_(2000_film)",
//"http://dbpedia.org/resource/A_Short_History_of_Tractors_in_Ukrainian",
//"http://dbpedia.org/resource/KGIG-LP",
//"http://dbpedia.org/resource/Uwe_Fuchs__17",
//"http://dbpedia.org/resource/Jerry_Sumners_Sr._Aurora_Municipal_Airport",
//"http://dbpedia.org/resource/Charles_B._DeBellevue",
//"http://dbpedia.org/resource/Teitur_Thordarson__7",
//"http://dbpedia.org/resource/Galong,_New_South_Wales",
//"http://dbpedia.org/resource/Mar_Prieto__6",
//"http://dbpedia.org/resource/Park_Point_at_RIT",
//"http://dbpedia.org/resource/Cognitive_deficit",
//"http://dbpedia.org/resource/Normanby_by_Spital",
//"http://dbpedia.org/resource/Cẩm_Vân",
//"http://dbpedia.org/resource/Protesilaus_glaucolaus",
//"http://dbpedia.org/resource/Sergei_Zhigulskiy",
//"http://dbpedia.org/resource/Cedar_Grove_High_School_(Ellenwood,_Georgia)",
//"http://dbpedia.org/resource/Andrei_Zenin__4",
//"http://dbpedia.org/resource/Viktor_Terentiev__13",
//"http://dbpedia.org/resource/Aaron_Brown_(footballer,_born_1980)__13",
//"http://dbpedia.org/resource/Inchiyani",
//"http://dbpedia.org/resource/P.D.A._(rapper)",
//"http://dbpedia.org/resource/National_Science_and_Technology_Council",
//"http://dbpedia.org/resource/Rakuten_Linkshare",
//"http://dbpedia.org/resource/2010_Nebraska_Cornhuskers_football_team",
//"http://dbpedia.org/resource/Sergey_Shustikov_(footballer,_born_1970)__2"
        };

        int count = 0;
        for (int i = 1; i < 3; i++) {
            for (String entity : entities) {
                long startTime = System.currentTimeMillis();
                String output_file = entity.substring(entity.lastIndexOf("/") + 1) + "_" + i;
                Main.main(new String[]{entity, "" + i, timeout, output_file + "_no_cache"});
                long endTimeNoCache = System.currentTimeMillis();
                logger.info("{} {} time: {}", entity, i, (endTimeNoCache - startTime) / 1000.0);
                Main.main(new String[]{entity, "" + i, timeout, output_file + "_cache"});
                long endTimeCache = System.currentTimeMillis();
                logger.info("{} {} time cache: {}", entity, i, (endTimeCache - endTimeNoCache) / 1000.0);
                FileUtils.deleteDirectory(new File(".cache"));
                count++;
                logger.info("Count: {}", count);
            }
        }
    }

}

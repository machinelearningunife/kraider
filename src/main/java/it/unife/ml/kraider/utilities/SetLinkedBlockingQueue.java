/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.utilities;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
/**
 * Linked blocking queue with {@link #add(Object)} method, which adds only
 * element, that is not already in the queue.
 */
public class SetLinkedBlockingQueue<T> extends LinkedBlockingQueue<T> {

    private Set<T> set = new HashSet<>();

    /**
     * Add only element, that is not already enqueued. The method is
     * synchronized, so that the duplicate elements can't get in during race
     * condition.
     *
     * @param t object to put in
     * @return true, if the queue was changed, false otherwise
     */
    @Override
    public synchronized boolean add(T t) {
        if (set.contains(t)) {
            return false;
        } else {
            if (super.add(t)) {
                set.add(t);
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public synchronized boolean offer(T t) {
        if (set.contains(t)) {
            return false;
        } else {
            if (super.offer(t)) {
                set.add(t);
                return true;
            } else {
                return false;
            }
        }
    }

    public synchronized boolean offer(T t,
            long timeout,
            TimeUnit unit)
            throws InterruptedException {
        if (set.contains(t)) {
            return false;
        } else {
            if (super.offer(t, timeout, unit)) {
                set.add(t);
                return true;
            }
            return false;
        }
    }

    /**
     * Takes the element from the queue. Note that no synchronization with
     * {@link #add(Object)} is here, as we don't care about the element staying
     * in the set longer needed.
     *
     * @return taken element
     * @throws InterruptedException
     */
    @Override
    public T take() throws InterruptedException {
        T t = super.take();
        if (t != null) {
            set.remove(t);
        }
        return t;
    }

    @Override
    public T poll() {
        T t = super.poll();
        if (t != null) {
            set.remove(t);
        }
        return t;
    }

    @Override
    public T poll(long timeout, TimeUnit unit) throws InterruptedException {
        T t = super.poll(timeout, unit);
        if (t != null) {
            set.remove(t);
        }
        return t;
    }
}

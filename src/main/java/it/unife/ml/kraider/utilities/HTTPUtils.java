/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.utilities;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class HTTPUtils {

    private static final Logger logger = LoggerFactory.getLogger(HTTPUtils.class);

    /**
     * Http HEAD Method to get URL content type
     *
     * @param urlString
     * @return content type
     * @throws IOException
     */
    public static String getContentType(String urlString) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("HEAD");
        if (isRedirect(connection.getResponseCode())) {
            String newUrl = connection.getHeaderField("Location"); // get redirect url from "location" header field
            logger.debug("Original request URL: '{}' redirected to: '{}'", urlString, newUrl);
            return getContentType(newUrl);
        }
        String contentType = connection.getContentType();
        return contentType;
    }

    /**
     * Http HEAD Method to get URL content type
     *
     * @param urlString
     * @return content type
     * @throws IOException
     */
    public static String getContentType(URL url, String accept) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("HEAD");
        connection.addRequestProperty("ACCEPT", accept);
        if (isRedirect(connection.getResponseCode())) {
            String newUrl = connection.getHeaderField("Location"); // get redirect url from "location" header field
            logger.debug("Original request URL: '{}' redirected to: '{}'", url, newUrl);
            return getContentType(newUrl);
        }
        String contentType = connection.getContentType();
        return contentType;
    }

    /**
     * Check status code for redirects
     *
     * @param statusCode
     * @return true if matched redirect group
     */
    protected static boolean isRedirect(int statusCode) {
        if (statusCode != HttpURLConnection.HTTP_OK) {
            if (statusCode == HttpURLConnection.HTTP_MOVED_TEMP
                    || statusCode == HttpURLConnection.HTTP_MOVED_PERM
                    || statusCode == HttpURLConnection.HTTP_SEE_OTHER) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean isLocal(URL url) {
        return url.getProtocol().equalsIgnoreCase("file");
//        String urlStr = url.toString();
//        String pattern = "\\w+://";
//        if (urlStr.matches(pattern)) {
//            return url.getProtocol().equalsIgnoreCase("file");
//        } else {
//            return true;
//        }
    }
    
    public static boolean remoteURLExists(URL url) throws MalformedURLException, IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("HEAD");
        if (isRedirect(connection.getResponseCode())) {
            URL newUrl = new URL(connection.getHeaderField("Location")); // get redirect url from "location" header field
            logger.debug("Original request URL: '{}' redirected to: '{}'", url, newUrl);
            return remoteURLExists(newUrl);
        } else {
            return connection.getResponseCode() == HttpURLConnection.HTTP_OK;
        }
    }

    public static URL extractDomain(URL url) throws MalformedURLException {
        String protocol = url.getProtocol();
        String host = url.getHost();
        if (protocol.equalsIgnoreCase("http") || protocol.equalsIgnoreCase("https")) {
            return new URL(protocol + "://" + host);
            //        return new URL(host);
        } else {
            return url;
        }
    }
    
    public static boolean checkDocumentExistence(URL domain) throws IOException {
        if (HTTPUtils.isLocal(domain)) {
            File f = new File(domain.getPath());
            return f.exists();
        } else {
            return HTTPUtils.remoteURLExists(domain);
        }
    }

}

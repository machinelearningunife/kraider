package it.unife.ml.kraider.activator;

import it.unife.ml.kraider.api.KnowledgeRaider;
import it.unife.ml.kraider.api.TripleExtractorFactory;
import it.unife.ml.kraider.core.KnowledgeRaiderImpl;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Activator implements BundleActivator, ServiceListener {
    
    private CopyOnWriteArrayList<TripleExtractorFactory> factoryQueue;
    
    private final Logger logger = LoggerFactory.getLogger(Activator.class);

    @Override
    public void start(BundleContext context) throws Exception {
        //System.out.println("Avvio KRaider");
        logger.info("Avvio KRaider");
        factoryQueue = new CopyOnWriteArrayList<>();
        
        // register service
        context.registerService(
                KnowledgeRaider.class.getName(), 
                new KnowledgeRaiderImpl(factoryQueue), null);
        
        
        
        // TODO add activation code here
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

    @Override
    public void serviceChanged(ServiceEvent se) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

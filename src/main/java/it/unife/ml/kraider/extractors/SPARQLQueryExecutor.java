/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.extractors;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
import it.unife.ml.kraider.exception.UnsupportedRDFSyntaxException;
import it.unife.ml.kraider.extractors.Endpoint;
import it.unife.ml.kraider.extractors.ldd.LDDModelReader;
import it.unife.ml.kraider.extractors.sparql.SPARQLEndpoint;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import javax.xml.ws.http.HTTPException;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFactory;
import org.apache.jena.query.ResultSetRewindable;
import org.apache.jena.rdf.model.Model;
//import org.apache.jena.rdf.model.ModelFactory;
//import org.apache.jena.sparql.engine.http.HttpQuery;
//import org.apache.jena.sparql.engine.http.QueryEngineHTTP;
import org.apache.jena.sparql.exec.http.QueryExecutionHTTP;
import org.apache.jena.sparql.exec.http.QueryExecutionHTTPBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface SPARQLQueryExecutor {

    static final Logger logger = LoggerFactory.getLogger(SPARQLQueryExecutor.class);
    
    public ResultSetRewindable executeQuery(String sparqlQueryString, Endpoint sparqlEndpoint)
            throws IOException, UnsupportedRDFSyntaxException, HTTPException;

    static SPARQLQueryExecutor sparqlEndpointQueryExecutor() {
        return (sparqlQueryString, sparqlEndpoint) -> {
            String service = sparqlEndpoint.getURL().toString();
            List<String> defaultGraphs = ((SPARQLEndpoint) sparqlEndpoint).getDefaultGraphIRIs();
            List<String> namedGraphs = ((SPARQLEndpoint) sparqlEndpoint).getNamedGraphIRIs();

            Query query = QueryFactory.create(sparqlQueryString);

            try {
                QueryExecutionHTTPBuilder qexecb = QueryExecutionHTTP.service(service).query(query);
                for ( String dgu : defaultGraphs) qexecb.addDefaultGraphURI(dgu);
                for ( String ngu : defaultGraphs) qexecb.addNamedGraphURI(ngu);
//                         QueryExecutionFactory.sparqlService(service, query, defaultGraphs, namedGraphs)) {
//            qexec.setTimeout(60, TimeUnit.SECONDS);

                QueryExecution qexec = qexecb.build();

                logger.debug("sending query: length: " + sparqlQueryString.length() + " | ENDPOINT: "
                        + sparqlEndpoint.getURL().toString());
                ResultSet results = qexec.execSelect();
                ResultSetRewindable rs = ResultSetFactory.copyResults(results);
                return rs;
            } catch (HTTPException e) {
                logger.error("HTTPException in SparqlQuery\n" + e.toString());
                logger.error("query was " + sparqlQueryString);
                throw e;
                // TODO: RuntimeException is very general; is it possible to catch more specific exceptions?
            } catch (RuntimeException e) {
                if (logger.isDebugEnabled()) {
                    logger.error("RuntimeException in SparqlQuery: "
                            + e.toString());
                    int length = Math.min(sparqlQueryString.length(), 300);
                    logger.error("query was (max. 300 chars displayed) "
                            + sparqlQueryString.substring(0, length - 1));
                }
                throw e;
            }
        };
    }

    static SPARQLQueryExecutor sparqlLDDQueryExecutor(ModelReader modelReader) {
        return (sparqlQueryString, lddEndpoint) -> {
            String service = lddEndpoint.getURL().toString();
            // The RDF model of the IRI
            Model model;
            try {
                model = modelReader.retrieveModel(service);
            } catch (IOException ioe) {
                throw ioe;
            } catch (UnsupportedRDFSyntaxException ex) {
                throw ex;
            }

            Query query = QueryFactory.create(sparqlQueryString);

            try (QueryExecution qexec = QueryExecutionFactory.create(query, model)) {
//            qexec.setTimeout(60, TimeUnit.SECONDS);
                logger.debug("executing query: length: " + sparqlQueryString.length() + " | ENDPOINT: "
                        + lddEndpoint.getURL().toString());
                ResultSet results = qexec.execSelect();
                ResultSetRewindable rs = ResultSetFactory.copyResults(results);
                return rs;
            } catch (HTTPException e) {
                logger.error("HTTPException in SparqlQuery\n" + e.toString());
                logger.error("query was " + sparqlQueryString);
                throw e;
                // TODO: RuntimeException is very general; is it possible to catch more specific exceptions?
            } catch (RuntimeException e) {
                if (logger.isDebugEnabled()) {
                    logger.error("RuntimeException in SparqlQuery: "
                            + e.toString());
                    int length = Math.min(sparqlQueryString.length(), 300);
                    logger.error("query was (max. 300 chars displayed) "
                            + sparqlQueryString.substring(0, length - 1));
                }
                throw e;
            }
        };

    }

}

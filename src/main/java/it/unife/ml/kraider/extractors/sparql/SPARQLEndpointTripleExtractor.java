/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.extractors.sparql;

import it.unife.ml.kraider.api.TripleExtractorManager;
import it.unife.ml.kraider.core.AbstractTripleExtractor;
import it.unife.ml.kraider.data.Job;
import it.unife.ml.kraider.data.JobStatus;
import it.unife.ml.kraider.exception.InvalidRecursionDepthException;
import it.unife.ml.kraider.extractors.JobCreator;
import it.unife.ml.kraider.extractors.SPARQLQueryExecutor;
import java.util.Collections;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import it.unife.ml.kraider.api.KTriple;

/**
 * Objects of this class can extract new triples from SPARQL Endpoints.
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class SPARQLEndpointTripleExtractor extends AbstractTripleExtractor {

    private static final Logger logger = LoggerFactory.getLogger(SPARQLEndpointTripleExtractor.class);

    private boolean stop = false;

    private final SPARQLQueryMaker queryMaker = new SPARQLQueryMaker();

    private SPARQLTask task;

    private SPARQLEndpoints endpoints;

    public SPARQLEndpointTripleExtractor(int id, TripleExtractorManager manager) {
        super(id, manager);
        endpoints = manager.getConfiguration().getSparqlEndpoints();
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void terminateExtraction() {
        stop = true;
    }

    @Override
    public void run() {
        running = true;
        // require first job
        Job job = manager.requireJob(id, null, null, null, null);

        while (!stopCriteria()) {
            logger.info("{} with id: {}: executing job: subject: {}, domain: {}, filters: {}",
                    this.getClass().getSimpleName(), id,
                    job.getSubject(), job.getDomain(), job.getFilters());
            Set<KTriple> triples = null;
            Set<Job> newJobs = null;
            JobStatus.Status status = JobStatus.Status.FAILURE;
            try {
                if (job.getRecursionDepth() < 0) {
                    String msg = "Invalid Recursion depth: " + job.getRecursionDepth();
                    logger.error(msg);
                    throw new InvalidRecursionDepthException(msg);
                }
                SPARQLEndpoint endpoint = endpoints.getSPARQLEndpointByDomain(job.getDomain());
                if (endpoint != null) {
                    boolean d = job.getFilters().getObjectForbidFilterSet().size() > 0;
                    task = new SPARQLTask(SPARQLQueryExecutor.sparqlEndpointQueryExecutor(),
                            queryMaker, job.getFilters(), endpoint, this.getClass());
                    if (job.getRecursionDepth() == 0) {
                        if (manager.getConfiguration().isSameAsLastObject()) {
                            // d = job.getSubject().toString().equals("http://data.europa.eu/euodp/jrc-names/Angela_Kasner");
                            logger.debug("Executing task: ( {}, equivalent, {}, {} )",
                                    job.getSubject(),
                                    job.getDomain(),
                                    job.getRecursionDepth());
                            triples = task.raidSameAs(job.getSubject());
                            triples.addAll(task.raidEquivalentClasses(job.getSubject()));
                            triples.addAll(task.raidEquivalentProperty(job.getSubject()));
                        } else {
                            triples = Collections.emptySet();
                        }
                    } else {
                        try {
                            logger.debug("Executing task: ( {}, {}, {} )",
                                    job.getSubject(),
                                    job.getDomain(),
                                    job.getRecursionDepth());
                            triples = task.raidEntityFragment(job.getSubject(), 1);
                        } catch (InvalidRecursionDepthException irde) {
                            triples = Collections.emptySet();
                            logger.error(irde.getMessage());
                        }
                    }
                    newJobs = JobCreator.createJobsFromTriples(manager.getConfiguration(), job, triples);
                    status = JobStatus.Status.COMPLETED;
//                    job = manager.requireJob(id, triples, job, status, newJobs);
                } else {
                    // endpoint not found!
                    status = JobStatus.Status.FAILURE;
                    triples = null;
                    newJobs = null;
//                    job = manager.requireJob(id, null, job, JobStatus.Status.FAILURE, null);
                }
            } catch (Exception ex) {
                // An unexpected exception occurred
                logger.error("{} with id: {}: ERROR: {}", this.getClass(), this.getId(), ex.getMessage());
                // logger.error(ex.getMessage());
                status = JobStatus.Status.FAILURE;
                triples = null;
                newJobs = null;
//                job = manager.requireJob(id, null, job, JobStatus.Status.FAILURE, null);
            } finally {
                if (!stopCriteria()) {
                    logger.info("{} with id: {}: requesting for another job",
                            this.getClass().getSimpleName(), id);
                    job = manager.requireJob(id, triples, job, status, newJobs);
                }
            }
        }

        running = false;

    }

    private boolean stopCriteria() {
        return stop;
    }

}

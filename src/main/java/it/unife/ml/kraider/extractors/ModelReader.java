/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.extractors;

import it.unife.ml.kraider.exception.UnsupportedRDFSyntaxException;
import java.io.IOException;
import org.apache.jena.rdf.model.Model;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface ModelReader {
    
    public Model retrieveModel(String iri) throws IOException, UnsupportedRDFSyntaxException ;
    
}

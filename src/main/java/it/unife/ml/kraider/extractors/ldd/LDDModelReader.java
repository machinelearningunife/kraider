/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.extractors.ldd;

import it.unife.ml.kraider.exception.UnsupportedRDFSyntaxException;
import it.unife.ml.kraider.extractors.ModelReader;
import it.unife.ml.kraider.utilities.HTTPUtils;
import java.io.IOException;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RDFLanguages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class LDDModelReader implements ModelReader {

    private static final Logger logger = LoggerFactory.getLogger(LDDModelReader.class);

    public enum SupportedSyntaxes {
        CSV("text/csv", RDFLanguages.strLangCSV,
                Arrays.asList(new String[]{".csv"})),
        LDJSON("application/ld+json", RDFLanguages.strLangJSONLD,
                Arrays.asList(new String[]{".jsonld"})),
        N3("text/n3", RDFLanguages.strLangN3,
                Arrays.asList(new String[]{".n3"})),
        NQUADS("application/n-quads", RDFLanguages.strLangNQuads,
                Arrays.asList(new String[]{".nq"})),
        NTRIPLES("application/n-triples", RDFLanguages.strLangNTriples,
                Arrays.asList(new String[]{".nt"})),
        RDFJSON("application/json", RDFLanguages.strLangRDFJSON,
                Arrays.asList(new String[]{".rj"})),
        RDFTHRIFT("application/rdf+thrift", RDFLanguages.strLangRDFTHRIFT,
                Arrays.asList(new String[]{""})),
        TSV("text/tab-separated-values", RDFLanguages.strLangTSV,
                Arrays.asList(new String[]{""})),
        TriG("application/trig", RDFLanguages.strLangTriG,
                Arrays.asList(new String[]{""})),
        TriX("application/trix+xml", RDFLanguages.strLangTriX,
                Arrays.asList(new String[]{""})),
        RDFXML("application/rdf+xml", RDFLanguages.strLangRDFXML,
                Arrays.asList(new String[]{""})),
        TURTLE("text/turtle", RDFLanguages.strLangTurtle,
                Arrays.asList(new String[]{""}));

        private final String httpContentType;
        private final String jenaSyntax;

        SupportedSyntaxes(String httpContentType, String jenaSyntax, List<String> extension) {
            this.httpContentType = httpContentType;
            this.jenaSyntax = jenaSyntax;
        }

        public String getHTTPContentType() {
            return this.httpContentType;
        }
    }

    private static String acceptString;

    static {
        EnumSet.allOf(SupportedSyntaxes.class)
                .forEach(syntax -> LDDModelReader.acceptString += syntax.getHTTPContentType() + ",");
        // remove last comma
        LDDModelReader.acceptString = LDDModelReader.acceptString.substring(0, LDDModelReader.acceptString.length() - 1);

    }

    public Model retrieveModel(String iri) throws IOException, UnsupportedRDFSyntaxException {
        // The RDF model of the IRI
        Model model = ModelFactory.createDefaultModel();

//        Thread t = new Thread(() -> {
//            try {
//                TimeUnit.SECONDS.sleep(60 * 2);
//                model.abort();
//            } catch (InterruptedException ex) {
//                // do nothing
//            }
//        });
//        t.start();

        model.read(iri);
//        t.interrupt();

//        /* First check the IRI file extension */
//        if (iri.toLowerCase().endsWith(".ntriples") || iri.toLowerCase().endsWith(".nt")) {
//            logger.debug("# Reading a N-Triples file...");
//            model.read(iri, "N-TRIPLE");
//        } else if (iri.toLowerCase().endsWith(".n3")) {
//            logger.debug("# Reading a Notation3 (N3) file...");
//            model.read(iri);
//
//        } else if (iri.toLowerCase().endsWith(".json") || iri.toLowerCase().endsWith(".jsod") || iri.toLowerCase().endsWith(".jsonld")) {
//            logger.debug("# Trying to read a 'json-ld' file...");
//            model.read(iri, "JSON-LD");
//        } else {
//            String contentType = HTTPUtils.getContentType(iri, acceptString).split(";")[0]; // get the IRI content type
//            logger.debug("# IRI Content Type: " + contentType);
//            SupportedSyntaxes syntax = EnumSet.allOf(SupportedSyntaxes.class)
//                    .stream()
//                    .filter(elem -> elem.httpContentType.equalsIgnoreCase(contentType)).findFirst().orElse(null);
//            logger.debug("Trying to read {} file", syntax.jenaSyntax);
//            if (syntax == null) {
//                throw new UnsupportedRDFSyntaxException("Unsupported RDF Syntax: " + contentType);
//            }
//            model.read(iri, syntax.jenaSyntax);
//        }
        return model;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.extractors.sparql;

import java.net.URL;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class SPARQLEndpoints {

    private final ConcurrentHashMap<String, SPARQLEndpoint> endpointsMapByDomain = new ConcurrentHashMap<>();

    private final ConcurrentHashMap<String, SPARQLEndpoint> endpointsMapByName = new ConcurrentHashMap<>();

    public SPARQLEndpoints(List<SPARQLEndpoint> endpointList) {
        endpointList.forEach((endpoint) -> {
            URL domainURL = endpoint.getDomain();
            String domain = domainURL.getHost() + domainURL.getPath();
            endpointsMapByDomain.put(domain, endpoint);
            endpointsMapByName.put(endpoint.getName().toUpperCase(), endpoint);
        });
    }

    public SPARQLEndpoint getSPARQLEndpointByName(String name) {
        return endpointsMapByName.get(name.toUpperCase());
    }

    public SPARQLEndpoint getSPARQLEndpointByDomain(URL domain) {
        String domainStr = domain.getHost() + domain.getPath();
        return endpointsMapByDomain.get(domainStr);
    }

//    public SPARQLEndpoint getSPARQLEndpointByResource(URL subject) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }

}

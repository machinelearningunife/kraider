/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.extractors.sparql;

import it.unife.ml.kraider.data.Filters;
import it.unife.ml.kraider.exception.InvalidRecursionDepthException;
import it.unife.ml.kraider.core.QueryMaker;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.apache.jena.query.ParameterizedSparqlString;
import org.apache.jena.shared.PrefixMapping;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.vocab.OWLRDFVocabulary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class used to make SPARQL query strings.
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class SPARQLQueryMaker implements QueryMaker {

    private static final Logger logger = LoggerFactory.getLogger(SPARQLQueryMaker.class);

    public static final String SUBJECT_SAME = "subject_same";
    public static final String PREDICATE = "predicate";
    public static final String PREDICATE_SAME = "predicate_same";
    public static final String OBJECT = "object";
    public static final String OBJECT_SAME = "object_same";

    private static final String LINEEND = "\n";

//    protected Filters filters;
    public SPARQLQueryMaker() {
//        this.filters = Filters.getDefaultFilters();
    }

//    public SPARQLQueryMaker(Filters filters) {
//
//        this.filters = filters;
//        Filters defaultFilters = Filters.getDefaultFilters();
//        
//        defaultFilters.getPredicateAllowFilterSet().forEach((filter) -> {
//            filters.addPredicateAllowFilter(filter);
//        });
//        
//        defaultFilters.getPredicateForbidFilterSet().forEach((filter) -> {
//            filters.addPredicateForbidFilter(filter);
//        });
//        
//        defaultFilters.getObjectAllowFilterSet().forEach((filter) -> {
//            filters.addObjectAllowFilter(filter);
//        });
//        
//        defaultFilters.getObjectForbidFilterSet().forEach((filter) -> {
//            filters.addObjectForbidFilter(filter);
//        });
//    }
    @Override
    public String makeSameAsQueryString(IRI resource, Filters filters) {

        String filter = createEntityFilterString(SUBJECT_SAME,
                filters.getObjectAllowFilterSet(),
                filters.getObjectForbidFilterSet());
        filter = (filter.length() > 0) ? "FILTER ( " + filter + " ) ." : " ";

        StringBuilder sbuff = new StringBuilder(400);
        sbuff.append("SELECT * WHERE { " + LINEEND + "<").append(resource).append(">");
        sbuff.append(" (owl:sameAs|^owl:sameAs)+ ?").append(SUBJECT_SAME).append(" .").append(LINEEND);
        sbuff.append(filter).append(LINEEND);
        sbuff.append("} ");

        ParameterizedSparqlString pss = new ParameterizedSparqlString();
        pss.setCommandText(sbuff.toString().trim());
        pss.setNsPrefixes(PrefixMapping.Standard);

        return pss.toString();
    }

    @Override
    public String makeEquivalentClassesQueryString(IRI resource, Filters filters) {

        String filter = createEntityFilterString(SUBJECT_SAME,
                filters.getObjectAllowFilterSet(),
                filters.getObjectForbidFilterSet());
        filter = (filter.length() > 0) ? "FILTER ( " + filter + " ) ." : " ";

        StringBuilder sbuff = new StringBuilder(400);
        sbuff.append("SELECT * WHERE { " + LINEEND + "<").append(resource).append(">");
        sbuff.append(" (owl:equivalentClass|^owl:equivalentClass)+ ?").append(SUBJECT_SAME).append(" .").append(LINEEND);
        sbuff.append(filter).append(LINEEND);
        sbuff.append(" } ");

        ParameterizedSparqlString pss = new ParameterizedSparqlString();
        pss.setCommandText(sbuff.toString().trim());
        pss.setNsPrefixes(PrefixMapping.Standard);

        return pss.toString();
    }

    @Override
    public String makeEquivalentPropertyQueryString(IRI resource, Filters filters) {

        String filter = createEntityFilterString(SUBJECT_SAME,
                filters.getObjectAllowFilterSet(),
                filters.getObjectForbidFilterSet());
        filter = (filter.length() > 0) ? "FILTER ( " + filter + " ) ." : " ";

        StringBuilder sbuff = new StringBuilder(400);
        sbuff.append("SELECT * WHERE { " + LINEEND + "<").append(resource).append(">");
        sbuff.append(" (owl:equivalentProperty|^owl:equivalentProperty)+ ?").append(SUBJECT_SAME).append(" .").append(LINEEND);
        sbuff.append(filter).append(LINEEND);
        sbuff.append("} ");

        ParameterizedSparqlString pss = new ParameterizedSparqlString();
        pss.setCommandText(sbuff.toString().trim());
        pss.setNsPrefixes(PrefixMapping.Standard);

        return pss.toString();
    }

    @Override
    public String makeEntityFragmentQueryString(IRI subject, IRI predicate, Filters filters, int recursionDepth)
            throws InvalidRecursionDepthException {
        if (recursionDepth > 0) {
            StringBuilder sb = new StringBuilder(1400);
            //sbuff.append("SELECT * WHERE { " + lineend + "{<").append(subject).append(">");
            sb.append("SELECT DISTINCT * WHERE { ").append(LINEEND);
            sb.append("<").append(subject).append(">");
//            sbuff.append(" (owl:sameAs|^owl:sameAs)* ?").append(SUBJECT_SAME).append(" .").append(LINEEND);
//            sbuff.append(" ?").append(SUBJECT_SAME);

            String filter = "";
            String objectVar = OBJECT + "0";
            String predicateVar = PREDICATE + "0";
            if (predicate != null) {
                sb.append(" <").append(predicate.toString()).append(">");
                // create filter string for object
                filter = createEntityFilterString(objectVar,
                        filters.getObjectAllowFilterSet(),
                        filters.getObjectForbidFilterSet());
            } else {
                sb.append(" ?").append(PREDICATE).append("0 ");
                filter = createFilterString(predicateVar, objectVar, filters);
            }
            sb.append(" ?").append(OBJECT).append("0 .").append(LINEEND);
            //sbuff.append("} ").append(lineend);

            filter = (filter.length() > 0) ? "FILTER ( " + filter + " ) ." : " ";
            sb.append(filter).append(LINEEND);

            for (int i = 1; i < recursionDepth; i++) {
                sb.append("OPTIONAL { " + LINEEND);
                sb.append(" ?").append(OBJECT).append(i - 1)
                        .append(" ?").append(PREDICATE).append(i)
                        .append(" ?").append(OBJECT).append(i).append(" . ").append(LINEEND);

                objectVar = OBJECT + i;
                predicateVar = PREDICATE + i;
                filter = createFilterString(predicateVar, objectVar, filters);
                filter = (filter.length() > 0) ? "FILTER ( " + filter + " ) ." : " ";
                sb.append(filter).append(LINEEND);

            }

            for (int i = 1; i < recursionDepth; i++) {
                sb.append(" }" + LINEEND);
            }

            sb.append(LINEEND + "} ");

            ParameterizedSparqlString pss = new ParameterizedSparqlString();
            pss.setCommandText(sb.toString().trim());
            pss.setNsPrefixes(PrefixMapping.Standard);

            return pss.toString();
        } else {
            String msg = "Recursion depth is less than 1: " + recursionDepth;
            logger.error(msg);
            throw new InvalidRecursionDepthException(msg);
        }
    }

    @Override
    public String makeEntityFragmentQueryString(IRI subject, Filters filters, int recursionDepth)
            throws InvalidRecursionDepthException {
        return makeEntityFragmentQueryString(subject, null, filters, recursionDepth);
    }

    public String makeEntitiesFragmentQueryString(SortedSet<IRI> subjects, IRI predicate, Filters filters, int recursionDepth)
            throws InvalidRecursionDepthException {
        if (recursionDepth > 0) {
            StringBuilder sb = new StringBuilder(1400);
            sb.append("SELECT DISTINCT * WHERE { ").append(LINEEND);

            String predicateStr;
            if (predicate != null) {
                predicateStr = " <" + predicate.toString() + ">";
            } else {
                predicateStr = " ?" + PREDICATE + "0 ";
            }

            IRI firstSubject = subjects.stream().findFirst().get();
            sb.append("{ <").append(firstSubject).append(">");
            sb.append(predicateStr);
            sb.append(" ?").append(OBJECT).append("0 . }").append(LINEEND);

            /*subjects.stream().skip(1).forEach((subject) -> {
                sb.append("UNION").append(LINEEND);
                sb.append("{ <").append(subject).append(">");
                sb.append(predicateStr);
                sb.append(" ?").append(OBJECT).append("0 . }").append(LINEEND);
            });*/

            String filter = "";
            String objectVar = OBJECT + "0";
            String predicateVar = PREDICATE + "0";

            if (predicate != null) {
                filter = createEntityFilterString(objectVar,
                        filters.getObjectAllowFilterSet(),
                        filters.getObjectForbidFilterSet());
            } else {
                filter = createFilterString(predicateVar, objectVar, filters);
            }

            filter = (filter.length() > 0) ? "FILTER ( " + filter + " ) ." : " ";
            sb.append(filter).append(LINEEND);

            for (int i = 1; i < recursionDepth; i++) {
                sb.append("OPTIONAL { " + LINEEND);
                sb.append(" ?").append(OBJECT).append(i - 1)
                        .append(" ?").append(PREDICATE).append(i)
                        .append(" ?").append(OBJECT).append(i).append(" . ").append(LINEEND);

                objectVar = OBJECT + i;
                predicateVar = PREDICATE + i;
                filter = createFilterString(predicateVar, objectVar, filters);
                filter = (filter.length() > 0) ? "FILTER ( " + filter + " ) ." : " ";
                sb.append(filter).append(LINEEND);

            }

            for (int i = 1; i < recursionDepth; i++) {
                sb.append(" }" + LINEEND);
            }

            sb.append(LINEEND + "} ");

            ParameterizedSparqlString pss = new ParameterizedSparqlString();
            pss.setCommandText(sb.toString().trim());
            pss.setNsPrefixes(PrefixMapping.Standard);

            return pss.toString();
        } else {
            String msg = "Recursion depth is less than 1: " + recursionDepth;
            logger.error(msg);
            throw new InvalidRecursionDepthException(msg);
        }
    }

    public String makeEntitiesFragmentQueryString(SortedSet<IRI> subjects, Filters filters, int recursionDepth)
            throws InvalidRecursionDepthException {
        return makeEntitiesFragmentQueryString(subjects, null, filters, recursionDepth);
    }

    private String createFilterString(String predicateVariable, String objectVariable, Filters filters) {

        String predicateFilterString = createEntityFilterString(predicateVariable,
                filters.getPredicateAllowFilterSet(),
                filters.getPredicateForbidFilterSet());
        String objectFilterString = createEntityFilterString(objectVariable,
                filters.getObjectAllowFilterSet(),
                filters.getObjectForbidFilterSet());

        Set<String> combinedFilters = new TreeSet<>();

        combinedFilters.add(predicateFilterString);
        combinedFilters.add(objectFilterString);

        return assembleTerms(combinedFilters, "&&");
    }

    private String createEntityFilterString(String variable, Set<String> allowFilterSet, Set<String> forbidFilterSet) {
        variable = (variable.startsWith("?")) ? variable
                : "?" + variable;

        SortedSet<String> allowFilters = new TreeSet<>();
        SortedSet<String> forbidFilters = new TreeSet<>();

        for (String obj : allowFilterSet) {
            allowFilters.add("REGEX( STR(" + variable + "), '" + obj
                    + "')");
        }

        for (String obj : forbidFilterSet) {
            forbidFilters.add("!REGEX( STR(" + variable + "), '" + obj
                    + "')");
        }

        String allowFilterString = assembleTerms(allowFilters, "||");
        String forbidFilterString = assembleTerms(forbidFilters, "&&");

        Set<String> filters = new TreeSet<>();

        if (!allowFilterSet.isEmpty()) {
            filters.add(allowFilterString);
        }

        if (!forbidFilterSet.isEmpty()) {
            filters.add(forbidFilterString);
        }

        return assembleTerms(filters, "&&");

    }

    private String assembleTerms(Set<String> terms, String operator) {
        if ((!operator.equals("||")) && (!operator.equals("&&"))) {
            String msg = "Wrong filter operator";
            logger.error(msg);
            throw new IllegalArgumentException(msg);
        }

        if (terms.isEmpty()) {
            return "";
        } else {
            StringBuilder sbuf = new StringBuilder(1400);
            String first = terms.stream().findFirst().get();
            if (terms.size() > 1) {
                sbuf.append("(");
            }
            sbuf.append(first).append(LINEEND);

            terms.stream().skip(1).forEach((term) -> {
                sbuf.append(operator).append(" ");
//                sbuf.append("(").append(term).append(")").append(LINEEND);
                sbuf.append(term).append(LINEEND);
            });

            if (terms.size() > 1) {
                sbuf.append(")");
            }
            return sbuf.toString();
        }

    }

//    /**
//     * @return the filters
//     */
//    public Filters getFilters() {
//        return filters;
//    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.extractors;

import it.unife.ml.kraider.api.KRaiderConfiguration;
import it.unife.ml.kraider.data.Filters;
import it.unife.ml.kraider.data.Job;
import it.unife.ml.kraider.data.JobStatus;
import it.unife.ml.kraider.data.NamedResourceJob;
import it.unife.ml.kraider.utilities.HTTPUtils;
import it.unife.ml.kraider.utilities.Utilities;
import static it.unife.ml.probowlapi.utilities.GeneralUtils.safe;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.semanticweb.owlapi.model.IRI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import it.unife.ml.kraider.api.KTriple;
import it.unife.ml.kraider.api.KTripleNode;

/**
 *
 * It creates jobs from a set of triples. WARNING! It works only if the
 * extractions query had recusionDepth equal to 1
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class JobCreator {

    private static final Logger logger = LoggerFactory.getLogger(JobCreator.class);

    public static Set<Job> createJobsFromTriples(KRaiderConfiguration conf, Job prevJob, Set<KTriple> triples) {
        Set<Job> newJobs = new LinkedHashSet<>();

        int prevJobRecursionDepth = prevJob.getRecursionDepth();

        Map<String, Set<IRI>> domainEquivEntityMap = createDomainEquivEntityMap(triples);

        for (KTriple triple : safe(triples)) {
            KTripleNode nextSubject = triple.getObject();

            if (nextSubject.isURIResource()) {
                IRI nextSubjectIRI;
                URL nextSubjectDomain;

                try {
                    nextSubjectIRI = IRI.create(nextSubject.toString());
                    nextSubjectDomain = HTTPUtils.extractDomain(nextSubjectIRI.toURI().toURL());
                } catch (MalformedURLException murle) {
                    logger.error(murle.getLocalizedMessage());
                    continue;
                }
                //boolean d = nextSubjectIRI.toString().equals("http://www.wikidata.org/entity/statement/Q2538-55df23f0-4703-d651-866d-e9d8e86a7328");
                if (triple.isSameAs() || triple.isEquivalentClass() || triple.isEquivalentProperty()) {

                    if (!prevJob.getDomain().equals(nextSubjectDomain)
                            && (prevJobRecursionDepth > 0 || conf.isSameAsLastObject())) {
                        // filters for owl:sameAs, owl:equivalentClass and owl:equivalentProperty queries
                        Filters sameEquivFilters = createFilters(nextSubjectIRI, nextSubjectDomain, domainEquivEntityMap);

                        Job newJob = new NamedResourceJob(
                                nextSubjectDomain,
                                nextSubjectIRI,
                                prevJobRecursionDepth,
                                new JobStatus(),
                                sameEquivFilters);
                        newJobs.removeIf((item) -> (item.equals(newJob)
                                && item.getRecursionDepth() < newJob.getRecursionDepth()));
                        newJobs.add(newJob);

                    }
//                    newJobs.add(new Job(
//                            job.getDomain(),
//                            nextSubjectURL,
//                            null,
//                            prevJobRecursionDepth,
//                            new JobStatus()));

                } else {
                    if (prevJobRecursionDepth > 0) {
                        newJobs.add(new NamedResourceJob(
                                prevJob.getDomain(),
                                nextSubjectIRI,
                                prevJobRecursionDepth - 1,
                                new JobStatus()));

                        newJobs.add(new NamedResourceJob(
                                nextSubjectDomain,
                                nextSubjectIRI,
                                prevJobRecursionDepth - 1,
                                new JobStatus()));
                    }
                }
            }

        }

        return newJobs;
    }

    /**
     * It takes the objects of triples where the predicate is owl:isSameAs,
     * owl:EquivalentClass and owl:EquivalentProperty and creates a map where
     * the keys are the domains of the objects and the value for each key is the
     * set of objects.
     *
     * @param triples
     * @return
     */
    private static Map<String, Set<IRI>> createDomainEquivEntityMap(Set<KTriple> triples) {
        Map<String, Set<IRI>> domainEquivEntityMap = new HashMap<>();
        for (KTriple triple : triples) {
            if (triple.isSameAs() || triple.isEquivalentClass() || triple.isEquivalentProperty()) {
                IRI objectIRI = IRI.create(triple.getObject().toString());
                try {
                    String objectDomain = HTTPUtils.extractDomain(objectIRI.toURI().toURL()).toString();
                    Set<IRI> equivEntities = domainEquivEntityMap.getOrDefault(objectDomain, new TreeSet<>());
                    equivEntities.add(objectIRI);
                    domainEquivEntityMap.putIfAbsent(objectDomain, equivEntities);
                } catch (MalformedURLException murle) {
                    logger.error(murle.getLocalizedMessage());
                }
            }
        }
        return domainEquivEntityMap;
    }

    private static Filters createFilters(IRI objectIRI, URL objectDomain, Map<String, Set<IRI>> domainEquivEntityMap) {
        Set<IRI> equivEntities = new TreeSet<>(domainEquivEntityMap.get(objectDomain.toString()));
        equivEntities.remove(objectIRI);
        Set<String> equivEntitiesString = new TreeSet<>();
        equivEntities.forEach((e) -> equivEntitiesString.add(e.toString()));
        Filters filters = new Filters();
        filters.addObjectForbidFilters(equivEntitiesString);
        return filters;
    }

}

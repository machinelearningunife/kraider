/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.extractors.sparql;

import it.unife.ml.kraider.extractors.sparql.SPARQLEndpoint;
import it.unife.ml.kraider.extractors.sparql.SPARQLQueryMaker;
import it.unife.ml.kraider.extractors.Task;
import static it.unife.ml.kraider.extractors.sparql.SPARQLQueryMaker.OBJECT;
import static it.unife.ml.kraider.extractors.sparql.SPARQLQueryMaker.PREDICATE;
import java.net.URL;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSetRewindable;
import org.apache.jena.rdf.model.RDFNode;
import it.unife.ml.kraider.api.TripleExtractor;
import it.unife.ml.kraider.core.OriginImpl;
import it.unife.ml.kraider.core.KTripleImpl;
import it.unife.ml.kraider.data.Filters;
import it.unife.ml.kraider.exception.InvalidRecursionDepthException;
import it.unife.ml.kraider.exception.UnsupportedRDFSyntaxException;
import it.unife.ml.kraider.extractors.Endpoint;
import it.unife.ml.kraider.extractors.SPARQLQueryExecutor;
import it.unife.ml.kraider.extractors.ldd.LDDEndpoint;
import static it.unife.ml.kraider.extractors.sparql.SPARQLQueryMaker.OBJECT_SAME;
import static it.unife.ml.kraider.extractors.sparql.SPARQLQueryMaker.SUBJECT_SAME;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.xml.ws.http.HTTPException;
import org.apache.jena.rdf.model.ResourceFactory;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.vocab.OWLRDFVocabulary;
import it.unife.ml.kraider.api.KTriple;
import it.unife.ml.kraider.api.Origin;

/**
 * This class is used for executing SPARQL queries, handling the cache and
 * converting the response into RDFTriple objects.
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class SPARQLTask implements Task {

    private final SPARQLQueryMaker queryMaker;

    private final SPARQLQueryExecutor queryExecutor;

    private final Endpoint endpoint;

    private final Origin origin;

    private final Filters filters;

    public SPARQLTask(SPARQLQueryExecutor queryExecutor, 
            SPARQLQueryMaker queryMaker, Filters filters, Endpoint endpoint, 
            Class<? extends TripleExtractor> extractorClass) {
        this.queryExecutor = queryExecutor;
        this.queryMaker = queryMaker;
        this.endpoint = endpoint;
        this.origin = new OriginImpl(endpoint.getDomain(), extractorClass);

        this.filters = new Filters(filters);
    }

    @Override
    public Set<KTriple> raidEntityFragment(IRI instance, int recursionDepth)
            throws InvalidRecursionDepthException, IOException, UnsupportedRDFSyntaxException, HTTPException {

        Set<KTriple> fragment = new TreeSet<>();

        SortedSet<IRI> sameEquivEntities = new TreeSet<>();
        sameEquivEntities.add(instance);
        // obtain the set of owl:sameAs objects
        Set<KTriple> sameAsTriples = raidSameAs(instance);
        fragment.addAll(sameAsTriples);
        sameEquivEntities.addAll(extractNamedObjects(sameAsTriples));
        // obtain the set of owl:equivalentClass objects
        Set<KTriple> equivClassTriples = raidEquivalentClasses(instance);
        fragment.addAll(equivClassTriples);
        sameEquivEntities.addAll(extractNamedObjects(equivClassTriples));
        // obtain the set of owl:equivalentProperty objects
        Set<KTriple> equivPropTriples = raidEquivalentProperty(instance);
        fragment.addAll(equivPropTriples);
        sameEquivEntities.addAll(extractNamedObjects(equivPropTriples));

        // add default filters
        Filters defaultFilters = Filters.getDefaultFilters();
        defaultFilters.getPredicateAllowFilterSet().forEach((filter) -> {
            filters.addPredicateAllowFilter(filter);
        });
        defaultFilters.getPredicateForbidFilterSet().forEach((filter) -> {
            filters.addPredicateForbidFilter(filter);
        });
        defaultFilters.getObjectAllowFilterSet().forEach((filter) -> {
            filters.addObjectAllowFilter(filter);
        });
        defaultFilters.getObjectForbidFilterSet().forEach((filter) -> {
            filters.addObjectForbidFilter(filter);
        });

        String sparqlQueryString = queryMaker.makeEntitiesFragmentQueryString(sameEquivEntities, filters, recursionDepth);
        // System.out.println(sparqlQueryString);
        ResultSetRewindable rs = queryAsResultSet(sparqlQueryString);

        URL domain = endpoint.getDomain();

        while (rs.hasNext()) {
            QuerySolution sol = rs.next();
            RDFNode subj = ResourceFactory.createResource(instance.toString());
            //RDFNode subjSame = sol.get(SUBJECT_SAME);
            recursiveSolutionParsing(subj, SUBJECT_SAME, domain, fragment, sol, 0, recursionDepth);
        }

        int nRows = rs.getRowNumber();
        return fragment;

    }

    private SortedSet<IRI> extractNamedObjects(Set<KTriple> triples) {
        SortedSet<IRI> objects = new TreeSet<>();
        triples.stream().filter((t) -> (t.getObject().isURIResource())).forEachOrdered((t) -> {
            objects.add(IRI.create(t.getObject().toString()));
        });

        return objects;
    }

    private void recursiveSolutionParsing(
            RDFNode subj,
            String subjSameLabel,
            URL domain,
            Set<KTriple> fragment,
            QuerySolution sol,
            int i,
            int recursionDepth) {

        if (subj.isResource() && recursionDepth > i) {

            RDFNode subjSame = sol.get(subjSameLabel);

            if (subjSame != null && !subj.toString().equals(subjSame.toString())) {

                KTriple sameAsTriple = new KTripleImpl(
                        subj,
                        ResourceFactory.createResource(OWLRDFVocabulary.OWL_SAME_AS.getIRI().toString()),
                        subjSame,
                        this.origin);

                fragment.add(sameAsTriple);
            }

            if (recursionDepth > i) {

                RDFNode pred = sol.get(PREDICATE + i);
                RDFNode obj = sol.get(OBJECT + i);

                if (pred == null || obj == null) {
                    return;
                }

                KTriple triple = new KTripleImpl(
                        subj,
                        pred,
                        obj,
                        this.origin);
                fragment.add(triple);

                RDFNode nextSubj = sol.get(OBJECT + i);

                recursiveSolutionParsing(nextSubj, OBJECT_SAME + i, domain, fragment, sol, i + 1, recursionDepth);

            }
        }
    }

    @Override
    public Set<KTriple> raidSameAs(IRI instance) throws IOException, UnsupportedRDFSyntaxException, HTTPException {
        String sparqlQueryString = queryMaker.makeSameAsQueryString(instance, filters);
        ResultSetRewindable rs = queryAsResultSet(sparqlQueryString);
        Set<KTriple> fragment = new TreeSet<>();
        URL domain = endpoint.getDomain();

        while (rs.hasNext()) {
            QuerySolution sol = rs.next();
            RDFNode subj = ResourceFactory.createResource(instance.toString());
            RDFNode subjSame = sol.get(SUBJECT_SAME);

            if (!subj.toString().equals(subjSame.toString())) {

                KTriple sameAsTriple = new KTripleImpl(
                        ResourceFactory.createResource(instance.toString()),
                        ResourceFactory.createResource(OWLRDFVocabulary.OWL_SAME_AS.getIRI().toString()),
                        subjSame,
                        this.origin);

                fragment.add(sameAsTriple);
            }
        }

        return fragment;
    }

    @Override
    public Set<KTriple> raidEquivalentClasses(IRI category) throws IOException, UnsupportedRDFSyntaxException, HTTPException {
        String sparqlQueryString = queryMaker.makeEquivalentClassesQueryString(category, filters);
        ResultSetRewindable rs = queryAsResultSet(sparqlQueryString);
        Set<KTriple> fragment = new TreeSet<>();
        URL domain = endpoint.getDomain();

        while (rs.hasNext()) {
            QuerySolution sol = rs.next();
            RDFNode subj = ResourceFactory.createResource(category.toString());
            RDFNode subjSame = sol.get(SUBJECT_SAME);

            if (!subj.toString().equals(subjSame.toString())) {

                KTriple sameAsTriple = new KTripleImpl(
                        ResourceFactory.createResource(category.toString()),
                        ResourceFactory.createResource(OWLRDFVocabulary.OWL_EQUIVALENT_CLASS.getIRI().toString()),
                        subjSame,
                        this.origin);

                fragment.add(sameAsTriple);
            }
        }

        return fragment;
    }

    @Override
    public Set<KTriple> raidEquivalentProperty(IRI property) throws IOException, UnsupportedRDFSyntaxException, HTTPException {
        String sparqlQueryString = queryMaker.makeEquivalentPropertyQueryString(property, filters);
        ResultSetRewindable rs = queryAsResultSet(sparqlQueryString);
        Set<KTriple> fragment = new TreeSet<>();
        URL domain = endpoint.getDomain();

        while (rs.hasNext()) {
            QuerySolution sol = rs.next();
            RDFNode subj = ResourceFactory.createResource(property.toString());
            RDFNode subjSame = sol.get(SUBJECT_SAME);

            if (!subj.toString().equals(subjSame.toString())) {

                KTriple sameAsTriple = new KTripleImpl(
                        ResourceFactory.createResource(property.toString()),
                        ResourceFactory.createResource(OWLRDFVocabulary.OWL_EQUIVALENT_PROPERTY.getIRI().toString()),
                        subjSame,
                        this.origin);

                fragment.add(sameAsTriple);
            }
        }

        return fragment;
    }

    /**
     * low level, executes query and returns ResultSet.
     *
     * @param sparqlQueryString The query
     * @return jena ResultSet
     */
    public ResultSetRewindable queryAsResultSet(String sparqlQueryString) throws IOException, UnsupportedRDFSyntaxException, HTTPException {

        return queryExecutor.executeQuery(sparqlQueryString, this.endpoint);

//        if (cache == null) {
//            return sq.send();
//        } else {
//            // get JSON from cache and convert to result set
//            String json = cache.executeSparqlQuery(sq);
//            return SparqlQuery.convertJSONtoResultSet(json);
//        }
    }

    @Override
    public Set<KTriple> resolveBlankNode() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.extractors;

import java.net.URL;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public abstract class Endpoint {

    protected String name;
    protected URL domain;
    protected URL url;
    protected boolean active = true;

    public Endpoint() {
    }

    public Endpoint(URL domain, URL url) {
        this.domain = domain;
        this.url = url;
    }

    public Endpoint(URL domain, URL url, String name) {
        this.domain = domain;
        this.url = url;
        this.name = name;
    }

    /**
     * @return the domain
     */
    public URL getDomain() {
        return domain;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the url
     */
    public URL getURL() {
        return url;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

}

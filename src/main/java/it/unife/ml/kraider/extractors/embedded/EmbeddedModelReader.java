/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.extractors.embedded;

import Jena.RDFEmbedded.ReaderRIOTFactoryJSONLD;
import Jena.RDFEmbedded.ReaderRIOTFactoryMICRODATA;
import Jena.RDFEmbedded.ReaderRIOTFactoryRDFA;
import Jena.RDFEmbedded.ReaderRIOTFactoryTURTLE;
import it.unife.ml.kraider.exception.UnsupportedRDFSyntaxException;
import it.unife.ml.kraider.extractors.ModelReader;
import java.io.IOException;
import java.util.EnumSet;
import java.util.concurrent.TimeUnit;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.LangBuilder;
import org.apache.jena.riot.RDFParserRegistry;
import org.apache.jena.riot.ReaderRIOTFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class EmbeddedModelReader implements ModelReader {

    private static final Logger logger = LoggerFactory.getLogger(EmbeddedModelReader.class);

    public enum SupportedSyntax {
        JSONLD_EMBEDDED("application/ld+json-embedded", "JSONLDEmbedded"),
        MICRODATA_EMBEDDED("text/microdata-embedded", "MicrodataEmbedded"),
        RDFA_EMBEDDED("text/rdfa-embedded", "Rdfaembedded"),
        TURTLE_EMBEDDED("text/turtle-embedded", "TURTLEEmbedded");

        private final String httpContentType;
        private final String jenaSyntax;

        SupportedSyntax(String httpContentType, String jenaSyntax) {
            this.httpContentType = httpContentType;
            this.jenaSyntax = jenaSyntax;
        }

        public String getHTTPContentType() {
            return this.httpContentType;
        }

        /**
         * @return the jenaSyntax
         */
        public String getJenaSyntax() {
            return jenaSyntax;
        }
    }

    private static String acceptString;

    static {
        EnumSet.allOf(SupportedSyntax.class)
                .forEach(syntax -> {
                    EmbeddedModelReader.acceptString += syntax.getHTTPContentType() + ",";

                    Lang lang = LangBuilder.create(syntax.getJenaSyntax(), syntax.getHTTPContentType()).build();
                    ReaderRIOTFactory readerRIOTFactory = null;
                    switch (syntax.getJenaSyntax()) {
                        case "JSONLDEmbedded":
                            readerRIOTFactory = new ReaderRIOTFactoryJSONLD();
                            break;
                        case "MicrodataEmbedded":
                            readerRIOTFactory = new ReaderRIOTFactoryMICRODATA();
                            break;
                        case "Rdfaembedded":
                            readerRIOTFactory = new ReaderRIOTFactoryRDFA();
                            break;
                        case "TURTLEEmbedded":
                            readerRIOTFactory = new ReaderRIOTFactoryTURTLE();
                            break;
                    }

                    if (lang != null && readerRIOTFactory != null) {
                        RDFParserRegistry.registerLangTriples(lang, readerRIOTFactory);
                    }
                });
        // remove last comma
        EmbeddedModelReader.acceptString = EmbeddedModelReader.acceptString.substring(0, EmbeddedModelReader.acceptString.length() - 1);

    }

    @Override
    public Model retrieveModel(String iri) throws IOException, UnsupportedRDFSyntaxException {
        // The RDF model of the IRI
        logger.debug("Trying reading embedded model from: " + iri);
        Model model = ModelFactory.createDefaultModel();

//        Thread t = new Thread(() -> {
//            try {
//                TimeUnit.SECONDS.sleep(60*2);
//                model.abort();
//            } catch (InterruptedException ex) {
//                // do nothing
//            }
//        });
//        t.start();

        for (SupportedSyntax syntax : SupportedSyntax.values()) {
            try {
                logger.debug("Trying with {}", syntax.getJenaSyntax());
                model.read(iri, iri, syntax.jenaSyntax);
                if (model.size() > 0) {
//                    t.interrupt();
                    return model;
                }
            } catch (Exception ex) {
                continue;
            }
        }

        logger.info("IRI " + iri + " does not contain supported embedded data");
        throw new UnsupportedRDFSyntaxException("IRI " + iri + " does not contain supported embedded data");
    }

}

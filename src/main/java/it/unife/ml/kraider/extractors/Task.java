/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.extractors;

import java.util.Set;
import it.unife.ml.kraider.exception.InvalidRecursionDepthException;
import it.unife.ml.kraider.exception.UnsupportedRDFSyntaxException;
import java.io.IOException;
import javax.xml.ws.http.HTTPException;
import org.semanticweb.owlapi.model.IRI;
import it.unife.ml.kraider.api.KTriple;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface Task {
    
    public Set<KTriple> raidEntityFragment(IRI instance, int recursionDepth)
            throws InvalidRecursionDepthException, IOException, UnsupportedRDFSyntaxException, HTTPException;
    
//    /**
//     * Extract a knowledge fragment concerning a set of instances identified by IRIs.
//     * The fragment does not contain triples with the following predicates:
//     * <ul>
//     *  <li>owl:sameAs</li>
//     *  <li>owl:equivalentClass</li>
//     *  <li>owl:equivalentProperty</li>
//     * </ul>
//     * 
//     * @param instance
//     * @param recursionDepth
//     * @return
//     * @throws InvalidRecursionDepthException 
//     */
//    public Set<Triple> raidEntitiesFragment(Set<IRI> instances, int recursionDepth)
//            throws InvalidRecursionDepthException;
    
    public Set<KTriple> raidSameAs(IRI instance) throws IOException, UnsupportedRDFSyntaxException, HTTPException;
    
    public Set<KTriple> raidEquivalentClasses(IRI category) throws IOException, UnsupportedRDFSyntaxException, HTTPException;
    
    public Set<KTriple> raidEquivalentProperty(IRI property) throws IOException, UnsupportedRDFSyntaxException, HTTPException;
    
    public Set<KTriple> resolveBlankNode();
    
}

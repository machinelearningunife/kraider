/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.extractors.ldd;

import it.unife.ml.kraider.extractors.Endpoint;
import java.net.URL;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class LDDEndpoint extends Endpoint {
    
    public LDDEndpoint(URL domain, URL url) {
        super(domain, url);
    }
    
}

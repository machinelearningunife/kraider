/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.extractors.embedded;

import it.unife.ml.kraider.api.KTriple;
import it.unife.ml.kraider.api.TripleExtractorManager;
import it.unife.ml.kraider.core.AbstractTripleExtractor;
import it.unife.ml.kraider.data.Job;
import it.unife.ml.kraider.data.JobStatus;
import it.unife.ml.kraider.exception.InvalidRecursionDepthException;
import it.unife.ml.kraider.extractors.Endpoint;
import it.unife.ml.kraider.extractors.JobCreator;
import it.unife.ml.kraider.extractors.SPARQLQueryExecutor;
import it.unife.ml.kraider.extractors.sparql.SPARQLQueryMaker;
import it.unife.ml.kraider.extractors.sparql.SPARQLTask;
import it.unife.ml.kraider.utilities.HTTPUtils;
import java.net.URL;
import java.util.Collections;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used to extract triples from data embedded in Web pages.
 * Embedded data can be in RDFa format or JSON-LD format (between <script>
 * tags).
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class EmbeddedDataTripleExtractor extends AbstractTripleExtractor {

    private boolean stop = false;

    private final SPARQLQueryMaker queryMaker = new SPARQLQueryMaker();

    private static final Logger logger = LoggerFactory.getLogger(EmbeddedDataTripleExtractor.class);

    private final EmbeddedModelReader modelReader = new EmbeddedModelReader();

    public EmbeddedDataTripleExtractor(int id, TripleExtractorManager manager) {
        super(id, manager);
    }

    @Override
    public void terminateExtraction() {
        this.stop = true;
    }

    @Override
    public void run() {
        running = true;
        // require first job
        Job job = manager.requireJob(id, null, null, null, null);

        while (!stopCriteria()) {
            Set<KTriple> triples = null;
            Set<Job> newJobs = null;
            JobStatus.Status status = JobStatus.Status.FAILURE;
            logger.info("{} with id: {}: executing job: subject: {}, domain: {}, filters: {}",
                    this.getClass().getSimpleName(), id,
                    job.getSubject(), job.getDomain(), job.getFilters());
            try {
                if (job.getRecursionDepth() < 0) {
                    String msg = "Invalid Recursion depth: " + job.getRecursionDepth();
                    logger.error(msg);
                    throw new InvalidRecursionDepthException(msg);
                }

                if (HTTPUtils.checkDocumentExistence(job.getSubject().toURI().toURL())) {
                    URL domain = job.getDomain();
                    Endpoint endpoint = new EmbeddedHTMLEndpoint(domain, job.getSubject().toURI().toURL());
                    SPARQLTask task = new SPARQLTask(SPARQLQueryExecutor.sparqlLDDQueryExecutor(modelReader),
                            queryMaker, job.getFilters(), endpoint, this.getClass());
                    if (job.getRecursionDepth() == 0) {
                        if (manager.getConfiguration().isSameAsLastObject()) {
                            logger.debug("Executing task: ( {}, equivalent, {}, {} )",
                                    job.getSubject(),
                                    job.getDomain(),
                                    job.getRecursionDepth());
                            triples = task.raidSameAs(job.getSubject());
                            triples.addAll(task.raidEquivalentClasses(job.getSubject()));
                            triples.addAll(task.raidEquivalentProperty(job.getSubject()));
                        } else {
                            triples = Collections.emptySet();
                        }
                    } else {
                        try {
                            logger.debug("Executing task: ( {}, {}, {} )",
                                    job.getSubject(),
                                    job.getDomain(),
                                    job.getRecursionDepth());
                            triples = task.raidEntityFragment(job.getSubject(), 1);
                        } catch (InvalidRecursionDepthException irde) {
                            triples = Collections.emptySet();
                            logger.error(irde.getMessage());
                        }
                    }
                    newJobs = JobCreator.createJobsFromTriples(manager.getConfiguration(), job, triples);
                    status = JobStatus.Status.COMPLETED;
//                    job = manager.requireJob(id, triples, job, status, newJobs);
                } else {
                    // endpoint not found!
                    status = JobStatus.Status.FAILURE;
                    triples = null;
                    newJobs = null;
//                    job = manager.requireJob(id, null, job, JobStatus.Status.FAILURE, null);
                }
            } catch (Exception ex) {
                // An unexpected exception occurred
                logger.error("{} with id: {}: ERROR: {}", this.getClass(), this.getId(), ex.getMessage());
                status = JobStatus.Status.FAILURE;
                triples = null;
                newJobs = null;
//                job = manager.requireJob(id, null, job, JobStatus.Status.FAILURE, null);
            } finally {
                if (!stopCriteria()) {
                    logger.info("{} with id: {}: requesting for another job",
                            this.getClass().getSimpleName(), id);
                    job = manager.requireJob(id, triples, job, status, newJobs);
                } else {
                    logger.info("{} with id: {}: STOPPED",
                            this.getClass().getSimpleName(), id);
                }
            }
        }

        running = false;
    }

    private boolean stopCriteria() {
        return stop;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.extractors.sparql;

import com.google.common.collect.Lists;
import it.unife.ml.kraider.extractors.Endpoint;
import it.unife.ml.kraider.utilities.HTTPUtils;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class SPARQLEndpoint extends Endpoint {

    protected List<String> defaultGraphIRIs;
    protected List<String> namedGraphIRIs;

    //public HashMap<String, String> parameters = new HashMap<String, String>();
    
    public SPARQLEndpoint() {
        
    }

    public SPARQLEndpoint(URL domain, URL url, List<String> defaultGraphURIs, List<String> namedGraphURIs) {
        super(domain, url);
        this.defaultGraphIRIs = defaultGraphURIs;
        this.namedGraphIRIs = namedGraphURIs;
    }

    public SPARQLEndpoint(URL url) throws MalformedURLException {
        this(url, Collections.<String>emptyList(), Collections.<String>emptyList());
    }

    public SPARQLEndpoint(URL url, List<String> defaultGraphURIs, List<String> namedGraphURIs) throws MalformedURLException {
        this(new URL("http://" + url.getHost()), url, defaultGraphURIs, namedGraphURIs);
    }

    public SPARQLEndpoint(URL url, String defaultGraphURI) throws MalformedURLException {
        this(url, Collections.singletonList(defaultGraphURI), Collections.<String>emptyList());
    }

    public static SPARQLEndpoint create(String url, String defaultGraphURI) throws MalformedURLException {
        return create(url, Lists.newArrayList(defaultGraphURI));
    }

    public static SPARQLEndpoint create(String url, List<String> defaultGraphURIs) throws MalformedURLException {
        return create(url, defaultGraphURIs, Collections.emptyList());
    }

    public static SPARQLEndpoint create(String url, List<String> defaultGraphURIs, List<String> namedGraphURIs) throws MalformedURLException {
        return new SPARQLEndpoint(new URL(url), defaultGraphURIs, namedGraphURIs);
    }

    public String getHTTPRequest() {
        String ret = this.getURL().toString() + "?";
        ret += (getDefaultGraphIRIs().isEmpty()) ? "" : "default-graph-uri=" + getDefaultGraphIRIs().get(0) + "&";
        ret += "query=";
        return ret;
    }

    public List<String> getDefaultGraphIRIs() {
        return defaultGraphIRIs;
    }

    public List<String> getNamedGraphIRIs() {
        return namedGraphIRIs;
    }

    @Override
    public String toString() {
        return getHTTPRequest();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getDefaultGraphIRIs() == null) ? 0 : getDefaultGraphIRIs().hashCode());
        result = prime * result + ((getNamedGraphIRIs() == null) ? 0 : getNamedGraphIRIs().hashCode());
        result = prime * result + ((getURL() == null) ? 0 : getURL().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SPARQLEndpoint other = (SPARQLEndpoint) obj;
        if (getDefaultGraphIRIs() == null) {
            if (other.getDefaultGraphIRIs() != null) {
                return false;
            }
        } else if (!defaultGraphIRIs.equals(other.defaultGraphIRIs)) {
            return false;
        }
        if (getNamedGraphIRIs() == null) {
            if (other.getNamedGraphIRIs() != null) {
                return false;
            }
        } else if (!namedGraphIRIs.equals(other.namedGraphIRIs)) {
            return false;
        }
        if (getURL() == null) {
            if (other.getURL() != null) {
                return false;
            }
        } else if (!url.equals(other.url)) {
            return false;
        }
        return true;
    }

    /**
     * @return the domain
     */
    public URL getDomain() {
        return domain;
    }

    /**
     * @param domain the domain to set
     */
    public void setDomain(URL domain) {
        this.domain = domain;
    }

    /**
     * @param defaultGraphIRIs the defaultGraphURIs to set
     */
    public void setDefaultGraphIRIs(List<String> defaultGraphIRIs) {
        this.defaultGraphIRIs = defaultGraphIRIs;
    }

    /**
     * @param namedGraphIRIs the namedGraphURIs to set
     */
    public void setNamedGraphIRIs(List<String> namedGraphIRIs) {
        this.namedGraphIRIs = namedGraphIRIs;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the url
     */
    @Override
    public URL getURL() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setURL(URL url) throws MalformedURLException {
        this.url = url;
        this.domain = HTTPUtils.extractDomain(url);
    }

}

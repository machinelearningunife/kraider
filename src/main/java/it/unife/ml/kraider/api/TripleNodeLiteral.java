/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.api;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface TripleNodeLiteral extends KTripleNode {
    
    public boolean isDouble();

    public boolean isFloat();

    public boolean isInt();

    public boolean isBoolean();

    public boolean isString();

    public float getFloat();

    public double getDouble();

    public int getInt();

    public boolean getBoolean();

    public String getString();

    public String getNTripleForm();
    
    
}

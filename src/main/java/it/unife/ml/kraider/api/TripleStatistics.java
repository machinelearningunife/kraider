/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.api;

import java.net.URL;
import java.util.Map;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface TripleStatistics {
    
    /**
     * @return the nTriples
     */
    public int getnTriples();

    /**
     * @return the methodCount
     */
    public Map<Class<? extends TripleExtractor>, Integer> getMethodCount();

    /**
     * @return the domainCount
     */
    public Map<URL, Integer> getDomainCount();

    /**
     * @return the originCount
     */
    public Map<Origin, Integer> getOriginCount();
    
    /**
     * Resets statistics
     */
    public void reset();
    
    
}

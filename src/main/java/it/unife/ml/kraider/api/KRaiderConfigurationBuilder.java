/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.api;

import it.unife.ml.kraider.exception.ConfigurationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.unife.ml.kraider.core.LDDTripleExtractorFactoryImpl;
import it.unife.ml.kraider.core.SPARQLEndpointTripleExtractorFactoryImpl;
import it.unife.ml.kraider.core.TripleStatisticsImpl;
import it.unife.ml.kraider.extractors.sparql.SPARQLEndpoint;
import it.unife.ml.kraider.extractors.sparql.SPARQLEndpoints;
import it.unife.ml.kraider.utilities.SetLinkedBlockingQueue;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.semanticweb.owlapi.model.IRI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class KRaiderConfigurationBuilder {

    private static final Logger logger = LoggerFactory.getLogger(KRaiderConfigurationBuilder.class);

    protected IRI resource;

    protected BlockingQueue<KTriple> tripleQueue = new SetLinkedBlockingQueue<>();

    protected CopyOnWriteArrayList<TripleExtractorFactory> availableExtractorFactories
            = new CopyOnWriteArrayList<>(new TripleExtractorFactory[]{new SPARQLEndpointTripleExtractorFactoryImpl(),
        new LDDTripleExtractorFactoryImpl()});

    protected int nMaxExtractors = 6;

    protected SPARQLEndpoints sparqlEndpoints;

    protected int recursionDepth = 2; // default value

    /**
     * This condition must be true if we want to handle blank nodes
     */
    protected boolean resolveBlankNodes = false;

    /**
     * Statistics about all the extracted triples. I.e. if the extracted triple
     * was already extracted previously we count it.
     */
    protected TripleStatisticsImpl extractedTriplesStatistics = new TripleStatisticsImpl();

    /**
     * If true finds the Owl:sameAs of the last object
     */
    protected boolean sameAsLastObject = true;

    /**
     * Timeout for polling a job from the job table in seconds. Default 1
     * second.
     */
    protected long jobPollTimeout = 1;

    /**
     * General timeout (default: Long.MAX_VALUE).
     */
    protected long timeout = Long.MAX_VALUE;

    /**
     * Cache usage (default: true).
     */
    protected boolean cache = true;

//    protected String endpointListFilepath = "target/classes/sparql_endpoints.json";
//    protected String endpointListFilepath;
    public static String DEFAULT_SPARQL_ENDPOINT_FILE_LIST = "/sparql_endpoints.json";

    public static String DEFAULT_SPARQL_ENDPOINT_FILE_WEBAPP = "https://sparql.ml.unife.it/sparql-endpoint-availability";

    public KRaiderConfigurationBuilder() {
//        URL f = KRaiderConfigurationBuilder.class.getResource("/sparql_endpoints.json");
//        endpointListFilepath = f.getFile();
    }

    public KRaiderConfigurationBuilder(KRaiderConfiguration conf) {
        this();
        this.availableExtractorFactories = conf.getAvailableExtractorFactories();
        this.extractedTriplesStatistics = conf.extractedTriplesStatistics;
        this.jobPollTimeout = conf.jobPollTimeout;
        this.nMaxExtractors = conf.nMaxExtractors;
        this.recursionDepth = conf.recursionDepth;
        this.resolveBlankNodes = conf.resolveBlankNodes;
        this.resource = conf.resource;
        this.sameAsLastObject = conf.sameAsLastObject;
        this.sparqlEndpoints = conf.getSparqlEndpoints();
        this.tripleQueue = conf.getTripleQueue();
        this.timeout = conf.timeout;
        this.cache = conf.isCache();
    }

    public void updateEndpoints() throws FileNotFoundException, IOException {
        ObjectMapper mapper = new ObjectMapper();
//        InputStream is = new FileInputStream(endpointListFilepath);

        List<SPARQLEndpoint> endpointList;
        // get sparql endpoint list from web site
        try {
            URL url = new URL(DEFAULT_SPARQL_ENDPOINT_FILE_WEBAPP + "/status/current/active");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() == 200) {
                InputStream is = conn.getInputStream();
                endpointList = mapper.readValue(is, new TypeReference<List<SPARQLEndpoint>>() {
                });
                logger.info("SPARQL Endpoint list from {}", DEFAULT_SPARQL_ENDPOINT_FILE_WEBAPP);
            } else {
                // get list from file
                throw new IOException("Response code different than from 200.");

            }
        } catch (Exception ex) {
            InputStream is = KRaiderConfigurationBuilder.class.getResourceAsStream(DEFAULT_SPARQL_ENDPOINT_FILE_LIST);
            endpointList = mapper.readValue(is, new TypeReference<List<SPARQLEndpoint>>() {
            });
            logger.info("SPARQL Endpoint list from {}", DEFAULT_SPARQL_ENDPOINT_FILE_LIST);
        }

        // TO DO: Check active endpoints
        sparqlEndpoints = new SPARQLEndpoints(endpointList);

    }

    public KRaiderConfiguration buildConfiguration() throws ConfigurationException {
        if (sparqlEndpoints == null) {
            try {
                updateEndpoints();
            } catch (Exception e) {
                throw new ConfigurationException(e.getLocalizedMessage());
            }
        }

        KRaiderConfiguration configuration = new KRaiderConfiguration(
                resource,
                tripleQueue,
                availableExtractorFactories,
                nMaxExtractors,
                sparqlEndpoints,
                extractedTriplesStatistics,
                sameAsLastObject,
                resolveBlankNodes,
                recursionDepth,
                jobPollTimeout,
                timeout,
                cache);

        // check if validation constraints of configuration are met
        validateConfiguration(configuration);

        return configuration;
    }

    public KRaiderConfigurationBuilder resource(String resource) {
        this.resource = IRI.create(resource);
        return this;
    }

    public KRaiderConfigurationBuilder tripleQueue(BlockingQueue<KTriple> tripleQueue) {
        this.tripleQueue = tripleQueue;
        return this;
    }

    public KRaiderConfigurationBuilder availableExtractorFactories(CopyOnWriteArrayList<TripleExtractorFactory> availableExtractorFactories) {
        this.availableExtractorFactories = availableExtractorFactories;
        return this;
    }

    public KRaiderConfigurationBuilder nMaxExtractors(int nMaxExtractors) {
        this.nMaxExtractors = nMaxExtractors;
        return this;
    }

    public KRaiderConfigurationBuilder sparqlEndpoints(SPARQLEndpoints sparqlEndpoints) {
        this.sparqlEndpoints = sparqlEndpoints;
        return this;
    }

    public KRaiderConfigurationBuilder recursionDepth(int recursionDepth) {
        this.recursionDepth = recursionDepth;
        return this;
    }

    public KRaiderConfigurationBuilder extractedTriplesStatistics(TripleStatisticsImpl extractedTriplesStatistics) {
        this.extractedTriplesStatistics = extractedTriplesStatistics;
        return this;
    }

    public KRaiderConfigurationBuilder sameAsLastObject(boolean sameAsLastObject) {
        this.sameAsLastObject = sameAsLastObject;
        return this;
    }

    public KRaiderConfigurationBuilder resolveBlankNodes(boolean resolveBlankNodes) {
        this.resolveBlankNodes = resolveBlankNodes;
        return this;
    }

    public KRaiderConfigurationBuilder jobPollTimeout(long jobPollTimeout) {
        this.jobPollTimeout = jobPollTimeout;
        return this;
    }

    public KRaiderConfigurationBuilder timeout(long timeout) {
        this.timeout = timeout;
        return this;
    }

    private void validateConfiguration(KRaiderConfiguration configuration) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<KRaiderConfiguration>> violations = validator.validate(configuration);

        // throws first violation
        if (violations.iterator().hasNext()) {
            ConstraintViolation<KRaiderConfiguration> violation = violations.iterator().next();
            throw new ConfigurationException(violation.getPropertyPath() + " " + violation.getMessage());
        }
//        for (ConstraintViolation<Configuration> violation : violations) {
//            ui.showError(violation.getPropertyPath() + " " + violation.getMessage());
//        }
    }

    /**
     * Set whether using the cache or not.
     *
     * @param useCache
     * @return
     */
    public KRaiderConfigurationBuilder cache(boolean useCache) {
        this.cache = useCache;
        return this;
    }

}

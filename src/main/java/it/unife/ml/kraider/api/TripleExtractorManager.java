/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.api;

import it.unife.ml.kraider.data.Job;
import it.unife.ml.kraider.data.JobStatus;
import java.util.Set;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface TripleExtractorManager {
    
//    public void useConfiguration(KRaiderConfiguration configuration);
    
    public void startExtraction();
    
    public Job requireJob(int extractorId, Set<KTriple> resultTriple, Job prevJob, 
            JobStatus.Status prevJobStatus, Set<Job> newJobs);
    
    public KRaiderConfiguration getConfiguration();

    public void stop();
    
}

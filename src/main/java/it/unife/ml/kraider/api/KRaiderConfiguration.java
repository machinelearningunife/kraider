/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.api;

import java.util.concurrent.BlockingQueue;
import it.unife.ml.kraider.core.TripleStatisticsImpl;
import it.unife.ml.kraider.extractors.sparql.SPARQLEndpoints;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.validation.constraints.NotNull;
import org.semanticweb.owlapi.model.IRI;

/**
 * Configuration class used by the TripleExtractorManager
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class KRaiderConfiguration {

    @NotNull
    protected IRI resource;

    @NotNull
    protected BlockingQueue<KTriple> tripleQueue;

    @NotNull
    protected CopyOnWriteArrayList<TripleExtractorFactory> availableExtractorFactories;

    @NotNull
    protected int nMaxExtractors;

    @NotNull
    protected SPARQLEndpoints sparqlEndpoints;

    @NotNull
    protected int recursionDepth;

    /**
     * This condition must be true if we want to handle blank nodes
     */
    @NotNull
    protected boolean resolveBlankNodes;

    /**
     * Statistics about all the extracted triples. I.e. if the extracted triple
     * was already extracted previously we count it.
     */
    @NotNull
    protected TripleStatisticsImpl extractedTriplesStatistics;

    /**
     * If true finds the Owl:sameAs of the last object
     */
    @NotNull
    protected boolean sameAsLastObject;

    /**
     * Timeout for polling a job from the job table in seconds
     */
    @NotNull
    protected long jobPollTimeout;

    /**
     * General timeout.
     */
    @NotNull
    protected long timeout;
    
    
    /**
     * If true cache is used
     */
    @NotNull
    protected boolean cache;

    public KRaiderConfiguration(
            IRI resource,
            BlockingQueue<KTriple> tripleQueue,
            CopyOnWriteArrayList<TripleExtractorFactory> availableExtractorFactories,
            int nMaxExtractors,
            SPARQLEndpoints sparqlEndpoints,
            TripleStatisticsImpl extractedTriplesStatistics,
            boolean sameAsLastObject,
            boolean resolveBlankNodes,
            int recursionDepth,
            long jobPollTimeout,
            long timeout,
            boolean cache) {
        this.resource = resource;
        this.tripleQueue = tripleQueue;
        this.availableExtractorFactories = availableExtractorFactories;
        this.nMaxExtractors = nMaxExtractors;
        this.sparqlEndpoints = sparqlEndpoints;
        this.sameAsLastObject = sameAsLastObject;
        this.resolveBlankNodes = resolveBlankNodes;
        this.recursionDepth = recursionDepth;
        this.extractedTriplesStatistics = extractedTriplesStatistics;
        this.jobPollTimeout = jobPollTimeout;
        this.timeout = timeout;
        this.cache = cache;
    }

    /**
     * @return the nMaxExtractors
     */
    public int getnMaxExtractors() {
        return nMaxExtractors;
    }

    /**
     * @return the queue
     */
    public BlockingQueue<KTriple> getTripleQueue() {
        return tripleQueue;
    }

    /**
     * @return the availableExtractors
     */
    public CopyOnWriteArrayList<TripleExtractorFactory> getAvailableExtractorFactories() {
        return availableExtractorFactories;
    }

    /**
     * @return the resource
     */
    public IRI getResource() {
        return resource;
    }

    /**
     * @return the sparqlEndpoints
     */
    public SPARQLEndpoints getSparqlEndpoints() {
        return sparqlEndpoints;
    }

    /**
     * @return the sameAsLastObject
     */
    public boolean isSameAsLastObject() {
        return sameAsLastObject;
    }

    /**
     * @return the recursionDepth
     */
    public int getRecursionDepth() {
        return recursionDepth;
    }

    /**
     * @return the extractedTriplesStatistics
     */
    public TripleStatisticsImpl getExtractedTriplesStatistics() {
        return extractedTriplesStatistics;
    }

    /**
     * @return the jobPollTimeout
     */
    public long getJobPollTimeout() {
        return jobPollTimeout;
    }

    /**
     * @return the timeout
     */
    public long getTimeout() {
        return timeout;
    }

    /**
     * @return true is caching is used, false otherwise
     */
    public boolean isCache() {
        return cache;
    }
}

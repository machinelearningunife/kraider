/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.api;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 * @param <T>
 */
public interface TripleNodeVisitor<T> {
    
    public T visit(TripleNodeBlank blankNode);
    
    public T visit(TripleNodeURIResource namedNode);
    
    public T visit(TripleNodeLiteral literalNode);
    
}

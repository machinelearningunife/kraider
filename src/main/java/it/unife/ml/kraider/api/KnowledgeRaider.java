/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.api;

import java.net.MalformedURLException;
import java.util.List;
import java.util.concurrent.BlockingQueue;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface KnowledgeRaider {
     
     /**
     * Returns the knowledge fragments of each resource
     *
     * @param subject
     * @param predicate
     * @param object
     * @param configuration
     * @return
     */
    public List<BlockingQueue<KTriple>> raidKnowledge(String subject, String predicate, String object, KRaiderConfiguration configuration);
    
        /**
     * Returns the knowledge fragments of each resource
     *
     * @param subject
     * @param predicate
     * @param object
     * @return
     */
    public List<BlockingQueue<KTriple>> raidKnowledge(String subject, String predicate, String object);

    /**
     * Returns the knowledge fragment of a given resource;
     *
     * @param resource
     * @param configuration
     * @param wait
     * @param resetStatistics if true reset the previously computed statistics
     * @return
     * @throws java.net.MalformedURLException
     */
    public BlockingQueue<KTriple> raidKnowledge(String resource, KRaiderConfiguration configuration, boolean wait, boolean resetStatistics) throws MalformedURLException;
    
    public BlockingQueue<KTriple> raidKnowledge(KRaiderConfiguration configuration, boolean wait) throws MalformedURLException;
    
     /**
     * Returns the knowledge fragment of a given resource;
     *
     * @param resource
     * @param wait
     * @return
     * @throws java.net.MalformedURLException
     */
    public BlockingQueue<KTriple> raidKnowledge(String resource) throws MalformedURLException;
    
    
    /**
     * Stop raiding triples.
     */
    public void stop();
    
    /**
     * Returns statistics about all the extracted triples. I.e. if the extracted triple
     * was already extracted previously we count it.
     * 
     * @return the extractedTriplesStatistics
     */
    public TripleStatistics getExtractedTriplesStatistics();
    
    public KRaiderConfiguration getDefaultConfiguration(String resource);

}

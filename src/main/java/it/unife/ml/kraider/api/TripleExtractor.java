/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.api;

/**
 * This is an interface used for classes that
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface TripleExtractor extends Runnable {

    /**
     * Returns the identifier of the extractor.
     *
     * @return identifier of the extractor.
     */
    public int getId();

    /**
     * This method must be invoked in order to tell the extractor to terminate
     * the extraction of triple from the web.
     */
    public void terminateExtraction();

//    @Override
//    public boolean equals(Object o);
//
//    @Override
//    public int hashCode();

}

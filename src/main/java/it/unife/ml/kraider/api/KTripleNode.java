/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.api;

import org.apache.jena.graph.Node;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface KTripleNode {

    /**
     * Answer true iff this RDFNode is an named resource.
     *
     * @return
     */
    public boolean isURIResource();

    /**
     * Answer true iff this RDFNode is an anonymous resource.
     *
     * @return
     */
    public boolean isAnon();

    /**
     * Answer true iff this RDFNode is a literal resource.
     *
     * @return
     */
    public boolean isLiteral();

    public <T> T visitWith(TripleNodeVisitor<T> visitor);

    public Node asNode();

}

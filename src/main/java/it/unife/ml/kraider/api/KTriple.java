/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.api;

import java.net.URL;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface KTriple extends Comparable<KTriple> {

    public KTripleNode getSubject();

    public KTripleNode getPredicate();

    public KTripleNode getObject();

    public URL getDomain();
    
    public String getDomainString();

    public Origin getOrigin();

    /**
     * Returns true if the predicate of the triple is owl:sameAs
     * @return 
     */
    public boolean isSameAs();
    
    /**
     * Returns true if the predicate of the triple is owl:equivalentClass
     * @return 
     */
    public boolean isEquivalentClass();
    
    /**
     * Returns true if the predicate of the triple is owl:equivalentProperty
     * @return 
     */
    public boolean isEquivalentProperty();
    
}

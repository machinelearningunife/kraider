/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.core;

import it.unife.ml.kraider.api.TripleExtractor;
import it.unife.ml.kraider.api.TripleExtractorFactory;
import it.unife.ml.kraider.api.TripleExtractorManager;
import it.unife.ml.kraider.extractors.embedded.EmbeddedDataTripleExtractor;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class EmbeddedTripleExtractorFactoryImpl implements TripleExtractorFactory {

    @Override
    public EmbeddedDataTripleExtractor getTripleExtractor(int id, TripleExtractorManager manager) {
        return new EmbeddedDataTripleExtractor(id, manager);
    }

    @Override
    public Class<EmbeddedDataTripleExtractor> getTripleExtractorType() {
        return EmbeddedDataTripleExtractor.class;
    }
    
}

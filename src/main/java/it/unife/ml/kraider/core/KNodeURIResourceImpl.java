/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.core;

import it.unife.ml.kraider.api.TripleNodeURIResource;
import it.unife.ml.kraider.api.TripleNodeVisitor;
import org.apache.jena.graph.Node_URI;
import org.apache.jena.rdf.model.RDFNode;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class KNodeURIResourceImpl extends KNodeImpl implements TripleNodeURIResource {

    public String uri;
    
       
    public KNodeURIResourceImpl(RDFNode node) {
        super(node);
        uri = node.toString();
    }

    public KNodeURIResourceImpl(Node_URI it, String uri) {
        super(it);
        this.uri = uri;
    }

    @Override
    public <T> T visitWith(TripleNodeVisitor<T> visitor) {
        return visitor.visit(this);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.core;

import it.unife.ml.kraider.api.TripleNodeBlank;
import it.unife.ml.kraider.api.TripleNodeLiteral;
import it.unife.ml.kraider.api.TripleNodeVisitor;
import org.apache.jena.graph.Node_Literal;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.RDFNode;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class KNodeLiteralImpl extends KNodeImpl implements TripleNodeLiteral {

    protected Literal literal;
    
    public KNodeLiteralImpl(Literal node) {
        super(node);
        literal = node;
//        literal = ((Literal) node);
//        if (!(node instanceof Literal)) {
//            throw new IllegalArgumentException("not a literal: " + node.getClass().getCanonicalName());
//        }
    }
    
    public KNodeLiteralImpl(Node_Literal node) {
        super(node);
    }
    

    public boolean isDouble() {
        try {
            return literal.getDatatypeURI() != null && literal.getDatatypeURI().contains("double");
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isFloat() {
        try {
            return literal.getDatatypeURI() != null && literal.getDatatypeURI().contains("float");
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isInt() {
        try {
            return literal.getDatatypeURI() != null && literal.getDatatypeURI().contains("int");
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isBoolean() {
        try {
            return literal.getDatatypeURI() != null && literal.getDatatypeURI().contains("boolean");
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isString() {
        try {
            literal.getString();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public <T> T visitWith(TripleNodeVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public float getFloat() {
        return literal.getFloat();
    }

    @Override
    public double getDouble() {
        return literal.getDouble();
    }

    @Override
    public int getInt() {
        return literal.getInt();
    }

    @Override
    public boolean getBoolean() {
        return literal.getBoolean();
    }

    @Override
    public String getString() {
        return literal.getString();
    }

    @Override
    public String getNTripleForm() {
        String quote = "\\\"";
        quote = "&quot;";
        String retVal = literal.getLexicalForm();
        retVal = retVal.replaceAll("\n", "\\n");
        retVal = retVal.replaceAll("\"", quote);
        retVal = "\"" + retVal + "\"";
        if (literal.getDatatypeURI() != null) {
            return retVal + "^^<" + literal.getDatatypeURI() + ">";
        } else {
            return retVal + ((literal.getLanguage().length() == 0) ? "" : "@" + literal.getLanguage());
        }
    }

   

}

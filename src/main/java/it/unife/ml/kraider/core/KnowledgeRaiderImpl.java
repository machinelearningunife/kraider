/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.core;

import it.unife.ml.kraider.api.KRaiderConfigurationBuilder;
import it.unife.ml.kraider.api.KRaiderConfiguration;
import it.unife.ml.kraider.api.KnowledgeRaider;
import it.unife.ml.kraider.api.TripleExtractorFactory;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import it.unife.ml.kraider.extractors.sparql.SPARQLEndpoints;
import it.unife.ml.kraider.utilities.SetLinkedBlockingQueue;
import java.io.IOException;
import java.util.concurrent.CopyOnWriteArrayList;
import it.unife.ml.kraider.api.KTriple;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class KnowledgeRaiderImpl implements KnowledgeRaider {

    private final Logger logger = LoggerFactory.getLogger(KnowledgeRaiderImpl.class);

    private final CopyOnWriteArrayList<TripleExtractorFactory> extractorFactoryList;

    private SPARQLEndpoints sparqlEndpoints;

    /**
     * Statistics about all the extracted triples. I.e. if the extracted triple
     * was already extracted previously we count it.
     */
    protected TripleStatisticsImpl extractedTriplesStatistics = new TripleStatisticsImpl();

    /**
     * List of knowledge resource raiders currently used
     */
    protected List<KnowledgeResourceRaider> krrs;

    public KnowledgeRaiderImpl(CopyOnWriteArrayList<TripleExtractorFactory> factoryQueue) throws IOException {
        this.extractorFactoryList = factoryQueue;
        // add default extractor factories
        factoryQueue.add(new SPARQLEndpointTripleExtractorFactoryImpl());
        factoryQueue.add(new LDDTripleExtractorFactoryImpl());
        factoryQueue.add(new EmbeddedTripleExtractorFactoryImpl());
//        updateEndpoints();
    }

    @Override
    public List<BlockingQueue<KTriple>> raidKnowledge(String subject, String predicate, String object) {
        return raidKnowledge(subject, predicate, object, null);
    }

    @Override
    public List<BlockingQueue<KTriple>> raidKnowledge(String subject, String predicate, String object, KRaiderConfiguration configuration) {

        krrs = new ArrayList<>();
        extractedTriplesStatistics.reset();

        List<BlockingQueue<KTriple>> queues = new ArrayList<>(3);

        try {
            queues.add(raidKnowledge(subject, configuration, false, false));
            queues.add(raidKnowledge(predicate, configuration, false, false));
            queues.add(raidKnowledge(object, configuration, false, false));
        } catch (MalformedURLException murle) {
            logger.error("NOT Valid URL: " + murle.getMessage());
        }
        return queues;
    }

    @Override
    public BlockingQueue<KTriple> raidKnowledge(String resource) throws MalformedURLException {
        return raidKnowledge(resource, null, false, true);
    }

    @Override
    public BlockingQueue<KTriple> raidKnowledge(KRaiderConfiguration configuration, boolean wait) throws MalformedURLException {
        return raidKnowledge(null, configuration, wait, true);
    }

    public BlockingQueue<KTriple> raidKnowledge(String resource, KRaiderConfiguration configuration, boolean wait, boolean resetStatistics) throws MalformedURLException {

        if (resetStatistics) {
            extractedTriplesStatistics.reset();
        }
        
        if (configuration == null) {
            configuration = getDefaultConfiguration(resource);
        }

        if (krrs == null) {
            krrs = new ArrayList<>();
        }

//        KnowledgeResourceRaider krr = new KnowledgeResourceRaider(resourceURL, tripleQueue, extractorFactoryQueue, sparqlEndpoints);
        KnowledgeResourceRaider krr = new KnowledgeResourceRaider(configuration);
        krrs.add(krr);
        Thread thread = new Thread(krr);
        logger.info("Started knowledge extraction for resource: " + configuration.getResource());
        thread.start();
        if (wait) {
            try {
                thread.join();
            } catch (InterruptedException ie) {
                logger.error(ie.getLocalizedMessage());
            }
        }
        return configuration.getTripleQueue();
    }

    @Override
    public TripleStatisticsImpl getExtractedTriplesStatistics() {
        return extractedTriplesStatistics;
    }

    @Override
    public KRaiderConfiguration getDefaultConfiguration(String resource) {
        BlockingQueue<KTriple> tripleQueue = new SetLinkedBlockingQueue<>();
//        URL resourceURL = new URL(resource);

        KRaiderConfiguration configuration = new KRaiderConfigurationBuilder()
                .availableExtractorFactories(extractorFactoryList)
                .extractedTriplesStatistics(extractedTriplesStatistics)
                .tripleQueue(tripleQueue)
                .resource(resource)
                .buildConfiguration();

        return configuration;
    }

    @Override
    public void stop() {
        if (krrs != null) {
            krrs.forEach((krr) -> {
                krr.stop();
            });
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.core;

import it.unife.ml.kraider.data.Filters;
import it.unife.ml.kraider.exception.InvalidRecursionDepthException;
import java.net.URL;
import org.semanticweb.owlapi.model.IRI;

/**
 *
 * Class used to generate queries.
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface QueryMaker {

//    public String makeInstanceFragmentQueryString(IRI subject, int recursionDepth)
//            throws InvalidRecursionDepthException;
//
//    public String makeInstanceFragmentQueryString(IRI subject, IRI predicate, int recurisonDepth)
//            throws InvalidRecursionDepthException;

    public String makeSameAsQueryString(IRI resource, Filters filters);

    public String makeEquivalentClassesQueryString(IRI resource, Filters filters);

    public String makeEquivalentPropertyQueryString(IRI resource, Filters filters);
    
    /**
     * Given subject and predicate identified by IRIs and a recursion depth,
     * this method generates the query string for extracting the knowledge
     * fragment of the form (subject, predicate, ?o_1, ..., ?p_k, ?o_k), 
     * where k = recursionDepth.
     * 
     * @param subject
     * @param predicate
     * @param recursionDepth
     * @return the query string
     */
    public String makeEntityFragmentQueryString(IRI subject, IRI predicate, Filters filters, int recursionDepth)
            throws InvalidRecursionDepthException;

    /**
     * Given a subject identified by an IRI and a recursion depth,
     * this method generates the query string for extracting the knowledge
     * fragment of the form (subject, ?p_1, ?o_1, ..., ?p_k, ?o_k), 
     * where k = recursionDepth.
     *
     * @param subject
     * @param recursionDepth 
     * @return the query string.
     */
    public String makeEntityFragmentQueryString(IRI subject, Filters filters, int recursionDepth)
            throws InvalidRecursionDepthException;

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.core;

import it.unife.ml.kraider.api.Origin;
import it.unife.ml.kraider.api.TripleExtractor;
import java.net.URL;
import java.util.Objects;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class OriginImpl implements Origin {

    public Class<? extends TripleExtractor> method;

    public URL domain;
  
    public OriginImpl(URL domain, Class<? extends TripleExtractor> method) {
        this.domain = domain;
        this.method = method;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof OriginImpl)) {
            return false;
        }
        OriginImpl cc = (OriginImpl) o;
        return cc.method == this.method
                && cc.domain.equals(this.domain);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.domain, this.method);
    }

    @Override
    public int compareTo(Origin o) {
        int res = this.domain.toString().compareToIgnoreCase(o.getDomain().toString());
        if (res != 0) {
            return res;
        }
        return this.method.getName().compareTo(o.getMethod().getName());
        
    }

    @Override
    public Class<? extends TripleExtractor> getMethod() {
        return method;
    }

    @Override
    public URL getDomain() {
        return domain;
    }
}

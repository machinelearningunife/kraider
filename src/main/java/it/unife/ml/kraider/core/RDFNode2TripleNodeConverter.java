/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.core;

import org.apache.jena.rdf.model.AnonId;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.RDFVisitor;
import org.apache.jena.rdf.model.Resource;
import it.unife.ml.kraider.api.KTripleNode;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class RDFNode2TripleNodeConverter implements RDFVisitor {

    @Override
    public KTripleNode visitBlank(Resource r, AnonId id) {
        return new KNodeBlankImpl(r, id);
    }

    @Override
    public KTripleNode visitURI(Resource r, String uri) {
        return new KNodeURIResourceImpl(r);
    }

    @Override
    public KTripleNode visitLiteral(Literal l) {
        return new KNodeLiteralImpl(l);
    }
    
}

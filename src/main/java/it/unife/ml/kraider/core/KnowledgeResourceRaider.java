/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.core;

import it.unife.ml.kraider.api.KRaiderConfiguration;
import it.unife.ml.kraider.api.TripleExtractorFactory;
import it.unife.ml.kraider.api.TripleExtractorManager;
import java.net.URL;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import it.unife.ml.kraider.extractors.sparql.SPARQLEndpoints;
import it.unife.ml.kraider.api.KTriple;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class KnowledgeResourceRaider implements Runnable {

    private URL resource;

    private BlockingQueue<KTriple> tripleQueue;

    private LinkedBlockingQueue<TripleExtractorFactory> extractorFactoryQueue;

    private SPARQLEndpoints sparqlEndpoints;

    KRaiderConfiguration configuration;
    
    private TripleExtractorManager tripleExtractorManager;

//    public KnowledgeResourceRaider(URL resource,
//            BlockingQueue<Triple> tripleQueue,
//            LinkedBlockingQueue<TripleExtractorFactory> factoryQueue,
//            SPARQLEndpoints sparqlEndpoints) {
//        this.resource = resource;
//        this.tripleQueue = tripleQueue;
//        this.extractorFactoryQueue = factoryQueue;
//        this.sparqlEndpoints = sparqlEndpoints;
//        this.configuration = new Configuration(
//                resource,
//                tripleQueue,
//                extractorFactoryQueue,
//                6,
//                sparqlEndpoints,
//                true,
//                3);
//    }
    
    public KnowledgeResourceRaider(KRaiderConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public void run() {
        // create configuration
        
        tripleExtractorManager = new DefaultTripleExtractorManager(configuration);
//        man.useConfiguration(configuration);
        tripleExtractorManager.startExtraction();
    }

    public void stop() {
        tripleExtractorManager.stop();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.core;

import it.unife.ml.kraider.api.Origin;
import java.net.URL;
import java.util.Comparator;
import java.util.Objects;
import java.util.function.Function;
import org.apache.jena.rdf.model.RDFNode;
import org.semanticweb.owlapi.vocab.OWLRDFVocabulary;
import it.unife.ml.kraider.api.KTriple;
import it.unife.ml.kraider.api.KTripleNode;
import org.apache.jena.graph.Node;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class KTripleImpl implements KTriple {

    protected KTripleNode subject;

    protected KTripleNode predicate;

    protected KTripleNode object;

    protected Origin origin;
    
    public KTripleImpl(){
        
    }

    public KTripleImpl(KTripleNode subject, KTripleNode predicate, KTripleNode object, Origin origin) {
        this.subject = subject;
        this.predicate = predicate;
        this.object = object;
        this.origin = origin;
    }
    
    public KTripleImpl(RDFNode subject, RDFNode predicate, RDFNode object, Origin origin) {
        RDFNode2TripleNodeConverter converter = new RDFNode2TripleNodeConverter();
        this.subject = (KTripleNode) subject.visitWith(converter);
        this.predicate = (KTripleNode) predicate.visitWith(converter);
        this.object = (KTripleNode) object.visitWith(converter);
        this.origin = origin;
    }
    
    public KTripleImpl(Node subject, Node predicate, Node object, Origin origin) {
        Node2KNodeConverter converter = new Node2KNodeConverter();
        this.subject = (KTripleNode) subject.visitWith(converter);
        this.predicate = (KTripleNode) predicate.visitWith(converter);
        this.object = (KTripleNode) object.visitWith(converter);
        this.origin = origin;
    }


    @Override
    public int compareTo(KTriple o) {
        Function<KTriple, KTripleNode> subjectFunction = KTriple::getSubject;
        Function<KTriple, KTripleNode> objectFunction = KTriple::getObject;
        Function<KTriple, KTripleNode> predicateFunction = KTriple::getPredicate;
        
        return Comparator.comparing(subjectFunction.andThen(KTripleNode::toString))
                .thenComparing(predicateFunction.andThen(KTripleNode::toString))
                .thenComparing(objectFunction.andThen(KTripleNode::toString))
                .thenComparing(KTriple::getOrigin)
                .compare(this, o);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof KTriple)) {
            return false;
        }
        KTriple cc = (KTriple) o;
        return cc.getSubject().toString().equalsIgnoreCase(getSubject().toString())
                && cc.getPredicate().toString().equalsIgnoreCase(getPredicate().toString())
                && cc.getObject().toString().equalsIgnoreCase(getObject().toString())
                && cc.getOrigin().getDomain().equals(origin.getDomain());
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getSubject(), this.getPredicate(), this.getObject(), this.getDomain());
    }

    /**
     * @return the subject
     */
    @Override
    public KTripleNode getSubject() {
        return subject;
    }

    /**
     * @return the predicate
     */
    @Override
    public KTripleNode getPredicate() {
        return predicate;
    }

    /**
     * @return the object
     */
    @Override
    public KTripleNode getObject() {
        return object;
    }

    /**
     * @return the domain
     */
    @Override
    public URL getDomain() {
        return origin.getDomain();
    }

    @Override
    public boolean isSameAs() {
        return predicate.toString().equals(OWLRDFVocabulary.OWL_SAME_AS.getIRI().toString());
    }

    /**
     * @return the origin
     */
    @Override
    public Origin getOrigin() {
        return origin;
    }

    @Override
    public String getDomainString() {
        return origin.getDomain().toString();
    }

    @Override
    public boolean isEquivalentClass() {
        return predicate.toString().equals(OWLRDFVocabulary.OWL_EQUIVALENT_CLASS.getIRI().toString());
    }

    @Override
    public boolean isEquivalentProperty() {
        return predicate.toString().equals(OWLRDFVocabulary.OWL_EQUIVALENT_PROPERTY.getIRI().toString());
    }

}

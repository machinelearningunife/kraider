/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.core;

import it.unife.ml.kraider.api.Origin;
import it.unife.ml.kraider.api.TripleExtractor;
import it.unife.ml.kraider.api.TripleStatistics;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import it.unife.ml.kraider.api.KTriple;

/**
 * Class used to obtain the statistics of the extracted triples.
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class TripleStatisticsImpl implements TripleStatistics {

    private final Logger logger = LoggerFactory.getLogger(TripleStatisticsImpl.class);

    /**
     * Count the number of extracted triples
     */
    protected int nTriples = 0;

    /**
     * keep count of how many triples a triple extractor method extracted
     */
    protected Map<Class<? extends TripleExtractor>, Integer> methodCount = new HashMap<>();

    /**
     * keep count of how many triples where extracted from a domain
     */
    protected Map<URL, Integer> domainCount = new HashMap<>();

    /**
     * keep count of how many triples where extracted from a domain and by which
     * method
     */
    protected Map<Origin, Integer> originCount = new HashMap<>();

    public synchronized void updateStatistics(Set<KTriple> triples) {
        triples.forEach(t -> {
            this.nTriples++;
            Class<? extends TripleExtractor> extractionMethod = t.getOrigin().getMethod();
            URL domain = t.getDomain();
            OriginImpl o = new OriginImpl(domain, extractionMethod);
            getMethodCount().merge(extractionMethod, 1, Integer::sum);
            getDomainCount().merge(domain, 1, Integer::sum);
            getOriginCount().merge(o, 1, Integer::sum);
        });
        logger.debug("Extracted triples: {}", triples.size());
    }

    public synchronized void mergeStatistics(TripleStatisticsImpl other) {
        this.nTriples += other.getnTriples();
        other.getMethodCount().forEach((k, v) -> getMethodCount().merge(k, v, Integer::sum));
        other.getDomainCount().forEach((k, v) -> getDomainCount().merge(k, v, Integer::sum));
        other.getOriginCount().forEach((k, v) -> getOriginCount().merge(k, v, Integer::sum));
    }

    /**
     * @return the nTriples
     */
    public int getnTriples() {
        return nTriples;
    }

    /**
     * @return the methodCount
     */
    public Map<Class<? extends TripleExtractor>, Integer> getMethodCount() {
        return methodCount;
    }

    /**
     * @return the domainCount
     */
    public Map<URL, Integer> getDomainCount() {
        return domainCount;
    }

    /**
     * @return the originCount
     */
    public Map<Origin, Integer> getOriginCount() {
        return originCount;
    }

    public void printStatistics() {
        logger.info("");
        logger.info("============ Result ============");
        logger.info("Extracted triples: {}", nTriples);
        methodCount.forEach((k, v) -> {
            logger.info("Method: {}\tN. Triples: {}", k.getCanonicalName(), v);
        });
        domainCount.forEach((k, v) -> {
            logger.info("Domain: {}\tN. Triples: {}", k, v);
        });
        originCount.forEach((k, v) -> {
            logger.info("Domain: {}\tMethod: {}\tN. Triples: {}", k.getDomain(), k.getMethod().getCanonicalName(), v);
        });

        logger.info("");
        logger.info("================================");
        logger.info("");
    }

    @Override
    public void reset() {
        nTriples = 0;
        domainCount.clear();
        methodCount.clear();
        originCount.clear();
    }

}

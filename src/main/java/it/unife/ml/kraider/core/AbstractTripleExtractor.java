/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.core;

import it.unife.ml.kraider.api.TripleExtractor;
import it.unife.ml.kraider.api.TripleExtractorManager;
import java.util.Objects;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public abstract class AbstractTripleExtractor implements TripleExtractor {

    protected final int id;

    protected TripleExtractorManager manager;

    protected boolean running = false;

    public AbstractTripleExtractor(int id, TripleExtractorManager manager) {
        this.id = id;
        this.manager = manager;
    }

    @Override
    public int getId() {
        return id;
    }

    /**
     * Returns true if the extractor is running, false otherwise
     * 
     * @return the running
     */
    public boolean isRunning() {
        return running;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (o == this) {
//            return true;
//        }
//        if (!(o instanceof TripleExtractor)) {
//            return false;
//        }
//        TripleExtractor cc = (TripleExtractor) o;
//        return cc.getId() == id;
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hashCode(this.getId());
//    }
}

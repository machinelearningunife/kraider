/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.core;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Maps;
import it.unife.ml.kraider.api.KRaiderConfiguration;
import it.unife.ml.kraider.api.TripleExtractorManager;
import static it.unife.ml.probowlapi.utilities.GeneralUtils.safe;
import it.unife.ml.kraider.api.TripleExtractor;
import it.unife.ml.kraider.api.TripleExtractorFactory;
import it.unife.ml.kraider.data.DefaultJobTable;
import it.unife.ml.kraider.data.Job;
import it.unife.ml.kraider.data.JobStatus;
import it.unife.ml.kraider.data.JobStatus.Status;
import it.unife.ml.kraider.data.JobTable;
import it.unife.ml.kraider.data.NamedResourceJob;
import it.unife.ml.kraider.utilities.HTTPUtils;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;
import org.semanticweb.owlapi.model.IRI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import it.unife.ml.kraider.api.KTriple;
import it.unife.ml.kraider.caching.CachedTripleExtractor;
import it.unife.ml.kraider.caching.KRaiderCache;
import it.unife.ml.kraider.extractors.JobCreator;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.jena.graph.Triple;
//import org.apache.jena.riot.web.HttpOp;

/**
 * This class manages the triple extractors and it is responsible for the
 * assignment of jobs to a triple extractor.
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class DefaultTripleExtractorManager implements TripleExtractorManager {

    private static final Logger logger = LoggerFactory.getLogger(DefaultTripleExtractorManager.class);

    protected KRaiderConfiguration configuration;

    protected JobTable jobTable;

    protected Map<Integer, TripleExtractor> extractorsMap = new ConcurrentHashMap<>();

    protected int createdThreads = 0;

    protected int deletedThreads = 0;

    protected List<TripleExtractorFactory> extractorFactoryList;

    protected long nExtractedTriples;

    protected boolean stop;

    protected final BlockingQueue<KTriple> tripleQueue;

    protected KRaiderCache cache;

    protected CachedTripleExtractor cachedTripleExtractor;

    private ReentrantLock extractorsLock = new ReentrantLock();

    protected BiMap<Thread, Integer> extractorThreads = HashBiMap.create();

    // this map save the last time an extractor performed an activity
    protected Map<Integer, Long> extractorActivities = new ConcurrentHashMap<>();

    protected Map<Integer, Job> currentlyAssignedJobs = new ConcurrentHashMap<>();

    private final long maxInactivityThread = 120 * 1000;

    /**
     * Statistic about all the triples added to the queue. I.e. if the extracted
     * triple was already extracted previously we DO NOT count it.
     */
//    protected Statistics addedTriplesStatistics;
    //protected int currentThreads = 0;
    public DefaultTripleExtractorManager(KRaiderConfiguration configuration) {
        this.extractorThreads = Maps.synchronizedBiMap(extractorThreads);
        this.configuration = configuration;
        this.extractorFactoryList = this.getConfiguration().getAvailableExtractorFactories();
        this.tripleQueue = configuration.getTripleQueue();
        jobTable = new DefaultJobTable(configuration.getJobPollTimeout());
        if (configuration.isCache()) {
            cache = KRaiderCache.getInstance();
            cachedTripleExtractor = new CachedTripleExtractor(cache, configuration);
        }

//        int connTimeout = 5;
//        RequestConfig config = RequestConfig.custom()
//                .setConnectTimeout(connTimeout * 1000)
//                .setConnectionRequestTimeout(connTimeout * 1000)
//                .setSocketTimeout(connTimeout * 1000).build();
//        CloseableHttpClient client
//                = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
//        HttpOp.setDefaultHttpClient(client);
    }

//    @Override
//    public void useConfiguration(KRaiderConfiguration configuration) {
//        this.configuration = configuration;
//        this.extractorFactoryQueue = this.getConfiguration().getAvailableExtractorFactories();
//    }
    @Override
    public void startExtraction() {
        this.stop = false;
        // add the first job
        IRI resource = getConfiguration().getResource();
        URL domain;
        try {
            domain = HTTPUtils.extractDomain(resource.toURI().toURL());
        } catch (MalformedURLException murle) {
            String errorMsg = murle.getLocalizedMessage();
            logger.error(errorMsg);
            throw new RuntimeException(errorMsg);
        }
        Job firstJob = new NamedResourceJob(domain, resource, configuration.getRecursionDepth(), new JobStatus());
        Set<Job> jobsToAdd = handleCashingConcurrent(null, null, Collections.singleton(firstJob));
        jobTable.addJobs(jobsToAdd);

//        startNewExtractors();
        synchronized (this) {
            if (!stop) {
                startNewExtractors();
                Thread t = new Thread(new ExtractorThreadsChecker());
                t.start();
                try {
                    if (extractorsMap.size() > 0) {
                        this.wait();
                        if (configuration.isCache()) {
//                            logger.debug("Caching Unanswerable jobs");
//                            Job dummyJob = new NamedResourceJob(, resource, configuration.getRecursionDepth(), new JobStatus());

                            logger.debug("Terminating {}", cachedTripleExtractor.getClass());
                            cachedTripleExtractor.terminateExtraction();
                        }
                    }
                } catch (InterruptedException ie) {
                    logger.error(ie.getLocalizedMessage());
                }
            }
        }
        // TO DO: get the list of available extraction services.
    }

    // this method is invoked by TripleExtractor subclasses
    @Override
    public Job requireJob(int extractorId, Set<KTriple> extractedKTriples, Job prevJob,
            Status prevJobStatus, Set<Job> newJobs) {
        extractorsLock.lock();
        try {
            // update thread activity
            if (extractorThreads.inverse().containsKey(extractorId)) {
                this.extractorActivities.put(extractorId, System.currentTimeMillis());
            } else {
                return null;
            }
        } finally {
            extractorsLock.unlock();
        }

        // update the previously computed job
        if (prevJob != null) {
            // boolean d = prevJob.getSubject().equals(IRI.create("http://www.wikidata.org/entity/Q567"));
            jobTable.updateJobStatus(prevJob, prevJobStatus, extractorFactoryList.size());
            logger.debug("{} with id: {}: status: {}. job:  subject: {}, domain: {}, filters: {}, failed methods: {}",
                    prevJob.getJobStatus().getMethod().getSimpleName(),
                    extractorId,
                    prevJobStatus,
                    prevJob.getSubject(),
                    prevJob.getDomain(),
                    prevJob.getFilters(),
                    prevJob.getJobStatus().getFailedMethods());
        }
        if (!safe(extractedKTriples).isEmpty() && !terminationCriteria()) {
            configuration.getExtractedTriplesStatistics().updateStatistics(extractedKTriples);

            newJobs = handleCashingConcurrent(extractedKTriples, prevJob, newJobs);
//            Set<Triple> addedTriples = new HashSet<>();
//            for (Triple t : resultTriples) {
//                if (configuration.tripleQueue.add(t)) {
//                    addedTriples.add(t);
//                }
//            }
//            getAddedTriplesStatistics().updateStatistics(addedTriples);
            configuration.getTripleQueue().addAll(extractedKTriples);
        }

        Job nextJob = null;
        if (!terminationCriteria()) {
            // add the new jobs that must be executed
            if (!safe(newJobs).isEmpty()) {
                jobTable.addJobs(newJobs);
            }
            TripleExtractor e = extractorsMap.get(extractorId);
//            jobTable.assignJob(nextJob, e.getClass());
            if (e != null) {
                nextJob = jobTable.getNextJob(e.getClass());
            }

                if (nextJob == null) {
                    // è passato il timeout per la richiesta del next job. Cosa fare? uccidere il thread?
                    // settare il thread a stop per liberare risorse
                    // modificare lista dei thread
                    synchronized (this) {
                        closeExtractor(extractorId);
                        if (extractorsMap.isEmpty()) {
                            this.notify();
                        }
                    }
                } else {
                extractorsLock.lock();
                try {
//            TripleExtractor e = extractorsMap.get(extractorId);
//            jobTable.assignJob(nextJob, e.getClass());
                    currentlyAssignedJobs.put(extractorId, nextJob);
                } finally {
                    extractorsLock.unlock();
                }
            }
        }

                // allocate new extractors
                if (!stop) {
                    startNewExtractors();
                }
        return nextJob;
    }

    /**
     * This method is invoked in order to start new extractors in case there is
     * enough jobs to do. The number of new extractors to create is equal to
     * min(nMaxAllowedExtractors, jobTableSize) - nAlreadyCreatedExtractors
     */
    protected synchronized void startNewExtractors() {
            int nJobTentatives = 0;
            for (TripleExtractorFactory extractorFactory : extractorFactoryList) {
                nJobTentatives += jobTable.getNAvailableJobsByExtractor(extractorFactory.getTripleExtractorType());
            }

            if (nJobTentatives == 0) {
                return;
            }
            int nExtractorsToCreate = Math.min(getConfiguration().getnMaxExtractors(), nJobTentatives);
            nExtractorsToCreate -= extractorsMap.size();

            int createdExtractors = 0;

            if (nExtractorsToCreate < extractorFactoryList.size()) {
                for (TripleExtractorFactory extractorFactory : extractorFactoryList) {
                    long availableJobsByExtractor = jobTable.getNAvailableJobsByExtractor(extractorFactory.getTripleExtractorType());
                    if (availableJobsByExtractor > 0 && nExtractorsToCreate > 0) {
                        createExtractor(createdThreads, extractorFactory);
                        createdExtractors++;
                        nExtractorsToCreate--;
                    } else {
                        return;
                    }
                }
            }

//        List<Long> availableJobsByExtractors = new ArrayList<>(extractorFactoryList.size());
            for (TripleExtractorFactory extractorFactory : extractorFactoryList) {
                long availableJobsByExtractor = jobTable.getNAvailableJobsByExtractor(extractorFactory.getTripleExtractorType());
                if (availableJobsByExtractor > 0) {
//                availableJobsByExtractors.add(jobTable.getNAvailableJobsByExtractor(extractorFactory.getTripleExtractorType()));
                    long nExtractorToCreateByExtractor = nExtractorsToCreate * availableJobsByExtractor / nJobTentatives;
                    for (int i = 0; i < nExtractorToCreateByExtractor; i++) {
                        createExtractor(createdThreads, extractorFactory);
                        createdExtractors++;
                    }

                }

                nExtractorsToCreate -= createdExtractors;
            }

            for (int i = 0; i < nExtractorsToCreate; i++) {
                TripleExtractorFactory extractorFactory = obtainRandomExtractorFactory();
                createExtractor(createdThreads, extractorFactory);
            }
        }

    /**
     * @return the deletedThreads
     */
    public synchronized int getDeletedThreads() {
            return deletedThreads;
        }

    /**
     * This method is invoked to obtain an instance of TripleExtractor that must
     * be exploited for extraction. The choice of which kind of TripleExtractor
     * must be used depends of some criteria which are dependent on the
     * TripleExtractorManager implementation.
     *
     * @return the TripleExtractor that must be used for triple extraction.
     */
    protected synchronized TripleExtractorFactory obtainRandomExtractorFactory() {
        // TO DO: get the list of available extraction services.
//        TripleExtractor extractor = null;
//        for (TripleExtractorFactory extractorFactory : extractorFactoryQueue) {
//            if (extractorFactory instanceof SPARQLEndpointTripleExtractorFactoryImpl) {
//                extractor = extractorFactory.getTripleExtractor(createdThreads, this);
//            }
//        }
//        if (extractor == null) {
//            extractor = (new SPARQLEndpointTripleExtractorFactoryImpl()).getTripleExtractor(createdThreads, this);
//        }

        int randomNum = ThreadLocalRandom.current().nextInt(0, extractorFactoryList.size());
        return extractorFactoryList.get(randomNum);
        // Then choose which extraction class must be used.
//        
//        Class<?> clazz = Class.forName("com.foo.MyClass");
//Constructor<?> constructor = clazz.getConstructor(String.class, Integer.class);
//Object instance = constructor.newInstance("stringparam", 42);
    }

    /**
     * @return the configuration
     */
    @Override
    public KRaiderConfiguration getConfiguration() {
        return configuration;
    }

    public synchronized boolean terminationCriteria() {
        return stop;
    }

    @Override
    public synchronized void stop() {
        stop = true;

        extractorsLock.lock();
        try {
            extractorsMap.forEach((extractorId, extractor) -> {
                closeExtractor(extractorId);
            });

//        cachedTripleExtractor.terminateExtraction();
            if (!extractorsMap.isEmpty()) {
                logger.error("Not all extraction threads stopped");
            }
        } finally {
            extractorsLock.unlock();
        }
        this.notify();
    }

    /**
     * @return the addedTriplesStatistics
     */
//    public Statistics getAddedTriplesStatistics() {
//        return addedTriplesStatistics;
//    }
//    private synchronized Set<Job> handleCashing(Set<KTriple> extractedKTriples, Job prevJob, Set<Job> newJobs) {
//
//        Set<Job> jobsToAdd = new HashSet<>();
//
//        if (newJobs != null) {
//            jobsToAdd.addAll(newJobs);
//        }
//
//        if (configuration.isCache() && !safe(newJobs).isEmpty()) {
//
//            // add new triples in cache
//            if (!safe(extractedKTriples).isEmpty()) {
//                Set<Triple> extractedTriples = new HashSet<>();
//                for (KTriple t : extractedKTriples) {
//                    Triple triple = new Triple(t.getSubject().asNode(),
//                            t.getPredicate().asNode(),
//                            t.getObject().asNode());
//                    extractedTriples.add(triple);
//                }
//                cache.put((NamedResourceJob) prevJob, extractedTriples);
//                logger.trace("Caching {} triples of job: subject: {}, domain: {}, filters: {}",
//                        extractedKTriples.size(),
//                        prevJob.getSubject(), prevJob.getDomain(), prevJob.getFilters());
//            }
//            // newJobsModified is true if the list of new jobs was modified
//            boolean newJobsModified = true;
//            while (newJobsModified) {
//                Set<Job> cachedNewJobs = new LinkedHashSet<>();
//                newJobsModified = jobsToAdd.removeIf((job) -> {
//                    if (job instanceof NamedResourceJob) {
//                        JobStatus jobStatus = jobTable.getJobStatus(job);
//                        if (jobStatus != null //                                && (jobStatus.getStatus() == Status.COMPLETED || jobStatus.getStatus() == Status.NOT_ANSWERABLE)
//                                ) {
//                            return true;
//                        } else {
//                            Set<KTriple> cachedTriples = cachedTripleExtractor.extractCachedTriples((NamedResourceJob) job, jobTable);
//                            if (cachedTriples != null && !cachedTriples.isEmpty()) {
//                                tripleQueue.addAll(cachedTriples);
//                                cachedNewJobs.addAll(JobCreator.createJobsFromTriples(configuration, job, cachedTriples));
//                                jobTable.addCompletedJob(job, CachedTripleExtractor.class);
//                                logger.debug("Found {} cached triples for job: subject: {}, domain: {}, filters: {}",
//                                        cachedTriples.size(), job.getSubject(), job.getDomain(), job.getFilters());
//                                configuration.getExtractedTriplesStatistics().updateStatistics(cachedTriples);
//                                return true;
//                            } else {
//                                logger.trace("Caching triples not found for job: subject: {}, domain: {}, filters: {}",
//                                        job.getSubject(), job.getDomain(), job.getFilters());
//                                return false;
//                            }
//                        }
//                    } else {
//                        return false;
//                    }
//
//                });
//                newJobsModified = jobsToAdd.addAll(cachedNewJobs) || newJobsModified;
//            }
//        }
//        return jobsToAdd;
//    }
    // Perché synchronized?
    private Set<Job> handleCashingConcurrent(Set<KTriple> extractedKTriples, Job prevJob, Set<Job> newJobs) {

        Set<Job> jobsToAdd = new HashSet<>();

        if (newJobs != null) {
            jobsToAdd.addAll(newJobs);
        }

        if (configuration.isCache()) {

            // add new triples in cache
            if (!safe(extractedKTriples).isEmpty()) {
                Set<Triple> extractedTriples = new HashSet<>();
                for (KTriple t : extractedKTriples) {
                    Triple triple = new Triple(t.getSubject().asNode(),
                            t.getPredicate().asNode(),
                            t.getObject().asNode());
                    extractedTriples.add(triple);
                }
                cache.put((NamedResourceJob) prevJob, extractedTriples);
                logger.trace("Caching {} triples of job: subject: {}, domain: {}, filters: {}",
                        extractedKTriples.size(),
                        prevJob.getSubject(), prevJob.getDomain(), prevJob.getFilters());
            }
            if (!jobsToAdd.isEmpty()) {
                // newJobsModified is true if the list of new jobs was modified
                boolean newJobsModified = true;
                while (newJobsModified) {
                    Set<Job> cachedNewJobs = ConcurrentHashMap.newKeySet();
                    Set<Job> jobsToDelete = ConcurrentHashMap.newKeySet();

                    jobsToAdd.parallelStream().forEach((job) -> {
                        if (job instanceof NamedResourceJob) {
                            JobStatus jobStatus = jobTable.getJobStatus(job);
                            if (jobStatus != null //                                && (jobStatus.getStatus() == Status.COMPLETED || jobStatus.getStatus() == Status.NOT_ANSWERABLE)
                                    ) {
                                // nothing
                            } else {
                                Set<KTriple> cachedTriples = cachedTripleExtractor.extractCachedTriples((NamedResourceJob) job, jobTable);
                                if (cachedTriples != null && !cachedTriples.isEmpty()) {
                                    tripleQueue.addAll(cachedTriples);
                                    jobsToDelete.add(job);
                                    cachedNewJobs.addAll(JobCreator.createJobsFromTriples(configuration, job, cachedTriples));
                                    jobTable.addCompletedJob(job, CachedTripleExtractor.class);
                                    logger.debug("Found {} cached triples for job: subject: {}, domain: {}, filters: {}",
                                            cachedTriples.size(), job.getSubject(), job.getDomain(), job.getFilters());
                                    configuration.getExtractedTriplesStatistics().updateStatistics(cachedTriples);
                                } else {
                                    logger.trace("Caching triples not found for job: subject: {}, domain: {}, filters: {}",
                                            job.getSubject(), job.getDomain(), job.getFilters());
                                }
                            }
                        }

                    });
                    newJobsModified = jobsToAdd.removeAll(jobsToDelete);
                    newJobsModified = jobsToAdd.addAll(cachedNewJobs) || newJobsModified;

                }
            }
        }
        return jobsToAdd;
    }

    private void closeExtractor(Integer extractorId) {
        extractorsLock.lock();
        try {
            TripleExtractor extractor = extractorsMap.get(extractorId);
            if (extractor != null) {
                logger.debug("Closing extractor type: {} id: {}", extractor.getClass(), extractorId);
                extractorsMap.remove(extractorId);
                extractor.terminateExtraction();
                deletedThreads++;
                extractorThreads.inverse().remove(extractorId);
            }
            String activeExtractors = extractorsMap.keySet().stream().map(String::valueOf).collect(Collectors.joining(","));
            logger.debug("# active extractors: {}. Active extractor ids: {}", extractorsMap.size(), activeExtractors);
        } finally {
            extractorsLock.unlock();
        }
    }

    private void createExtractor(Integer extractorId, TripleExtractorFactory extractorFactory) {
        extractorsLock.lock();
        try {
            TripleExtractor extractor = extractorFactory.getTripleExtractor(createdThreads, this);
            extractorsMap.put(createdThreads, extractor);
            Thread t = new Thread(extractor);
            t.start();
            logger.debug("Created 1 extractor of type {} with id: {}", extractor.getClass(), createdThreads);
            extractorThreads.put(t, extractorId);
            createdThreads++;
        } finally {
            extractorsLock.unlock();
        }
    }

    private class ExtractorThreadsChecker implements Runnable {

        @Override
        public void run() {
            try {
                while (!terminationCriteria()) {
                    // check if extractor thread are active every 10 seconds
                    TimeUnit.SECONDS.sleep(10);
                    logger.debug("Checking for inactive extractors");
                    extractorsLock.lock();
                    try {
                        BiMap<Thread, Integer> extractorThreadMapCopy = HashBiMap.create(extractorThreads);
                        extractorThreadMapCopy.entrySet().forEach((entry) -> {
                            long currentTime = System.currentTimeMillis();
                            long lastThreadActivityTime = currentTime - extractorActivities.get(entry.getValue());
                            if (!entry.getKey().isAlive() || lastThreadActivityTime > maxInactivityThread) {
                                Integer extractorId = entry.getValue();
                                closeExtractor(extractorId);
                                Job job = currentlyAssignedJobs.remove(extractorId);
                                if (job != null) {
                                    job.getJobStatus().getStatus();
                                    jobTable.updateJobStatus(job, Status.FAILURE, extractorFactoryList.size());
                                    job.getJobStatus().getStatus();
                                    logger.debug("{} with id: {}: status: {}. job:  subject: {}, domain: {}, filters: {}, failed methods: {}",
                                            job.getJobStatus().getMethod().getSimpleName(),
                                            extractorId,
                                            Status.FAILURE,
                                            job.getSubject(),
                                            job.getDomain(),
                                            job.getFilters(),
                                            job.getJobStatus().getFailedMethods());
                                }
                                if (lastThreadActivityTime > maxInactivityThread) {
                                    logger.warn("The extractor with id: {} was inactive for too long", extractorId);
                                    entry.getKey().interrupt();
                                } else {
                                    entry.getKey().interrupt();
                                    logger.warn("The extractor with id: {} was not running", extractorId);
                                }
                            }
                        });
                        if (extractorThreads.isEmpty()) {
                            stop();
                        } else {
                            String activeExtractors = extractorsMap.keySet().stream().map(String::valueOf).collect(Collectors.joining(","));
                            String states = extractorThreads.keySet().stream().map((k) -> k.getState().toString()).collect(Collectors.joining(","));
                            logger.debug("# active extractors: {}. Active extractor ids: {} statuses: {}", extractorsMap.size(), activeExtractors, states);
                        }
                    } catch (Exception ex) {
                        StringWriter p = new StringWriter();
                        ex.printStackTrace(new PrintWriter(p));
                        logger.error(ex.getMessage());
                        logger.error(p.toString());
                    } finally {
                        extractorsLock.unlock();
                    }
                }
            } catch (InterruptedException ex) {
                throw new RuntimeException("ExtractorThreadsChecker ended abnormally: " + ex);
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new RuntimeException("ExtractorThreadsChecker ended abnormally: " + ex.getMessage());
            }

        }
    }

}

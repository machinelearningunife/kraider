/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.core;

import it.unife.ml.kraider.api.KTripleNode;
import org.apache.jena.graph.*;
import org.apache.jena.graph.impl.LiteralLabel;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class Node2KNodeConverter implements NodeVisitor {

    @Override
    public Object visitAny(Node_ANY it) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public KTripleNode visitBlank(Node_Blank it, BlankNodeId id) {
        return new KNodeBlankImpl(it, id);
    }

    @Override
    public Object visitLiteral(Node_Literal it, LiteralLabel lit) {
        return new KNodeLiteralImpl(it);
    }

    @Override
    public Object visitURI(Node_URI it, String uri) {
        return new KNodeURIResourceImpl(it, uri);
    }

    @Override
    public Object visitVariable(Node_Variable it, String name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object visitTriple(Node_Triple node_triple, Triple triple) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Object visitGraph(Node_Graph node_graph, Graph graph) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}

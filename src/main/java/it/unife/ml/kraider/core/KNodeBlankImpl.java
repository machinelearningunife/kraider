/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.core;

import it.unife.ml.kraider.api.TripleNodeBlank;
import it.unife.ml.kraider.api.TripleNodeVisitor;
import org.apache.jena.graph.BlankNodeId;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.Node_Blank;
import org.apache.jena.rdf.model.AnonId;
import org.apache.jena.rdf.model.RDFNode;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class KNodeBlankImpl extends KNodeImpl implements TripleNodeBlank {

    public String id;

    public KNodeBlankImpl(RDFNode node, AnonId id) {
        super(node);
        this.id = id.getLabelString();
    }

    public KNodeBlankImpl(Node_Blank node, BlankNodeId id) {
        super(node);
        this.id = id.getLabelString();
    }

    @Override
    public String toString() {
        return this.id;
    }

    @Override
    public <T> T visitWith(TripleNodeVisitor<T> visitor) {
        return visitor.visit(this);
    }

}

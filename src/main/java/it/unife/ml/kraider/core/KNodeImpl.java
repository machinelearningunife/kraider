/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.core;

import org.apache.jena.rdf.model.RDFNode;
import it.unife.ml.kraider.api.KTripleNode;
import org.apache.jena.graph.Node;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public abstract class KNodeImpl implements KTripleNode {
    
    protected Node node;
      
    public KNodeImpl(RDFNode node) {
        this.node = node.asNode();
    }
    
    public KNodeImpl(Node node) {
        this.node = node;
    }
    
    @Override
    public String toString() {
        return node.toString();
    }

    @Override
    public boolean isURIResource() {
        return node.isURI();
    }

    @Override
    public boolean isAnon() {
        return node.isBlank();
    }

    @Override
    public boolean isLiteral() {
        return node.isLiteral();
    }
    
    @Override
    public Node asNode() {
        return node;
    }
    
}

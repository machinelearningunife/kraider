/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.data;

import java.net.URL;
import org.semanticweb.owlapi.model.IRI;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class ResolveBlankNodeJob extends Job {

    public ResolveBlankNodeJob(URL domain, IRI subject, IRI predicate, int recursionDepth, JobStatus status, Filters filters) {
        super(domain, subject, recursionDepth, status, filters);
    }

    public ResolveBlankNodeJob(URL domain, IRI subject, IRI predicate, int recursionDepth, JobStatus status) {
        super(domain, subject, recursionDepth, status, new Filters());
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.data;

import static it.unife.ml.probowlapi.utilities.GeneralUtils.safe;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.semanticweb.owlapi.vocab.OWLRDFVocabulary;

/**
 * Class used for containing filters.
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class Filters {

    /**
     * List of filters for allowing selection of objects that match them.
     */
    protected SortedSet<String> objectAllowFilterSet = new TreeSet<>();

    /**
     * List of filters for forbidding selection of objects that match them.
     */
    protected SortedSet<String> objectForbidFilterSet = new TreeSet<>();

    /**
     * List of filters for allowing selection of predicates that match them.
     */
    protected SortedSet<String> predicateAllowFilterSet = new TreeSet<>();

    /**
     * List of filters for forbidding selection of objects that match them.
     */
    protected SortedSet<String> predicateForbidFilterSet = new TreeSet<>();

    public Filters() {

    }

    public Filters(SortedSet<String> predicateAllowFilterSet,
            SortedSet<String> predicateForbidFilterSet,
            SortedSet<String> objectAllowFilterSet,
            SortedSet<String> objectForbidFilterSet) {

        this.predicateAllowFilterSet = new TreeSet<>(predicateAllowFilterSet);
        this.predicateForbidFilterSet = new TreeSet<>(predicateForbidFilterSet);
        this.objectAllowFilterSet = new TreeSet<>(objectAllowFilterSet);
        this.objectForbidFilterSet = new TreeSet<>(objectForbidFilterSet);

    }

    public Filters(Filters filters) {
        this(filters.predicateAllowFilterSet,
                filters.predicateForbidFilterSet,
                filters.objectAllowFilterSet,
                filters.objectForbidFilterSet);
    }

    public void addPredicateAllowFilter(String filter) {
        predicateAllowFilterSet.add(filter);
    }

    public void addPredicateForbidFilter(String filter) {
        predicateForbidFilterSet.add(filter);
    }

    public void addObjectAllowFilter(String filter) {
        objectAllowFilterSet.add(filter);
    }

    public void addObjectForbidFilter(String filter) {
        objectForbidFilterSet.add(filter);
    }

    public void addPredicateAllowFilters(Set<String> predicateAllowFilterSet) {
        this.predicateAllowFilterSet.addAll(predicateAllowFilterSet);
    }

    public void addPredicateForbidFilters(Set<String> predicateForbidFilterSet) {
        this.predicateForbidFilterSet.addAll(predicateForbidFilterSet);
    }

    public void addObjectAllowFilters(Set<String> objectAllowFilterSet) {
        this.objectAllowFilterSet.addAll(objectAllowFilterSet);
    }

    public void addObjectForbidFilters(Set<String> objectForbidFilterSet) {
        this.objectForbidFilterSet.addAll(objectForbidFilterSet);
    }

    /**
     * @return the objectAllowFilterSet
     */
    public SortedSet<String> getObjectAllowFilterSet() {
        return objectAllowFilterSet;
    }

    /**
     * @return the objectForbidFilterSet
     */
    public SortedSet<String> getObjectForbidFilterSet() {
        return objectForbidFilterSet;
    }

    /**
     * @return the predicateAllowFilterSet
     */
    public SortedSet<String> getPredicateAllowFilterSet() {
        return predicateAllowFilterSet;
    }

    /**
     * @return the predicateForbidFilterSet
     */
    public Set<String> getPredicateForbidFilterSet() {
        return predicateForbidFilterSet;
    }

    public static Filters getDefaultFilters() {
        return FiltersHolder.DEFAULT_FILTERS;
    }

    /**
     * Auxiliary class used for creating a singleton instance in a thread-safe
     * way. See: <a href="https://dzone.com/articles/singleton-in-java">How to
     * Use Singleton Design Pattern in Java</a>
     */
    private static class FiltersHolder {

        static final Filters DEFAULT_FILTERS;

        static {

            SortedSet<String> predicateAllowFilterSet = new TreeSet<>();

            SortedSet<String> predicateForbidFilterSet = new TreeSet<>();
            predicateForbidFilterSet.add(OWLRDFVocabulary.OWL_SAME_AS.getIRI().toString());
            predicateForbidFilterSet.add(OWLRDFVocabulary.OWL_EQUIVALENT_CLASS.getIRI().toString());
            predicateForbidFilterSet.add(OWLRDFVocabulary.OWL_EQUIVALENT_PROPERTY.getIRI().toString());

            SortedSet<String> objectAllowFilterSet = new TreeSet<>();

            SortedSet<String> objectForbidFilterSet = new TreeSet<>();
            objectForbidFilterSet.add(OWLRDFVocabulary.OWL_THING.getIRI().toString());

            DEFAULT_FILTERS = new Filters(
                    Collections.unmodifiableSortedSet(predicateAllowFilterSet),
                    Collections.unmodifiableSortedSet(predicateForbidFilterSet),
                    Collections.unmodifiableSortedSet(objectAllowFilterSet),
                    Collections.unmodifiableSortedSet(objectForbidFilterSet));
        }
    }

    @Override
    public String toString() {
        String s = "allowed predicates: ";
        s = predicateAllowFilterSet.stream().map((p) -> p + " ").reduce(s, String::concat);
        s += "forbidden predicates: ";
        s = predicateForbidFilterSet.stream().map((p) -> p + " ").reduce(s, String::concat);
        s += "allowed objects: ";
        s = objectAllowFilterSet.stream().map((o) -> o + " ").reduce(s, String::concat);
        s += "forbidden objects: ";
        s = objectForbidFilterSet.stream().map((o) -> o + " ").reduce(s, String::concat);
        return s;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Filters)) {
            return false;
        }
        
        if (this.getClass() != o.getClass()) {
            return false;
        }
            
        Filters other = (Filters) o;

        return safe(other.predicateAllowFilterSet).equals(safe(this.predicateAllowFilterSet))
                && safe(other.predicateForbidFilterSet).equals(safe(this.predicateForbidFilterSet))
                && safe(other.objectAllowFilterSet).equals(safe(this.objectAllowFilterSet))
                && safe(other.objectForbidFilterSet).equals(safe(this.objectForbidFilterSet));
    }

    @Override
    public int hashCode() {
//        return Objects.hash(this.getSubject(), this.getPredicate(), this.getDomain());
        return Objects.hash(this.predicateAllowFilterSet, this.predicateForbidFilterSet, this.objectAllowFilterSet, this.objectForbidFilterSet);
    }

}

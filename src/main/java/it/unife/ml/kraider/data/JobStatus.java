/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.data;

import it.unife.ml.kraider.api.TripleExtractor;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class JobStatus implements Serializable {
    
    public enum Status {
        // the job is waiting to run
        WAITING,
        // the job is running
        RUNNING,
        // the job is completed
        COMPLETED,
        // the extractor failed to complete the job (another extractor must be tried)
        FAILURE,
        // no extractor was able to complete the job
        NOT_ANSWERABLE
    }
    
    protected Class<? extends TripleExtractor> method;
    
    protected Status status;
    
    protected Set<Class<? extends TripleExtractor>> failedMethods = new HashSet<>();
    
    public JobStatus() {
        status = Status.WAITING;
    }

    /**
     * @return the method
     */
    public Class<? extends TripleExtractor> getMethod() {
        return method;
    }

    /**
     * @param method the method to set
     */
    public void setMethod(Class<? extends TripleExtractor> method) {
        this.method = method;
    }

    /**
     * @return the status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Status status) {
        this.status = status;
    }
    
    public void failure() {
        getFailedMethods().add(method);
        status = Status.FAILURE;
    }

    /**
     * @return the failedMethods
     */
    public Set<Class<? extends TripleExtractor>> getFailedMethods() {
        return failedMethods;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.data;

import it.unife.ml.kraider.api.TripleExtractor;
import it.unife.ml.kraider.data.JobStatus.Status;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class DefaultJobTable implements JobTable {

    private ConcurrentMap<Job, Integer> assignedJobsMap;

    private ConcurrentMap<Job, Integer> availableJobsMap;

    private BlockingQueue<Job> availableJobs;

    private static final Logger logger = LoggerFactory.getLogger(DefaultJobTable.class);

    /**
     * Timeout in seconds for polling an element
     */
    private long timeout;

    public DefaultJobTable(long timeout) {
        /**
         * Queue of jobs that were completed or that are assigned and still
         * running.
         */
        assignedJobsMap = new ConcurrentHashMap<>();
        availableJobsMap = new ConcurrentHashMap<>();
        availableJobs = new LinkedBlockingQueue<>();
        this.timeout = timeout;
    }

    @Override
    public synchronized Job getNextJob(Class<? extends TripleExtractor> clazz) {
        Job nextJob = null;
//        Set<Job> removedJobs = new LinkedHashSet<>();
        try {
//            nextJob = availableJobs.poll(timeout, TimeUnit.SECONDS);
//            availableJobs.stream().anyMatch(job -> !job.jobStatus.failedMethods.contains(clazz));

            nextJob = availableJobs.stream().filter(job -> !job.jobStatus.failedMethods.contains(clazz)).findAny().orElse(null);
//            
//            Iterator<Job> iter = availableJobs.iterator();
//            while (iter.hasNext()) {
//                Job job = iter.next();
//                if (!job.jobStatus.failedMethods.contains(clazz)) {
//                    nextJob = job;
//                    iter.remove();
//                    break;
//                }
//            }
//            availableJobs.removeIf((job) -> {
//                if (!job.jobStatus.failedMethods.contains(clazz)) {
//                    removedJobs.add(job);
//                    return true;
//                } else {
//                    return false;
//                }
//            });
//            if (removedJobs.size() > 1) {
//                logger.error("Removed {} jobs from");
//            }
        } catch (Exception ie) {
            // TO DO: what to do????
        }

        if (nextJob != null) {
            assignJob(nextJob, clazz);
            availableJobs.remove(nextJob);
            availableJobsMap.remove(nextJob);
        }
        logger.debug("Available jobs: " + availableJobs.size());
        return nextJob;
    }

    /**
     * This method is used to say that a specific job will be assigned to a
     * particular extraction method (SPARLEndpointExtractor, LODExtractor,
     * TPFExtractor, ecc.)
     *
     * @param job the job that must be assigned
     * @param extractionMethod the extraction method used for job
     */
    public synchronized void assignJob(Job job, Class clazz) {
        job.jobStatus.method = clazz;
        job.jobStatus.status = Status.RUNNING;
        assignedJobsMap.put(job, job.getRecursionDepth());
    }

    @Override
    public synchronized void updateJobStatus(Job job, Status status, int availableExtractors) {
        job.jobStatus.status = status;
        if (job.jobStatus.status == Status.FAILURE) {
            job.jobStatus.getFailedMethods().add(job.jobStatus.getMethod());
            // check if we tested all the available methods!!
            if (job.jobStatus.getFailedMethods().size() == availableExtractors) {
                job.jobStatus.status = Status.NOT_ANSWERABLE;
            } else {
                job.jobStatus.status = Status.WAITING;
                assignedJobsMap.remove(job);
                availableJobs.add(job);
                availableJobsMap.put(job, job.recursionDepth);
            }
        }
        //assignedJobs.add(job);
    }

    @Override
    public synchronized boolean addCompletedJob(Job job, Class<? extends TripleExtractor> clazz) {
        job.jobStatus.status = Status.COMPLETED;
        job.jobStatus.setMethod(clazz);
        if (addCondition(job)) {
            availableJobs.remove(job);
            availableJobsMap.remove(job);
            assignedJobsMap.put(job, job.getRecursionDepth());
            return true;
        } else {
            return false;
        }
    }

    private boolean addCondition(Job job) {
        return assignedJobsMap.getOrDefault(job, -1) < job.getRecursionDepth()
                && availableJobsMap.getOrDefault(job, -1) < job.getRecursionDepth()
                && assignedJobsMap.getOrDefault(new NamedResourceJob(job.domain, job.subject, job.recursionDepth, null, null), -1) < job.recursionDepth
                && availableJobsMap.getOrDefault(new NamedResourceJob(job.domain, job.subject, job.recursionDepth, null, null), -1) < job.recursionDepth
                && assignedJobsMap.getOrDefault(new NamedResourceJob(job.domain, job.subject, job.recursionDepth, null), -1) < job.recursionDepth
                && availableJobsMap.getOrDefault(new NamedResourceJob(job.domain, job.subject, job.recursionDepth, null), -1) < job.recursionDepth;
    }

    @Override
    public synchronized void addJobs(Set<Job> jobs) {
//        jobs.stream().filter((job) -> (
//                (!assignedJobs.containsKey(job) && !availableJobs.contains(job)) ||
//                        (assignedJobs.getOrDefault(job, 0) < job.getRecursionDepth())
//                ))
//                .forEachOrdered((item) -> {
//                    availableJobs.add(item);
//                });

        jobs.parallelStream().filter((job) -> (addCondition(job) //                assignedJobsMap.getOrDefault(job, -1) < job.getRecursionDepth()
                //                && availableJobsMap.getOrDefault(job, -1) < job.getRecursionDepth()
                //                && assignedJobsMap.getOrDefault(new NamedResourceJob(job.domain, job.subject, job.recursionDepth, null, null), -1) < job.recursionDepth
                //                && availableJobsMap.getOrDefault(new NamedResourceJob(job.domain, job.subject, job.recursionDepth, null, null), -1) < job.recursionDepth
                )
        )
                .forEach((item) -> {

                    boolean d = item.getSubject().toString().equals("http://dbpedia.org/resource/Angela_Merkel");

                    availableJobsMap.put(item, item.getRecursionDepth());
                    availableJobs.add(item);
                });
//        jobs.stream().forEachOrdered((job) -> {
//
//            boolean d = job.getSubject().toString().equals("http://dbpedia.org/resource/Angela_Merkel");
//
//            if (assignedJobsMap.getOrDefault(job, -1) < job.getRecursionDepth()
//                    && availableJobsMap.getOrDefault(job, -1) < job.getRecursionDepth()) {
//
//                if (assignedJobsMap.getOrDefault(new NamedResourceJob(job.domain, job.subject, null, job.recursionDepth, null, null), -1) < job.recursionDepth
//                        && availableJobsMap.getOrDefault(new NamedResourceJob(job.domain, job.subject, null, job.recursionDepth, null, null), -1) < job.recursionDepth) {
//
//                    availableJobsMap.put(job, job.getRecursionDepth());
//                    availableJobs.add(job);
//                } else {
//                    int i = 0;
//                }
//            }
//        });
    }

    @Override
    public int size() {
        return availableJobs.size();
    }

    @Override
    public synchronized JobStatus getJobStatus(Job job) {
        Set<Job> jobsInTable = new HashSet<>();
        jobsInTable.addAll(assignedJobsMap.keySet());
        jobsInTable.addAll(availableJobsMap.keySet());

        for (Job jobInTable : jobsInTable) {
            if (job.equals(jobInTable)) {
                return jobInTable.jobStatus;
            }
        }
        return null;
    }

    @Override
    public synchronized long getNAvailableJobsByExtractor(Class<? extends TripleExtractor> extractorType) {
        return availableJobs.parallelStream()
                .filter((job) -> (!job.getJobStatus().failedMethods.contains(extractorType)))
                .count();
    }

}

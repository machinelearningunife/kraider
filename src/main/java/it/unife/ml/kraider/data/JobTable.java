/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.data;

import it.unife.ml.kraider.api.TripleExtractor;
import it.unife.ml.kraider.api.TripleExtractorFactory;
import java.util.Set;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface JobTable {
    
    /**
     * This method is used to get the next available job.
     * @return 
     */
    public Job getNextJob(Class<? extends TripleExtractor> clazz);
    
    /**
     * Adds new jobs to the set of available jobs.
     * @param jobs 
     */
    public void addJobs(Set<Job> jobs);
    
    /**
     * This method is used to say that a specific job will be assigned to a particular
     * extraction method (SPARLEndpointExtractor, LODExtractor, TPFExtractor, ecc.)
     * @param job the job that must be assigned
     * @param extractionMethod the extraction method used for job
     */
//    public void assignJob(Job job, Class extractionMethod);
    
    /**
     * Update the job status. We need the number of available extractors in order
     * to establish if the job can be answered by using another method or not.
     * @param job
     * @param status
     * @param availableExtractors 
     */
    public void updateJobStatus(Job job, JobStatus.Status status, int availableExtractors);
    
    /**
     * This method can be used to add a completed job .
     * @param job 
     */
    public boolean addCompletedJob(Job job, Class<? extends TripleExtractor> clazz);
    
    /**
     * Return the job status of a given job in the table or null if that job is 
     * not in the table.
     * 
     * @param job the job whose status we want to know.
     * @return 
     */
    public JobStatus getJobStatus(Job job);
    
    /**
     * Return the number of available jobs for a given extractor
     * @param clazz
     * @return 
     */
    public long getNAvailableJobsByExtractor(Class<? extends TripleExtractor> extractorType);
    
    
    /**
     * Return the size of available jobs.
     * 
     * @return 
     */
    public int size();
    
}

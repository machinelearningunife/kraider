/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.data;

import java.io.Serializable;
import java.net.URL;
import java.util.Objects;
import org.semanticweb.owlapi.model.IRI;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public abstract class Job implements Serializable {

    protected final URL domain;
    protected final IRI subject;
//    protected final IRI predicate;
    protected final int recursionDepth;
    protected JobStatus jobStatus;
    protected Filters filters;

    public Job(URL domain, IRI subject, int recursionDepth, JobStatus status, Filters filters) {
        this.domain = domain;
        this.subject = subject;
//        this.predicate = predicate;
        this.recursionDepth = recursionDepth;
        this.jobStatus = status;
        this.filters = filters;
    }

    /**
     * @return the domain
     */
    public URL getDomain() {
        return domain;
    }

    /**
     * @return the subject
     */
    public IRI getSubject() {
        return subject;
    }

//    /**
//     * @return the predicate
//     */
//    public IRI getPredicate() {
//        return predicate;
//    }
    /**
     * @return the status
     */
    public JobStatus getJobStatus() {
        return jobStatus;
    }

    /**
     * @param jobStatus the status to set
     */
    public void setJobStatus(JobStatus jobStatus) {
        this.jobStatus = jobStatus;
    }

    /**
     * @return the recursionDepth
     */
    public int getRecursionDepth() {
        return recursionDepth;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Job)) {
            return false;
        }

        if (this.getClass() != o.getClass()) {
            return false;
        }

        Job other = (Job) o;
//        String thisPredicate = this.predicate == null ? "" : this.predicate.toString();
//        String otherPredicate = other.predicate == null ? "" : other.predicate.toString();
        //boolean equalFilters = Objects.toString(other.filters, "").equals(Objects.toString(this.filters, ""));
        boolean equalFilters = other.filters.equals(this.filters);

        return other.domain.toString().equals(this.domain.toString())
                && other.subject.toString().equals(this.subject.toString())
                && equalFilters;

//                && thisPredicate.equals(otherPredicate);
//                && this.recursionDepth == other.recursionDepth;
    }

    @Override
    public int hashCode() {
//        return Objects.hash(this.getSubject(), this.getPredicate(), this.getDomain());
        return Objects.hash(this.getSubject(), this.getFilters(), this.getDomain());
    }

    /**
     * @return the filters
     */
    public Filters getFilters() {
        return filters;
    }

}

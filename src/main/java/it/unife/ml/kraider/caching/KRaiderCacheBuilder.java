///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package it.unife.ml.kraider.caching;
//
//import java.io.File;
//
///**
// *
// * @author Giuseppe Cota <giuseppe.cota@unife.it>
// */
//public class KRaiderCacheBuilder {
//    
//    protected String cacheDir = ".cache" + File.separator;
//
//    // max capacity of entries for in-memory cache
//    protected int inMemoryEntries = 1000;
//
//    // specifies after how many seconds a cached result becomes invalid in in-memory cache
//    protected long inMemoryFreshnessSecond = 5 * 60;
//
//    // max capacity of entries in persistent cache
//    protected int persistentEntries = 4000;
//
//    // specifies after how many seconds a cached result becomes invalid in persistent cache
//    protected long persistentFreshnessSeconds = 1 * 24 * 60 * 60;
//    
//    public KRaiderCacheBuilder() {
//        
//    }
//    
//    public KRaiderCacheBuilder cacheDir(String cacheDir) {
//        this.cacheDir = cacheDir;
//        return this;
//    }
//    
//    public KRaiderCacheBuilder inMemoryEntries(int inMemoryEntries) {
//        this.inMemoryEntries = inMemoryEntries;
//        return this;
//    }
//    
//    public KRaiderCacheBuilder inMemoryFreshnessSecond(long inMemoryFreshnessSecond) {
//        this.inMemoryFreshnessSecond = inMemoryFreshnessSecond;
//        return this;
//    }
//    
//    public KRaiderCacheBuilder persistentEntries(int persistentEntries) {
//        this.persistentEntries = persistentEntries;
//        return this;
//    }
//    
//    public KRaiderCacheBuilder persistentFreshnessSeconds(long persistentFreshnessSeconds) {
//        this.persistentFreshnessSeconds = persistentFreshnessSeconds;
//        return this;
//    }
//    
//    public KRaiderCache buildCache(String name) {
//        KRaiderCache cache = new KRaiderCache(name, cacheDir, inMemoryEntries, 
//                inMemoryFreshnessSecond, persistentEntries, persistentFreshnessSeconds);
//        return cache;
//    }
//    
//}

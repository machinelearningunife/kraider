/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.caching;

import it.unife.ml.kraider.data.Filters;
import it.unife.ml.kraider.data.Job;
import it.unife.ml.kraider.data.NamedResourceJob;
import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Set;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.jena.graph.Triple;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.Configuration;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.spi.loaderwriter.CacheLoadingException;
import org.ehcache.spi.loaderwriter.CacheWritingException;
import org.ehcache.xml.XmlConfiguration;
import org.semanticweb.owlapi.model.IRI;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class UnanswerableCache {

    protected String cacheDir = ".cache" + File.separator;

    private String name = "KRaiderUnanswerableCache";

    private CacheManager cacheManager;

    private Cache<String, JobSet> cache;

    public UnanswerableCache() {
        URL configFile = getClass().getResource("/cache-config.xml");
        Configuration xmlConfig = new XmlConfiguration(configFile);
        System.setProperty("net.sf.ehcache.enableShutdownHook", "true");
        cacheManager = CacheManagerBuilder.newCacheManager(xmlConfig);
        cacheManager.init();

        this.cache = cacheManager.getCache(name, String.class, JobSet.class);

//        System.getProperties().setProperty("java -Dnet.sf.ehcache.use.classic.lru", "true");
//        cacheManager = CacheManagerBuilder
//                .newCacheManagerBuilder().build();
//        cacheManager.init();
//        this.cache = cacheManager
//                .createCache("cacheOfPersonList", CacheConfigurationBuilder
//                        .newCacheConfigurationBuilder(String.class, TripleSet.class, ResourcePoolsBuilder.heap(10)
//                                .offheap(10, MemoryUnit.MB)));
    }

//    public void put(String sparqlQuery, URL domain, Set<Triple> triples) throws CacheWritingException {
//        String hash = getHash(sparqlQuery, domain);
//        try {
//            this.cache.put(hash, new TripleSet(triples));
//        } catch (CacheWritingException cwe) {
//            throw cwe;
//        }
//    }
    public void put(NamedResourceJob job, Set<Job> jobs) throws CacheWritingException {
        String hash = getHash(job);
        try {
            this.cache.put(hash, new JobSet(jobs));
        } catch (CacheWritingException cwe) {
            throw cwe;
        }
    }

//    public Set<Triple> get(String sparqlQuery, URL domain) throws CacheLoadingException {
//        String hash = getHash(sparqlQuery, domain);
//        try {
////            SizeOf sizeOf = SizeOf.newInstance();
////            long deepSize = sizeOf.deepSizeOf(cache);
////            System.out.println(deepSize);
//            TripleSet cachedTriples = cache.get(hash);
//            return (Set<Triple>) cachedTriples;
//        } catch (CacheLoadingException cle) {
//            throw cle;
//        }
//
////        TreeSet<Triple> triples = cachedTriples.stream()
////                .map(obj -> (Triple) obj)
////                .collect(Collectors.toSet());
//    }
    public Set<Job> get(NamedResourceJob job) throws CacheLoadingException {
        String hash = getHash(job);
        try {
            JobSet cachedjobs = cache.get(hash);
            return (Set<Job>) cachedjobs;
        } catch (CacheLoadingException cle) {
            throw cle;
        }
    }

    public void close() {
        cacheManager.close();
    }


//    private String getFilename(String sparqlQuery, URL domain) {
//        return getCacheDir() + getHash(sparqlQuery, domain) + getFileEnding();
//    }
//    private String getHash(String sparqlQuery, URL domain) {
//        String hash = DigestUtils.md5Hex(sparqlQuery + domain);
//        return hash;
//    }
    private String getHash(NamedResourceJob job) {
        IRI subject = job.getSubject();
        Filters filters = job.getFilters();
        URL domain = job.getDomain();
        String additionalString = "";
        if (job.getRecursionDepth() == 0) {
            additionalString = "0";
        }
        String hash = DigestUtils.md5Hex(additionalString + subject + filters + domain);
        return hash;
    }

    /**
     * @return the cacheDir
     */
    public String getCacheDir() {
        return cacheDir;
    }

    /**
     * @param cacheDir the cacheDir to set
     */
    public void setCacheDir(String cacheDir) {
        this.cacheDir = cacheDir;
    }
}

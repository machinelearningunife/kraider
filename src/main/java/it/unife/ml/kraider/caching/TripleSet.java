/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.caching;

import java.util.Collection;
import java.util.LinkedHashSet;
import org.apache.jena.graph.Triple;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class TripleSet extends LinkedHashSet<Triple> {

    public TripleSet() {
        
    }
    
    public TripleSet(final Collection<? extends Triple> c) {
        super(c);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.caching;

import it.unife.ml.kraider.api.KRaiderConfiguration;
import it.unife.ml.kraider.api.KTriple;
import it.unife.ml.kraider.api.Origin;
import it.unife.ml.kraider.api.TripleExtractor;
import it.unife.ml.kraider.core.KTripleImpl;
import it.unife.ml.kraider.core.OriginImpl;
import it.unife.ml.kraider.data.Job;
import it.unife.ml.kraider.data.JobStatus;
import it.unife.ml.kraider.data.JobTable;
import it.unife.ml.kraider.data.NamedResourceJob;
import it.unife.ml.kraider.extractors.JobCreator;
import java.net.URL;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.Triple;
import org.semanticweb.owlapi.model.IRI;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class CachedTripleExtractor implements TripleExtractor {

    protected KRaiderCache cache;

    protected KRaiderConfiguration conf;

    protected boolean stop = false;

    public CachedTripleExtractor(KRaiderCache cache, KRaiderConfiguration conf) {
        this.cache = cache;
        this.conf = conf;
    }

    @Override
    public int getId() {
        return -1;
    }

    @Override
    public void terminateExtraction() {
        stop = true;
        cache.close();
    }

    @Override
    public void run() {
    }

//    public synchronized Set<KTriple> extractCachedTriples(NamedResourceJob job, JobTable jobTable) {
//        // TODO: scrivere funzione ricorsiva per estrarre le triple in cache
//        Set<KTriple> totalCachedTriples = new HashSet<>();
//
//        JobStatus jobStatus = jobTable.getJobStatus(job);
//
//        if (jobStatus == null) {
//            Set<KTriple> jobCachedTriples = getCachedTriples(job);
//            if (jobCachedTriples != null) {
//                totalCachedTriples.addAll(jobCachedTriples);
//                
//                
//                jobTable.addCompletedJob(job, this.getClass());
//                
//                Set<Job> newJobs = JobCreator.createJobsFromTriples(conf, job, jobCachedTriples);
//                for (Job newJob : newJobs) {
//                    if (newJob instanceof NamedResourceJob) {
//                        jobCachedTriples = extractCachedTriples((NamedResourceJob) newJob, jobTable);
//                        if (jobCachedTriples != null) {
//                            totalCachedTriples.addAll(jobCachedTriples);
//                        }
//                    }
//                }
//            } else {
////                job.getJobStatus().setMethod(this.getClass());
////                jobTable.addJobs(Collections.singleton(job));
////                jobTable.updateJobStatus(job, JobStatus.Status.FAILURE, availableExtractors);
//            }
//        }
//
//        return totalCachedTriples;
//    }
    public Set<KTriple> extractCachedTriples(NamedResourceJob job, JobTable jobTable) {
        // TODO: scrivere funzione ricorsiva per estrarre le triple in cache
        Set<KTriple> totalCachedTriples = new HashSet<>();

        if (!stop) {

            JobStatus jobStatus = jobTable.getJobStatus(job);

            if (jobStatus == null) {
                Set<KTriple> jobCachedTriples = getCachedTriples(job);
                if (jobCachedTriples != null) {
                    totalCachedTriples.addAll(jobCachedTriples);

                } else {
//                job.getJobStatus().setMethod(this.getClass());
//                jobTable.addJobs(Collections.singleton(job));
//                jobTable.updateJobStatus(job, JobStatus.Status.FAILURE, availableExtractors);
                }
            }
        }

        return totalCachedTriples;
    }

    private Set<KTriple> getCachedTriples(NamedResourceJob job) {
        IRI subject = job.getSubject();
        URL domain = job.getDomain();
        Set<Triple> triples = cache.get(job);
        if (triples == null || triples.isEmpty()) {
            return null;
        }
        Set<KTriple> kTriples = new HashSet<>(triples.size());
        Node subjectNode = NodeFactory.createURI(subject.toString());
        triples.forEach((t) -> {
            Origin origin = new OriginImpl(domain, CachedTripleExtractor.class);
            kTriples.add(new KTripleImpl(subjectNode, t.getPredicate(), t.getObject(), origin));
        });
//        Set<Job> jobsFromCache = JobCreator.createJobsFromTriples(configuration, job, kTriples);
//        for (Job newJob : jobsFromCache) {
//            if (newJob instanceof NamedResourceJob) {
//                Set<KTriple> newKTriples = getCachedTriples((NamedResourceJob) newJob);
//                if (safe(newKTriples).isEmpty()) {
//                    jobTable.addJobs(Collections.singleton(newJob));
//                } else {
//                    kTriples.addAll(newKTriples);
//                }
//            } else {
//                jobTable.addJobs(Collections.singleton(newJob));
//            }
//        }
        return kTriples;
    }

}

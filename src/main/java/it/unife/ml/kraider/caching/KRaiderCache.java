/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.caching;

import java.io.File;
import java.net.URL;
import java.util.Set;
import org.apache.commons.codec.digest.DigestUtils;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.Status;
import org.ehcache.config.Configuration;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.spi.loaderwriter.CacheLoadingException;
import org.ehcache.spi.loaderwriter.CacheWritingException;
import org.ehcache.xml.XmlConfiguration;
import it.unife.ml.kraider.data.Filters;
import it.unife.ml.kraider.data.NamedResourceJob;
import org.apache.jena.graph.Triple;
import org.semanticweb.owlapi.model.IRI;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class KRaiderCache {

    protected String cacheDir = ".cache" + File.separator;

    protected String fileEnding = ".rdf";

    // max capacity of entries for in-memory cache
    protected int inMemoryEntries = 1000;

    // specifies after how many seconds a cached result becomes invalid in in-memory cache
    protected long inMemoryFreshnessSecond = 5 * 60;

    // max capacity of entries in persistent cache
    protected int persistentEntries = 4000;

    // specifies after how many seconds a cached result becomes invalid in persistent cache
    protected long persistentFreshnessSeconds = 1 * 24 * 60 * 60;

    private String name = "KRaiderCache";

    private CacheManager cacheManager;

    private Cache<String, TripleSet> cache;

    private static KRaiderCache INSTANCE;

    private static int nOpenConnections = 0;

    public synchronized static KRaiderCache getInstance() {
        if (INSTANCE == null || INSTANCE.cacheManager.getStatus() != Status.AVAILABLE) {
            INSTANCE = new KRaiderCache();
        }
        KRaiderCache.nOpenConnections++;
        return INSTANCE;
    }

    private KRaiderCache() {
        URL configFile = getClass().getResource("/cache-config.xml");
        Configuration xmlConfig = new XmlConfiguration(configFile);
        System.setProperty("net.sf.ehcache.enableShutdownHook", "true");
        cacheManager = CacheManagerBuilder.newCacheManager(xmlConfig);
        cacheManager.init();

        this.cache = cacheManager.getCache("KRaiderCache", String.class, TripleSet.class);

//        System.getProperties().setProperty("java -Dnet.sf.ehcache.use.classic.lru", "true");
//        cacheManager = CacheManagerBuilder
//                .newCacheManagerBuilder().build();
//        cacheManager.init();
//        this.cache = cacheManager
//                .createCache("cacheOfPersonList", CacheConfigurationBuilder
//                        .newCacheConfigurationBuilder(String.class, TripleSet.class, ResourcePoolsBuilder.heap(10)
//                                .offheap(10, MemoryUnit.MB)));
    }

//    public void put(String sparqlQuery, URL domain, Set<Triple> triples) throws CacheWritingException {
//        String hash = getHash(sparqlQuery, domain);
//        try {
//            this.cache.put(hash, new TripleSet(triples));
//        } catch (CacheWritingException cwe) {
//            throw cwe;
//        }
//    }
    public void put(NamedResourceJob job, Set<Triple> triples) throws CacheWritingException {
        String hash = getHash(job);
        try {
            this.cache.put(hash, new TripleSet(triples));
        } catch (CacheWritingException cwe) {
            throw cwe;
        }
    }

//    public Set<Triple> get(String sparqlQuery, URL domain) throws CacheLoadingException {
//        String hash = getHash(sparqlQuery, domain);
//        try {
////            SizeOf sizeOf = SizeOf.newInstance();
////            long deepSize = sizeOf.deepSizeOf(cache);
////            System.out.println(deepSize);
//            TripleSet cachedTriples = cache.get(hash);
//            return (Set<Triple>) cachedTriples;
//        } catch (CacheLoadingException cle) {
//            throw cle;
//        }
//
////        TreeSet<Triple> triples = cachedTriples.stream()
////                .map(obj -> (Triple) obj)
////                .collect(Collectors.toSet());
//    }
    public Set<Triple> get(NamedResourceJob job) throws CacheLoadingException {
        String hash = getHash(job);
        try {
            TripleSet cachedTriples = cache.get(hash);
            return (Set<Triple>) cachedTriples;
        } catch (CacheLoadingException cle) {
            throw cle;
        }
    }

    public synchronized void close() {
        nOpenConnections--;
        if (nOpenConnections == 0) {
            cacheManager.close();
        }
    }

    /**
     * return filename where the query result should be saved
     *
     */
    private String getFilename(NamedResourceJob job) {
        return getCacheDir() + getHash(job) + getFileEnding();
    }

//    private String getFilename(String sparqlQuery, URL domain) {
//        return getCacheDir() + getHash(sparqlQuery, domain) + getFileEnding();
//    }
//    private String getHash(String sparqlQuery, URL domain) {
//        String hash = DigestUtils.md5Hex(sparqlQuery + domain);
//        return hash;
//    }
    private String getHash(NamedResourceJob job) {
        IRI subject = job.getSubject();
        Filters filters = job.getFilters();
        URL domain = job.getDomain();
        String additionalString = "";
        if (job.getRecursionDepth() == 0) {
            additionalString = "0";
        }
        String hash = DigestUtils.md5Hex(additionalString + subject + filters + domain);
        return hash;
    }

    /**
     * @return the cacheDir
     */
    public String getCacheDir() {
        return cacheDir;
    }

    /**
     * @param cacheDir the cacheDir to set
     */
    public void setCacheDir(String cacheDir) {
        this.cacheDir = cacheDir;
    }

    /**
     * @return the fileEnding
     */
    public String getFileEnding() {
        return fileEnding;
    }

    /**
     * @param fileEnding the fileEnding to set
     */
    public void setFileEnding(String fileEnding) {
        this.fileEnding = fileEnding;
    }

    /**
     * @return the inMemoryEntries
     */
    public int getInMemoryEntries() {
        return inMemoryEntries;
    }

    /**
     * @param inMemoryEntries the inMemoryEntries to set
     */
    public void setInMemoryEntries(int inMemoryEntries) {
        this.inMemoryEntries = inMemoryEntries;
    }

    /**
     * @return the inMemoryFreshnessSecond
     */
    public long getInMemoryFreshnessSecond() {
        return inMemoryFreshnessSecond;
    }

    /**
     * @param inMemoryFreshnessSecond the inMemoryFreshnessSecond to set
     */
    public void setInMemoryFreshnessSecond(long inMemoryFreshnessSecond) {
        this.inMemoryFreshnessSecond = inMemoryFreshnessSecond;
    }

    /**
     * @return the persistentEntries
     */
    public int getPersistentEntries() {
        return persistentEntries;
    }

    /**
     * @param persistentEntries the persistentEntries to set
     */
    public void setPersistentEntries(int persistentEntries) {
        this.persistentEntries = persistentEntries;
    }

    /**
     * @return the persistentFreshnessSeconds
     */
    public long getPersistentFreshnessSeconds() {
        return persistentFreshnessSeconds;
    }

    /**
     * @param persistentFreshnessSeconds the persistentFreshnessSeconds to set
     */
    public void setPersistentFreshnessSeconds(long persistentFreshnessSeconds) {
        this.persistentFreshnessSeconds = persistentFreshnessSeconds;
    }

}

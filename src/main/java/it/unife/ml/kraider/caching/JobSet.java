/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.kraider.caching;

import it.unife.ml.kraider.data.Job;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class JobSet extends LinkedHashSet<Job> {

    public JobSet() {
        super();
    }
    
    public JobSet(Set<Job> jobs) {
        super(jobs);
    }
    
}
